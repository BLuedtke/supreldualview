# Support Relation Analysis of Objects in Multiple View Point Clouds

This is a VC++ project (/.sln) intended for Visual Studio 19. Program is working in a minimal configuration, as I have not been able to finish refactoring and modularization.

I wrote this program as part of a research project lead by Prof. Dr. Wolter at the University of Bamberg. It aims to achieve a similar goal as Zhang et al. describe in https://arxiv.org/abs/1905.04084. That is, it takes in two point clouds that contain a view on the same scene from different angles (but ideally similar camera height). First, it removes the ground plane from the point clouds. The program then tries to align the views based on a segmentation of each view (segmentation with locally convex connected patches (LCCP)). In this alignment process, the segments from both views are matched to form 'objects'. These are then converted into a pseudo-voxel representation, and analyzed for stability relations. Check the document in the "report" folder for more information, and make sure to check out Zhang et al.'s original paper.

This project was originally not intended to be open-sourced. Thus, the program is far from optimal regarding class design etc., neither was it intended to be adaptable to others needs. However, it might still be interesting to see this code for some, which is why this repository exists. I will not maintain this code regularly. There are _some_ open TODOs, the most important of which I shall finish sometime in the future (hopefully).

Dependencies are (not distributed with this project):
- PCL (I used version 1.11.1) and its dependencies (especially VTK, Boost) {BSD license}
- TCLAP {MIT LICENSE}

The _code_ of this project itself is put here under the MIT LICENSE as well.

PCL must be installed and a PCL_ROOT system environment variable has to be set to the install location. For TCLAP, there needs to be a variable TCLAP_INCLUDE pointing at the directory containing the folder/directory which then contains the header files.
After compiling (use the release mode!), running could then be done with (for example):
` .\PCLrel.exe -a "C:\MDev\supreldualview\.testData\scan_1_2.pcd" -b "C:\MDev\supreldualview\.testData\scan_1_3.pcd" -f 0`

Here, Ground/Floor removal is disabled by `-f 0` because the example data does not have a floor to remove. Set to 1 if there is a floor to remove. Paths need to be adjusted to your folder/directory location obviously.
