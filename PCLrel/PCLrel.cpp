/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

//necessary defines for use of PCL 
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)


// Local includes
#include "ViewProcessor.h"
#include "src/PointCloudIO/PointCloudReader.h";
#include "src/Segmentation/Segment.h"
#include "src/Visualization/Visualizer.h"
#include "src/Modifiers/Transforms.h"
#include "src/Stability/SupportRelationAnalyzer.h"

// Stl/stdlib includes
#include <iostream>
#include <iomanip>
#include <string>
#include <algorithm>

// 3rd Party includes
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/console.hpp>

#include <tclap/CmdLine.h>

// Boost logging stuff (for SupportRelationAnalyzer only atm)
void setBoostLoggingLevel(int lvl);

// 'using' Definitions, Namespace renaming and other things
namespace logging = boost::log;
using std::cout;


int main(int argc, char** argv)
{
	// Read Commandline arguments
	
	setBoostLoggingLevel(2);
	using namespace TCLAP;
	std::string f1Path{};
	std::string f2Path{};
	bool visualizeCriticals{ true };

	ViewProcessor vProcessor{};

	try
	{
		CmdLine cmd("SupRel", ' ', "0.0.1");
		
		ValueArg<std::string> pathArgFile1 ("a", "file1", "Path to point cloud file 1", true, "", "string");
		ValueArg<std::string> pathArgFile2 ("b", "file2", "Path to point cloud file 2", true, "", "string");
		ValueArg<bool> floorRemoval ("f", "floorRemoval", "Enable (default) or disable floor removal (disable if model point cloud does not contain floor data)", false, true, "0 (false) or 1 (true)");
		ValueArg<bool> visualizer_flag("v", "visualizer", "Enable (default) or disable visualization of critical segment analysis", false, true, "0 (false) or 1 (true)");
		
		//TODO add more configurability!

		cmd.add(pathArgFile1);
		cmd.add(pathArgFile2);
		cmd.add(floorRemoval);
		cmd.parse(argc, argv);

		f1Path = pathArgFile1.getValue();
		f2Path = pathArgFile2.getValue();
		vProcessor.config.removeFloor = floorRemoval.getValue();
		visualizeCriticals = visualizer_flag.getValue();
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
	}

	// Load the Pointclouds from the location(s) that was/were provided via the commandline
	//...
	auto pc1 = PointCloudReader::getColorCloudFromPCD(f1Path);
	auto pc2 = PointCloudReader::getColorCloudFromPCD(f2Path);
	// Register and transform to Segments via ViewProcessor
	
	vProcessor.setInputViewClouds(pc1, pc2);
	auto segmentVec = vProcessor.registerViewsToSegments();

	// Apply Structural/Support Relation Analysis
	SupportRelationAnalyzer::prepareSegments(segmentVec);
	float voxelSize = SupportRelationAnalyzer::guessVoxelSize(segmentVec);

	std::vector<pcl::PointCloud<pcl::PointXYZ>> outVis2{};

	SupportRelationAnalyzer stabAnalyzer{ std::move(segmentVec), false, voxelSize};
	int retCode = 0;
	int countIts = 0;
	//TODO make configurable
	int maxTries = 3;
	do
	{
		countIts++;
		if (retCode != 0) {
			voxelSize *= 0.9f;
			cout << "Scene classified as unstable. Re-Trying with VoxelSize " << voxelSize << ". Iteration " << countIts << " out of max. " << maxTries << "\n";
			stabAnalyzer.config.voxelSize = voxelSize;
		}
		retCode = stabAnalyzer.evaluateSupportRelations();

	} while (retCode != 0 && countIts < maxTries);


	if (retCode == 0) {
		auto critSeg = stabAnalyzer.findCriticalSegments();

		cout << "STABLE: Scene stable with VoxelSize " << voxelSize << "." << endl;
		cout << "Critical VoxelSegment IDs: ";
		if (critSeg.empty()) {
			cout << "There are no VoxelSegments critical to the stability of the scene.\n";
		}
		else {
			for (const auto entry : critSeg) {
				cout << entry;
				if (entry != critSeg[critSeg.size() - 1]) {
					cout << ", ";
				}
				outVis2.push_back(stabAnalyzer.getVoxelRepresentationForSegmentID(entry));
			}
			cout << ".\n";
		}
	}
	else if (retCode == -1) {
		cout << "DEFECT: voxelSize is not set greater 0 or there are no segments for support analysis.";
	}
	else if (retCode == -2) {
		cout << "DEFECT: intermediate Voxel-Generation failed in support analysis controller.\n";
	}
	else if (retCode == -3) {
		cout << "UNSTABLE: support analysis controller found floating Voxel Segments.\n";
	}
	else if (retCode == -4) {
		cout << "UNSTABLE: Scene unstable.\n";
	}
	auto outVis = stabAnalyzer.getVoxelRepresentationAfterEvaluate();
	auto intersections = stabAnalyzer.getVoxelIntersectionRepresentationAfterEvaluate();
	
	//TODO REFINE
	Visualizer vis{};
	vis.init();
	if (retCode == 0) {
		cout << "\n\n\nTHE STABLE SCENE IS NOW BEING DISPLAYED.\n";
		cout << " SEGMENTS CRITICAL TO THE STABILITY OF THE SCENE ARE RED.\n";
		cout << " TO STOP, FOCUS VIEWER AND PRESS q." << std::endl;
		for (size_t i = 0; i < outVis.size(); ++i) {
			vis.addPointCloudRGBColor(outVis[i], 10.0, 200.0, 10.0, std::to_string(i) + "a");
		}
		vis.POINTSIZE = vis.POINTSIZE * 1.1f;
		for (size_t i = 0; i < outVis2.size(); ++i) {
			vis.addPointCloudRGBColor(outVis2[i], 200.0, 10.0, 10.0, std::to_string(i) + "_critical");
		}
	}
	else {
		cout << "\n\n\nTHE UNSTABLE SCENE IS NOW BEING DISPLAYED. TO STOP, FOCUS VIEWER AND PRESS q." << std::endl;
		for (size_t i = 0; i < outVis.size(); ++i) {
			vis.addPointCloudRGBColor(outVis[i], 190.0, 50.0, 10.0, std::to_string(i) + "a");
		}
	}

	vis.addCoordSysArrows(1.0);
	vis.run(true);
	
	//END
}







//BOOST_LOG_ATTRIBUTE_KEYWORD(line_id, "LineID", unsigned int)
namespace keywords = boost::log::keywords;
namespace attrs = boost::log::attributes;
namespace expr = boost::log::expressions;
BOOST_LOG_ATTRIBUTE_KEYWORD(severity, "Severity", logging::trivial::severity_level)

void setBoostLoggingLevel(int lvl)
{
	switch (lvl) {
		case 0: logging::core::get()->set_filter(logging::trivial::severity >= logging::trivial::trace); break;
		case 1: logging::core::get()->set_filter(logging::trivial::severity >= logging::trivial::debug); break;
		case 2: logging::core::get()->set_filter(logging::trivial::severity >= logging::trivial::info); break;
		case 3: logging::core::get()->set_filter(logging::trivial::severity >= logging::trivial::warning); break;
		case 4: logging::core::get()->set_filter(logging::trivial::severity >= logging::trivial::error); break;
		default: logging::core::get()->set_filter(logging::trivial::severity >= logging::trivial::debug); break;
	}

	//logging::core::get()->set
	//logging::core::remove_global_attribute()

	//auto formatT = expr::stream << "[" << severity << "]" << ": \t" << expr::message;
	auto formatT = expr::stream << expr::message << std::setw(6) << "[" << severity << "]";
	logging::add_console_log(std::clog, keywords::format = formatT);
	//add_console_log(std::clog, keywords::format = format);
}



/*
void askLoggingLevel()
{
	cout << "Which logging level do you want? Enter anything for debug (standard). Available Levels:\n";
	cout << "    - trace\n";
	cout << "    - debug (d)\n";
	cout << "    - info\n";
	cout << "    - warn\n";
	cout << "    - err\n";
	std::string userInput;
	cin >> userInput;
	int lvl = 1;
	if (cin.fail()) {
		cin.clear();
		cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		cout << "Unknown Level -> Standard Setting DEBUG\n";
	}
	else if (userInput.find("trace") != std::string::npos) {
		lvl = 0;
	}
	else if (userInput.find("d") != std::string::npos) {
		lvl = 1;
	}
	else if (userInput.find("info") != std::string::npos) {
		lvl = 2;
	}
	else if (userInput.find("warn") != std::string::npos) {
		lvl = 3;
	}
	else if (userInput.find("err") != std::string::npos) {
		lvl = 4;
	}
	else {
		cout << "Unknown Level -> Standard Setting DEBUG\n";
	}
	setLoggingLevel(lvl);
	cout << "Logging has been adjusted.\n";
}

/**/