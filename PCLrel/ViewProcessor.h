/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#pragma once
#ifndef VIEWPROCESSOR_H
#define VIEWPROCESSOR_H
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)

#include <pcl/pcl_base.h>
#include <pcl/common/colors.h>
#include <map>
#include "ViewProcessorConfig.hpp"

// Forward Declarations
class Segment;

class ViewProcessor
{
public:
	ViewProcessor();
	ViewProcessor(const pcl::PointCloud<pcl::PointXYZRGB>& view1, const pcl::PointCloud<pcl::PointXYZRGB>& view2, const ViewProcessorConfig& vConfig);
	ViewProcessor(const ViewProcessorConfig& vConfig);
	~ViewProcessor() = default;

	//Give in two point clouds that capture the scene from two different angles (at roughly similar height)
	// and receive back segments that each describe one recognized and reconstructed object in the scene.
	// You can use these segments to perform support relation analysis (or other stuff).
	std::vector<Segment> registerViewsToSegments();
	
	//Use this to configure how views should be processed/registered
	ViewProcessorConfig config;

	void setInputViewClouds(const pcl::PointCloud<pcl::PointXYZRGB>& view1, const pcl::PointCloud<pcl::PointXYZRGB>& view2);
	std::pair<pcl::PointCloud<pcl::PointXYZRGB>, pcl::PointCloud<pcl::PointXYZRGB>> getInputViewClouds() const;


private:
	pcl::PointCloud<pcl::PointXYZRGB> viewCloud1{};
	pcl::PointCloud<pcl::PointXYZRGB> viewCloud2{};

	void applySmartSmoothingOnScene(pcl::PointCloud<pcl::PointXYZRGB>& inputView1, pcl::PointCloud<pcl::PointXYZRGB>& inputView2);

	std::pair<pcl::PointCloud<pcl::PointXYZRGBNormal>, pcl::PointCloud<pcl::PointXYZRGBNormal>> preProcessViewsForSegmentation(const pcl::PointCloud<pcl::PointXYZRGB>& inputView1,  const pcl::PointCloud<pcl::PointXYZRGB>& inputView2);

	std::pair<std::map<int, Segment>, std::map<int, Segment>> performSegmentation(const pcl::PointCloud<pcl::PointXYZRGBNormal>& view1Mod, const pcl::PointCloud<pcl::PointXYZRGBNormal>& view2Mod);

	std::tuple<bool, float, std::map<int, int>> processSegmentsAndApplyGuess(const std::map<int, Segment>& seg1Map, std::map<int, Segment>& seg2Map, pcl::PointCloud<pcl::PointXYZRGBNormal>& view2Mod);

	std::vector<Segment> mergeSegments(const std::map<int, Segment>& seg1map, const std::map<int, Segment>& seg2map, const std::map<int, int>& matches, float midNNdist);
};

#endif /* VIEWPROCESSOR_H */