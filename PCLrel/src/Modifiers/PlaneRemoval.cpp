/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#include "PlaneRemoval.h"
#include <pcl/sample_consensus/ransac.h>
#include <pcl/sample_consensus/sac_model_parallel_plane.h>
#include <pcl/sample_consensus/sac_model_perpendicular_plane.h>
#include <pcl/sample_consensus/sac_model_normal_parallel_plane.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/common/io.h>
#include <pcl/common/common.h>
#include <map>


template pcl::PointCloud<pcl::PointNormal> PlaneRemoval::removeGroundPlaneByRateOfChangeUpNorm<pcl::PointNormal>(const pcl::PointCloud<pcl::PointNormal>& inputCloud, float& detectedGroundHeight, bool onlyLow);
template pcl::PointCloud<pcl::PointXYZRGBNormal> PlaneRemoval::removeGroundPlaneByRateOfChangeUpNorm<pcl::PointXYZRGBNormal>(const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud, float& detectedGroundHeight, bool onlyLow);

//Find Ground plane by Rate of Change, but only consider points whose normals point roughly "up" (aka normal_y > 0.8)
template<typename PointT>
pcl::PointCloud<PointT> PlaneRemoval::removeGroundPlaneByRateOfChangeUpNorm(const pcl::PointCloud<PointT>& inputCloud, float& detectedGroundHeight, bool preferLow)
{
	if (inputCloud.empty()) {
		std::cout << "removeGroundPlaneByRateOfChangeUpNorm: Input Cloud is empty!\n";
		return pcl::PointCloud<PointT>();
	}
	Eigen::Vector4f minPt = Eigen::Vector4f(0, 0, 0, 0);
	Eigen::Vector4f maxPt = Eigen::Vector4f(0, 0, 0, 0);
	pcl::getMinMax3D(inputCloud, minPt, maxPt);
	const auto minYfloored = static_cast<int>(std::floorf(minPt.y() * 100.0f));
	const auto maxYceiled = static_cast<int>(std::ceilf(maxPt.y() * 100.0f));

	std::map<int, int> heightCounter = {};
	std::map<int, int> heightCounterInverse = {};
	for (auto i = minYfloored; i < maxYceiled; ++i) {
		heightCounter[i] = 0;
		heightCounterInverse[i] = 0;
	}

	for (const auto& pt : inputCloud.points) {
		if (pt.normal_y < 0.8f) {
			const auto indxPos = static_cast<int>(std::floorf(pt.y * 100.0f));
			heightCounterInverse[indxPos] += 1;
			continue;
		}
		const auto indxPos = static_cast<int>(std::floorf(pt.y * 100.0f));
		heightCounter[indxPos] = heightCounter[indxPos] + 1;
	}
	int lastHeightCount = 0;
	int mostPosDiff = 0;
	int mostPosDiffHeight = 0;
	int mostMinDiff = 0;
	int mostMinDiffHeight = 0;
	int previousPointsHalf = 0;
	for (const auto ptCounter : heightCounter) {
		int diffH = ptCounter.second - lastHeightCount;
		if (preferLow) {
			diffH += previousPointsHalf;
		}
		if (diffH > mostPosDiff) {
			mostPosDiff = diffH;
			mostPosDiffHeight = ptCounter.first;
		}
		else if (diffH < mostMinDiff) {
			mostMinDiff = diffH;
			mostMinDiffHeight = ptCounter.first;
		}
		lastHeightCount = ptCounter.second;
		previousPointsHalf += (heightCounterInverse.at(ptCounter.first) / 2);
	}
	//std::cout << "Highest Negative difference was Height " << mostMinDiffHeight - 1 << " -> " << mostMinDiffHeight << ", with a diff of " << mostMinDiff << "\n";
	bool increased = true;
	int incCounter = 0;
	while (mostMinDiffHeight + 1 < maxYceiled && increased) {
		// Could also be set to only increment only if twice as many in heightCounter at that height.
		if (heightCounter.at(mostMinDiffHeight + 1) > heightCounterInverse.at(mostMinDiffHeight + 1)) {
			mostMinDiffHeight += 1;
			incCounter += 1;
			increased = true;
		}
		else {
			increased = false;
		}
	}
	
	detectedGroundHeight = static_cast<float>(mostMinDiffHeight) * 0.01f;
	pcl::PointCloud<PointT> outCloud {};
	outCloud.reserve(inputCloud.size());
	for (const auto& pt : inputCloud.points) {
		if (std::floorf(pt.y * 100.0f) >= static_cast<float>(mostMinDiffHeight)) {
			outCloud.push_back(pt);
		}
	}
	outCloud.resize(outCloud.size());
	return outCloud;
}

template float PlaneRemoval::determineGroundPlaneHeightByRateOfChangeUpNorm<pcl::PointNormal>(const pcl::PointCloud<pcl::PointNormal>& inputCloud);
template float PlaneRemoval::determineGroundPlaneHeightByRateOfChangeUpNorm<pcl::PointXYZRGBNormal>(const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud);

template<typename PointT>
float PlaneRemoval::determineGroundPlaneHeightByRateOfChangeUpNorm(const pcl::PointCloud<PointT>& inputCloud)
{
	if (inputCloud.empty()) {
		std::cout << "determineGroundPlaneHeightByRateOfChangeUpNorm: Input Cloud is empty!\n";
		return 0.0f;
	}
	Eigen::Vector4f minPt = Eigen::Vector4f(0, 0, 0, 0);
	Eigen::Vector4f maxPt = Eigen::Vector4f(0, 0, 0, 0);
	pcl::getMinMax3D(inputCloud, minPt, maxPt);
	const auto minYfloored = static_cast<int>(std::floorf(minPt.y() * 100.0f));
	const auto maxYceiled = static_cast<int>(std::ceilf(maxPt.y() * 100.0f));
	std::map<int, int> heightCounter = {};
	for (auto i = minYfloored; i < maxYceiled; ++i) {
		heightCounter[i] = 0;
	}
	for (const auto& pt : inputCloud.points) {
		if (pt.normal_y < 0.8f) {
			continue;
		}
		const auto indxPos = static_cast<int>(std::floorf(pt.y * 100.0f));
		heightCounter[indxPos] = heightCounter[indxPos] + 1;
	}
	int lastHeightCount = 0;
	int mostPosDiff = 0;
	int mostPosDiffHeight = 0;
	int mostMinDiff = 0;
	int mostMinDiffHeight = 0;

	for (const auto ptCounter : heightCounter) {
		const auto diffH = ptCounter.second - lastHeightCount;
		if (diffH > mostPosDiff) {
			mostPosDiff = diffH;
			mostPosDiffHeight = ptCounter.first;
		}
		else if (diffH < mostMinDiff) {
			mostMinDiff = diffH;
			mostMinDiffHeight = ptCounter.first;
		}
		lastHeightCount = ptCounter.second;
	}
	return (static_cast<float>(mostMinDiffHeight) * 0.01f);
}




pcl::PointCloud<pcl::PointXYZRGBNormal> PlaneRemoval::removeDominantXZParallelPlane(pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud, double planeWH, float& height)
{
	// -> Scene has to be on roughly even ground, and not on a slope
	if (inputCloud.empty()) {
		std::cout << "removeDominantXZParallelPlane: Input Cloud is empty!\n";
		return pcl::PointCloud<pcl::PointXYZRGBNormal>();
	}
	pcl::PointCloud<pcl::PointXYZRGB> test1P {};
	pcl::PointCloud<pcl::Normal> test1N {};
	test1P.reserve(inputCloud.size());
	test1N.reserve(inputCloud.size());
	pcl::copyPointCloud(inputCloud, test1P);
	pcl::copyPointCloud(inputCloud, test1N);
	test1P.resize(test1P.size());
	test1N.resize(test1N.size());

	auto cloudPtsPtr = std::make_shared<pcl::PointCloud<pcl::PointXYZRGB>>(test1P);
	auto cloudNrmlsPtr = std::make_shared<pcl::PointCloud<pcl::Normal>>(test1N);
	std::shared_ptr<pcl::SampleConsensusModelNormalParallelPlane<pcl::PointXYZRGB, pcl::Normal>> model_prl(new pcl::SampleConsensusModelNormalParallelPlane<pcl::PointXYZRGB, pcl::Normal>(cloudPtsPtr));

	model_prl->setAxis(Eigen::Vector3f(0.0f, 1.0f, 0.0f));
	model_prl->setNormalDistanceWeight(0.9);
	model_prl->setInputNormals(cloudNrmlsPtr);
	model_prl->setEpsAngle(DEG2RAD(8.0));	//HAS to be set > 0.0 

	pcl::RandomSampleConsensus<pcl::PointXYZRGB> ranSac(model_prl, planeWH);
	ranSac.computeModel();
	auto inliersVec = std::make_shared<std::vector<int>>();
	inliersVec->reserve(inputCloud.size() / 5);
	ranSac.getInliers(*inliersVec);
	if (!inliersVec->empty()) {
		Eigen::VectorXf t = Eigen::VectorXf();
		ranSac.getModelCoefficients(t);
		std::cout << "T: \n" << t << "\n";
		if (t(1) > 0.0f) {
			height = -t(3); // Possible as long as the epsilon angle is low
		}
		else {
			height = t(3);
		}
		height += planeWH * 0.5f;
		std::cout << "Height: " << height << "\n";

		auto masked = std::vector<bool>(inputCloud.size(), true);
		for (int i = 0; i < inliersVec->size(); ++i) {
			masked[inliersVec->at(i)] = false;
		}
		pcl::PointCloud<pcl::PointXYZRGBNormal> outCloud {};
		outCloud.reserve(inputCloud.size() - inliersVec->size());
		for (unsigned int i = 0; i < inputCloud.size(); ++i) {
			if (masked[i]) {
				if (inputCloud[i].y > height) {
					outCloud.push_back(inputCloud[i]);
				}
			}
		}
		outCloud.resize(outCloud.points.size());
		//std::cout << "Maximum expected size: " << inputCloud.size() - inliersVec->size() << "\n";
		//std::cout << "Actual size: " << outCloud.size() << "\n";
		return outCloud;
	}
	else {
		std::cout << "RANSAC INLIERS EMPTY\n";
		return pcl::PointCloud<pcl::PointXYZRGBNormal>(inputCloud);
	}
}