/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#include "Transforms.h"
#include <pcl/common/transforms.h>
#include <pcl/common/centroid.h>

template pcl::PointCloud<pcl::PointXYZ> Transforms::rotatePCloudAroundAxis<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& inputCloud, const Eigen::Vector3f axis, float angleInRad);
template pcl::PointCloud<pcl::PointXYZL> Transforms::rotatePCloudAroundAxis<pcl::PointXYZL>(const pcl::PointCloud<pcl::PointXYZL>& inputCloud, const Eigen::Vector3f axis, float angleInRad);
template pcl::PointCloud<pcl::PointXYZRGB> Transforms::rotatePCloudAroundAxis<pcl::PointXYZRGB>(const pcl::PointCloud<pcl::PointXYZRGB>& inputCloud, const Eigen::Vector3f axis, float angleInRad);
template pcl::PointCloud<pcl::PointXYZRGBA> Transforms::rotatePCloudAroundAxis<pcl::PointXYZRGBA>(const pcl::PointCloud<pcl::PointXYZRGBA>& inputCloud, const Eigen::Vector3f axis, float angleInRad);

template<typename PointT>
pcl::PointCloud<PointT> Transforms::rotatePCloudAroundAxis(const pcl::PointCloud<PointT>& inputCloud, const Eigen::Vector3f axis, float angleInRad)
{
	if (inputCloud.empty()) {
		std::cout << "rotatePCloudAroundAxis: Input Cloud is empty!\n";
		return pcl::PointCloud<PointT>();
	}
	Eigen::Affine3f transform = Eigen::Affine3f::Identity();
	transform.rotate(Eigen::AngleAxisf(angleInRad, axis));
	pcl::PointCloud<PointT> outCloud{};
	pcl::transformPointCloud(inputCloud, outCloud, transform);
	return outCloud;
}

template pcl::PointCloud<pcl::PointXYZ> Transforms::revolvePCloudAroundAxis<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& inputCloud, const Eigen::Vector3f axis, float angleInRad);
template pcl::PointCloud<pcl::PointXYZL> Transforms::revolvePCloudAroundAxis<pcl::PointXYZL>(const pcl::PointCloud<pcl::PointXYZL>& inputCloud, const Eigen::Vector3f axis, float angleInRad);
template pcl::PointCloud<pcl::PointXYZRGB> Transforms::revolvePCloudAroundAxis<pcl::PointXYZRGB>(const pcl::PointCloud<pcl::PointXYZRGB>& inputCloud, const Eigen::Vector3f axis, float angleInRad);
template pcl::PointCloud<pcl::PointXYZRGBA> Transforms::revolvePCloudAroundAxis<pcl::PointXYZRGBA>(const pcl::PointCloud<pcl::PointXYZRGBA>& inputCloud, const Eigen::Vector3f axis, float angleInRad);

template<typename PointT>
pcl::PointCloud<PointT> Transforms::revolvePCloudAroundAxis(const pcl::PointCloud<PointT>& inputCloud, const Eigen::Vector3f axis, float angleInRad)
{
	if (inputCloud.empty()) {
		std::cout << "revolvePCloudAroundAxis: Input Cloud is empty!\n";
		return pcl::PointCloud<PointT>();
	}
	pcl::PointXYZ ptCenter{};
	pcl::computeCentroid(inputCloud, ptCenter);
	auto tmpCloud = translatePCloud(inputCloud, -ptCenter.x, -ptCenter.y, -ptCenter.z);
	tmpCloud = rotatePCloudAroundAxis(tmpCloud, axis, angleInRad);
	return translatePCloud(tmpCloud, ptCenter.x, ptCenter.y, ptCenter.z);
}

template pcl::PointCloud<pcl::PointXYZ> Transforms::translatePCloud<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& inputCloud, float x, float y, float z);
template pcl::PointCloud<pcl::PointXYZL> Transforms::translatePCloud<pcl::PointXYZL>(const pcl::PointCloud<pcl::PointXYZL>& inputCloud, float x, float y, float z);
template pcl::PointCloud<pcl::PointXYZRGB> Transforms::translatePCloud<pcl::PointXYZRGB>(const pcl::PointCloud<pcl::PointXYZRGB>& inputCloud, float x, float y, float z);
template pcl::PointCloud<pcl::PointXYZRGBA> Transforms::translatePCloud<pcl::PointXYZRGBA>(const pcl::PointCloud<pcl::PointXYZRGBA>& inputCloud, float x, float y, float z);

template<typename PointT>
pcl::PointCloud<PointT> Transforms::translatePCloud(const pcl::PointCloud<PointT>& inputCloud, float x, float y, float z)
{
	if (inputCloud.empty()) {
		std::cout << "translatePCloud: Input Cloud is empty!\n";
		return pcl::PointCloud<PointT>();
	}
	Eigen::Affine3f transform = Eigen::Affine3f::Identity();
	transform.translation() << x, y, z;
	pcl::PointCloud<PointT> outCloud {};
	pcl::transformPointCloud(inputCloud, outCloud, transform);
	return outCloud;
}

template pcl::PointCloud<pcl::PointXYZ> Transforms::translatePCloud<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& inputCloud, const Eigen::Vector3f translateVec);
template pcl::PointCloud<pcl::PointXYZL> Transforms::translatePCloud<pcl::PointXYZL>(const pcl::PointCloud<pcl::PointXYZL>& inputCloud, const Eigen::Vector3f translateVec);
template pcl::PointCloud<pcl::PointXYZRGB> Transforms::translatePCloud<pcl::PointXYZRGB>(const pcl::PointCloud<pcl::PointXYZRGB>& inputCloud, const Eigen::Vector3f translateVec);
template pcl::PointCloud<pcl::PointXYZRGBA> Transforms::translatePCloud<pcl::PointXYZRGBA>(const pcl::PointCloud<pcl::PointXYZRGBA>& inputCloud, const Eigen::Vector3f translateVec);

template<typename PointT>
pcl::PointCloud<PointT> Transforms::translatePCloud(const pcl::PointCloud<PointT>& inputCloud, const Eigen::Vector3f translateVec)
{
	if (inputCloud.empty()) {
		std::cout << "translatePCloud: Input Cloud is empty!\n";
		return pcl::PointCloud<PointT>();
	}
	Eigen::Affine3f transform = Eigen::Affine3f::Identity();
	transform.translation() << translateVec;
	pcl::PointCloud<PointT> outCloud{};
	pcl::transformPointCloud(inputCloud, outCloud, transform);
	return outCloud;
}

template pcl::PointCloud<pcl::PointXYZ> Transforms::shiftCenterToOrigin<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& inputCloud);
template pcl::PointCloud<pcl::PointXYZL> Transforms::shiftCenterToOrigin<pcl::PointXYZL>(const pcl::PointCloud<pcl::PointXYZL>& inputCloud);
template pcl::PointCloud<pcl::PointXYZRGB> Transforms::shiftCenterToOrigin<pcl::PointXYZRGB>(const pcl::PointCloud<pcl::PointXYZRGB>& inputCloud);
template pcl::PointCloud<pcl::PointXYZRGBA> Transforms::shiftCenterToOrigin<pcl::PointXYZRGBA>(const pcl::PointCloud<pcl::PointXYZRGBA>& inputCloud);

template<typename PointT>
pcl::PointCloud<PointT> Transforms::shiftCenterToOrigin(const pcl::PointCloud<PointT>& inputCloud)
{
	if (inputCloud.empty()) {
		std::cout << "shiftCenterToOrigin: Input Cloud is empty!\n";
		return pcl::PointCloud<PointT>();
	}
	pcl::PointXYZ ptCenter {};
	pcl::computeCentroid(inputCloud, ptCenter);
	return translatePCloud(inputCloud, -ptCenter.x, -ptCenter.y, -ptCenter.z);
}


template pcl::PointCloud<pcl::PointNormal> Transforms::translatePCloudWithNormals<pcl::PointNormal>(const pcl::PointCloud<pcl::PointNormal>& inputCloud, float x, float y, float z);
template pcl::PointCloud<pcl::PointXYZRGBNormal> Transforms::translatePCloudWithNormals<pcl::PointXYZRGBNormal>(const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud, float x, float y, float z);

template<typename PointT>
pcl::PointCloud<PointT> Transforms::translatePCloudWithNormals(const pcl::PointCloud<PointT>& inputCloud, float x, float y, float z)
{
	if (inputCloud.empty()) {
		std::cout << "translatePCloudWithNormals: Input Cloud is empty!\n";
		return pcl::PointCloud<PointT>();
	}
	Eigen::Affine3f transform = Eigen::Affine3f::Identity();
	transform.translation() << x, y, z;
	pcl::PointCloud<PointT> outCloud {};
	pcl::transformPointCloudWithNormals(inputCloud, outCloud, transform);
	return outCloud;
}

template pcl::PointCloud<pcl::PointNormal> Transforms::translatePCloudWithNormals<pcl::PointNormal>(const pcl::PointCloud<pcl::PointNormal>& inputCloud, const Eigen::Vector3f translateVec);
template pcl::PointCloud<pcl::PointXYZRGBNormal> Transforms::translatePCloudWithNormals<pcl::PointXYZRGBNormal>(const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud, const Eigen::Vector3f translateVec);

template<typename PointT>
pcl::PointCloud<PointT> Transforms::translatePCloudWithNormals(const pcl::PointCloud<PointT>& inputCloud, const Eigen::Vector3f translateVec)
{
	if (inputCloud.empty()) {
		std::cout << "translatePCloudWithNormals: Input Cloud is empty!\n";
		return pcl::PointCloud<PointT>();
	}
	Eigen::Affine3f transform = Eigen::Affine3f::Identity();
	transform.translation() << translateVec;
	pcl::PointCloud<PointT> outCloud{};
	pcl::transformPointCloudWithNormals(inputCloud, outCloud, transform);
	return outCloud;
}


template pcl::PointCloud<pcl::PointNormal> Transforms::rotatePCloudAroundAxisWithNormals<pcl::PointNormal>(const pcl::PointCloud<pcl::PointNormal>& inputCloud, const Eigen::Vector3f axis, float angleInRad);
template pcl::PointCloud<pcl::PointXYZRGBNormal> Transforms::rotatePCloudAroundAxisWithNormals<pcl::PointXYZRGBNormal>(const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud, const Eigen::Vector3f axis, float angleInRad);

template<typename PointT>
pcl::PointCloud<PointT> Transforms::rotatePCloudAroundAxisWithNormals(const pcl::PointCloud<PointT>& inputCloud, const Eigen::Vector3f axis, float angleInRad)
{
	if (inputCloud.empty()) {
		std::cout << "rotatePCloudAroundAxisWithNormals: Input Cloud is empty!\n";
		return pcl::PointCloud<PointT>();
	}
	Eigen::Affine3f transform = Eigen::Affine3f::Identity();
	transform.rotate(Eigen::AngleAxisf(angleInRad, axis));
	pcl::PointCloud<PointT> outCloud{};
	pcl::transformPointCloudWithNormals(inputCloud, outCloud, transform);
	return outCloud;
}

