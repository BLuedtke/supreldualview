/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#pragma once
#ifndef PointRemoval_H
#define PointRemoval_H
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>


static class PointRemoval
{
public:
	template<typename PointT>
	static pcl::PointCloud<PointT> backfaceCulling(const pcl::PointCloud<PointT>& inCloud, float cullThreshold = 0.01f);

	//Deprecated. Please use hiddenPointRemovalQuick(2) instead.
	template<typename PointT>
	static pcl::PointCloud<PointT> hiddenPointRemoval(const pcl::PointCloud<PointT>& inCloud, float hiddenThreshold = 0.004f, unsigned int threads = 8);

	template<typename PointT>
	static pcl::PointCloud<PointT> hiddenPointRemovalQuick(const pcl::PointCloud<PointT>& inputCloud, float hiddenThreshold = 0.004f, unsigned int threads = 8);
	
	template<typename PointT>
	static pcl::PointCloud<PointT> hiddenPointRemovalQuick2(const pcl::PointCloud<PointT>& inputCloud, float hiddenThreshold = 0.004f, unsigned int threads = 8);

	template<typename PointT>
	static pcl::PointCloud<PointT> hiddenPointRemovalByOtherCloud(const pcl::PointCloud<PointT>& inCloud, const pcl::PointCloud<PointT>& obscuringCloud, float hiddenThreshold = 0.004f, unsigned int threads = 16);

	template<typename PointT>
	static pcl::PointCloud<PointT> removeSpatiallyDuplicatePoints(const pcl::PointCloud<PointT>& inputCloud);

	template<typename PointT>
	static pcl::PointCloud<PointT> getFarClippedCloud(const pcl::PointCloud<PointT>& inputCloud, float farClip);
	
	//TODO TEMPLATE
	static pcl::PointCloud<pcl::PointXYZRGBNormal> statistOutliersRemoval(const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud, int nrNeighbors = 10, float thresholdFactor = 2.0f);
private:
	template<typename PointT>
	static bool hidden(const pcl::PointCloud<PointT>& inCloud, int index, float hiddenThreshold);

	template<typename PointT>
	static bool hidden2(const pcl::PointCloud<PointT>& inCloud, const PointT& pt1, float hiddenThreshold);

	template<typename PointT>
	static bool hidden3(const pcl::PointCloud<PointT>& inputCloud, int index, float hiddenThreshold);

	template<typename PointT>
	static pcl::PointCloud<PointT> hiddenPart(const pcl::PointCloud<PointT>& inputCloud, int start, int end, float hiddenThreshold);

	template<typename PointT>
	static std::vector<int> hiddenPartMask(const pcl::PointCloud<PointT>& inputCloud, int start, int end, float hiddenThreshold);

	template<typename PointT>
	static pcl::PointCloud<PointT> distancePointCloudSorting(const pcl::PointCloud<PointT>& inputCloud);
};
#endif /* PointRemoval_H */
