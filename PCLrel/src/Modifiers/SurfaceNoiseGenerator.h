#pragma once
#ifndef SurfaceNoiseGenerator_H
#define SurfaceNoiseGenerator_H
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
//DEPRECATED
static class SurfaceNoiseGenerator
{
public:
	template<typename PointT>
	static pcl::PointCloud<PointT> generateNoisySurfaceFromNormalsT(const pcl::PointCloud<PointT>& inputCloud, float scaleDown = 0.01f, float strengthNormal = 1.0f, float strengthPlane = 1.0f);
private:

};
#endif /* SurfaceNoiseGenerator_H */
