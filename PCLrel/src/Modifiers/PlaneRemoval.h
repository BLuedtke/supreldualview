/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#pragma once
#ifndef PlaneRemoval_H
#define PlaneRemoval_H
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

static class PlaneRemoval
{
public:
	

	template<typename PointT>
	static pcl::PointCloud<PointT> removeGroundPlaneByRateOfChangeUpNorm(const pcl::PointCloud<PointT>& inputCloud, float& detectedGroundHeight, bool preferLow = true);

	template<typename PointT>
	static float determineGroundPlaneHeightByRateOfChangeUpNorm(const pcl::PointCloud<PointT>& inputCloud);

	//TODO TEMPLATE
	static pcl::PointCloud<pcl::PointXYZRGBNormal> removeDominantXZParallelPlane(pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud, double planeWH, float& height);


};
#endif /* PlaneRemoval_H */