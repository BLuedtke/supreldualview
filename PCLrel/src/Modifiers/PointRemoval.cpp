/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#include "PointRemoval.h"

#include <vector>
#include <thread>
#include <future>
#include <pcl/kdtree/kdtree_flann.h>
#include "../Constants/Constants.h"
#include <pcl/filters/statistical_outlier_removal.h>
#include <tuple>
#include <cmath>
#include <algorithm>

template pcl::PointCloud<pcl::PointNormal> PointRemoval::backfaceCulling<pcl::PointNormal>(const pcl::PointCloud<pcl::PointNormal>& inputCloud, float cullThreshold);
template pcl::PointCloud<pcl::PointXYZRGBNormal> PointRemoval::backfaceCulling<pcl::PointXYZRGBNormal>(const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud, float cullThreshold);

//PointT has to contain XYZ and NORMAL data, e.g. type PointNormal or PointXYZRGBNormal.
template<typename PointT>
pcl::PointCloud<PointT> PointRemoval::backfaceCulling(const pcl::PointCloud<PointT>& inputCloud, float cullThreshold)
{
	if (inputCloud.empty()) {
		std::cout << "backfaceCulling: Input Cloud is empty!\n";
		return pcl::PointCloud<PointT>();
	}
	using Eigen::Vector3f;
	pcl::PointCloud<PointT> outCloud {};
	outCloud.reserve(inputCloud.size());
	for (unsigned int i = 0; i < inputCloud.size(); ++i) {
		if (inputCloud[i].getVector3fMap().normalized().dot(inputCloud[i].getNormalVector3fMap()) < cullThreshold) {
			outCloud.push_back(inputCloud[i]);
		}
	}
	outCloud.points.shrink_to_fit();
	outCloud.resize(outCloud.points.size());
	return outCloud;
}


template bool PointRemoval::hidden<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& inputCloud, int index, float hiddenThreshold);
template bool PointRemoval::hidden<pcl::PointXYZRGB>(const pcl::PointCloud<pcl::PointXYZRGB>& inputCloud, int index, float hiddenThreshold);
template bool PointRemoval::hidden<pcl::PointXYZRGBA>(const pcl::PointCloud<pcl::PointXYZRGBA>& inputCloud, int index, float hiddenThreshold);
template bool PointRemoval::hidden<pcl::PointNormal>(const pcl::PointCloud<pcl::PointNormal>& inputCloud, int index, float hiddenThreshold);
template bool PointRemoval::hidden<pcl::PointXYZRGBNormal>(const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud, int index, float hiddenThreshold);

template<typename PointT>
inline bool PointRemoval::hidden(const pcl::PointCloud<PointT>& inputCloud, int index, float hiddenThreshold)
{
	//using const auto& is a bit slower here
	const auto pt1 = inputCloud[index]; 
	const auto pt1Norm = pt1.getVector3fMap().norm();
	const float pt1NormRed = pt1Norm * 0.99f;
	const float adjustedThreshold = hiddenThreshold * pt1Norm;
	for (unsigned int n = 0; n < inputCloud.size(); ++n) {
		//pt1 can't obscure itself.
		if (index == n) {
			continue;
		}
		//using const auto& is a bit slower here
		const auto pt2 = inputCloud[n];
		const auto pt2Norm = pt2.getVector3fMap().norm();
		
		//If pt2 is farther away from the camera than pt1, it can't obscure pt1
		if (pt1NormRed < pt2Norm) {
			continue;
		} 
		// Check if distance is smaller than the threshold. Higher chance of obscuration when pt1 and pt2 are further away from each other.
		//if ((pt2.getVector3fMap().cross(pt1.getVector3fMap())).norm() / pt1Norm < adjustedThreshold / pt2Norm) {
		if(adjustedThreshold > (pt2Norm * (pt2.getVector3fMap().cross(pt1.getVector3fMap())).norm()) / pt1Norm) {
			if (std::fabsf(pt2.y - pt1.y) < 0.01f && std::fabsf(pt2.y) > 0.05f) {
				//Special check because we know the camera is at 0,0,0
				//If both points are on the same height, and the camera is on a different height, then pt2 should not obscure pt1.
				continue;
			}
			else {
				return true;
			}
		}
		
	}
	return false;
}
template bool PointRemoval::hidden3<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& inputCloud, int index, float hiddenThreshold);
template bool PointRemoval::hidden3<pcl::PointXYZRGB>(const pcl::PointCloud<pcl::PointXYZRGB>& inputCloud, int index, float hiddenThreshold);
template bool PointRemoval::hidden3<pcl::PointXYZRGBA>(const pcl::PointCloud<pcl::PointXYZRGBA>& inputCloud, int index, float hiddenThreshold);
template bool PointRemoval::hidden3<pcl::PointNormal>(const pcl::PointCloud<pcl::PointNormal>& inputCloud, int index, float hiddenThreshold);
template bool PointRemoval::hidden3<pcl::PointXYZRGBNormal>(const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud, int index, float hiddenThreshold);

//inputCloud points need to be sorted by distance (ASC)!
template<typename PointT>
inline bool PointRemoval::hidden3(const pcl::PointCloud<PointT>& inputCloud, int index, float hiddenThreshold)
{
	//Only starts looking at the points with indices < index, because here we expect the cloud to be sorted by point distance from origin (ascending)!
	const auto& pt1 = inputCloud[index];
	const auto pt1Norm = pt1.getVector3fMap().norm();
	const float adjustedThreshold = hiddenThreshold * pt1Norm;
	const bool pt1IsAtCamHeight = (std::fabsf(pt1.y) < 0.05f) ? true : false;
	for(auto cIT = inputCloud.cbegin(); cIT != inputCloud.cbegin()+index; ++cIT){
		//Pt1 is hidden if Pt2 is closer to the camera and ON the line between pt1 and the origin (+ a slack value)
		
		//Special check because we know the camera is at 0,0,0
		//If both points are on the same height, and the camera is on a different height, then pt2 should not obscure pt1.
		if (!(std::fabsf(cIT->y - pt1.y) < 0.01f && !pt1IsAtCamHeight)) {
			//Now obscuration check.
			//if ((cIT->getVector3fMap().cross(pt1.getVector3fMap())).norm() / pt1Norm < adjustedThreshold / cIT->getVector3fMap().norm()) {
			//Faster Variant
			if(adjustedThreshold > (cIT->getVector3fMap().norm() * (cIT->getVector3fMap().cross(pt1.getVector3fMap())).norm()) / pt1Norm){
				//Point at index is hidden
				return true;
			}
		}
	}
	return false;
}


template bool PointRemoval::hidden2<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& inputCloud, const pcl::PointXYZ& pt1, float hiddenThreshold);
template bool PointRemoval::hidden2<pcl::PointXYZRGB>(const pcl::PointCloud<pcl::PointXYZRGB>& inputCloud, const pcl::PointXYZRGB& pt1, float hiddenThreshold);
template bool PointRemoval::hidden2<pcl::PointXYZRGBA>(const pcl::PointCloud<pcl::PointXYZRGBA>& inputCloud, const pcl::PointXYZRGBA& pt1, float hiddenThreshold);
template bool PointRemoval::hidden2<pcl::PointNormal>(const pcl::PointCloud<pcl::PointNormal>& inputCloud, const pcl::PointNormal& pt1, float hiddenThreshold);
template bool PointRemoval::hidden2<pcl::PointXYZRGBNormal>(const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud, const pcl::PointXYZRGBNormal& pt1, float hiddenThreshold);

//For testing obscuration from a different cloud
template<typename PointT>
bool PointRemoval::hidden2(const pcl::PointCloud<PointT>& inputCloud, const PointT& pt1, float hiddenThreshold)
{
	const auto pt1Norm = pt1.getVector3fMap().norm();
	const float pt1NormRed = pt1Norm * 0.99f;
	for (unsigned int n = 0; n < inputCloud.size(); ++n) {
		const auto& pt2 = inputCloud[n];
		const auto pt2Norm = pt2.getVector3fMap().norm();
		//If pt2 is farther away from the camera than pt1, it can't obscure pt1
		if (pt1NormRed < pt2Norm) {
			continue;
		}

		//Special check because we know the camera is at 0,0,0
		if (std::fabsf(pt2.y - pt1.y) < 0.01f && std::fabsf(pt2.y) > 0.05f) {
			//If both points are on the same height, and the camera is on a different height, then pt2 should not obscure pt1.
			continue;
		}
		// Check if distance is smaller than the threshold. Higher chance of obscuration when pt1 and pt2 are further away from each other.
		if ((pt2.getVector3fMap().cross(pt1.getVector3fMap())).norm() / pt1Norm < (hiddenThreshold * (pt1Norm / pt2Norm))) {
			return true;
		}
	}
	return false;
}

template pcl::PointCloud<pcl::PointXYZ> PointRemoval::hiddenPointRemoval<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& inputCloud, float hiddenThreshold, unsigned int threads);
template pcl::PointCloud<pcl::PointXYZRGB> PointRemoval::hiddenPointRemoval<pcl::PointXYZRGB>(const pcl::PointCloud<pcl::PointXYZRGB>& inputCloud, float hiddenThreshold, unsigned int threads);
template pcl::PointCloud<pcl::PointXYZRGBA> PointRemoval::hiddenPointRemoval<pcl::PointXYZRGBA>(const pcl::PointCloud<pcl::PointXYZRGBA>& inputCloud, float hiddenThreshold, unsigned int threads);
template pcl::PointCloud<pcl::PointXYZRGBNormal> PointRemoval::hiddenPointRemoval<pcl::PointXYZRGBNormal>(const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud, float hiddenThreshold, unsigned int threads);
template pcl::PointCloud<pcl::PointNormal> PointRemoval::hiddenPointRemoval<pcl::PointNormal>(const pcl::PointCloud<pcl::PointNormal>& inputCloud, float hiddenThreshold, unsigned int threads);

template<typename PointT>
inline pcl::PointCloud<PointT> PointRemoval::hiddenPointRemoval(const pcl::PointCloud<PointT>& inputCloud, float hiddenThreshold, unsigned int threads)
{
	if (inputCloud.empty()) {
		std::cout << "hiddenPointRemoval: Input Cloud is empty!\n";
		return pcl::PointCloud<PointT>();
	}
	
	pcl::PointCloud<PointT> outCloud{};
	outCloud.reserve(inputCloud.size());

	if (inputCloud.size() > threads) {
		for (unsigned int i = 0; (i + threads) < inputCloud.size(); i = i + threads) {
			std::vector<std::future<bool>> threadVec;
			threadVec.reserve(threads);

			for (unsigned int t = 0; t < threads; ++t) {
				threadVec.push_back(std::async(std::launch::async, hidden<PointT>, std::ref(inputCloud), i + t, hiddenThreshold));
			}
			for (unsigned int t = 0; t < threadVec.size(); ++t) {
				if (!(threadVec[t].get())) {
					outCloud.push_back(inputCloud[i + t]);
				}
			}
		}
		
		const auto start = inputCloud.size() - (inputCloud.size() % threads) - 1;
		for (unsigned int i = start; i < inputCloud.size(); ++i) {
			if (!hidden(inputCloud, i, hiddenThreshold)) {
				outCloud.push_back(inputCloud[i]);
			}
		}
	}
	else {
		for (unsigned int i = 0; i < inputCloud.size(); ++i) {
			if (!hidden(inputCloud, i, hiddenThreshold)) {
				outCloud.push_back(inputCloud[i]);
			}
		}
	}
	outCloud.resize(outCloud.size());
	return outCloud;
}


template pcl::PointCloud<pcl::PointXYZ> PointRemoval::distancePointCloudSorting<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& inputCloud);
template pcl::PointCloud<pcl::PointXYZRGB> PointRemoval::distancePointCloudSorting<pcl::PointXYZRGB>(const pcl::PointCloud<pcl::PointXYZRGB>& inputCloud);
template pcl::PointCloud<pcl::PointXYZRGBA> PointRemoval::distancePointCloudSorting<pcl::PointXYZRGBA>(const pcl::PointCloud<pcl::PointXYZRGBA>& inputCloud);
template pcl::PointCloud<pcl::PointXYZRGBNormal> PointRemoval::distancePointCloudSorting<pcl::PointXYZRGBNormal>(const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud);
template pcl::PointCloud<pcl::PointNormal> PointRemoval::distancePointCloudSorting<pcl::PointNormal>(const pcl::PointCloud<pcl::PointNormal>& inputCloud);

template<typename PointT>
pcl::PointCloud<PointT> PointRemoval::distancePointCloudSorting(const pcl::PointCloud<PointT>& inputCloud)
{
	if (inputCloud.empty()) {
		std::cout << "distancePointCloudSorting: Input Cloud is empty!\n";
		return pcl::PointCloud<PointT>();
	}
	struct {
		bool operator()(const PointT& a,const PointT& b) const {
			return a.getVector3fMap().squaredNorm() < b.getVector3fMap().squaredNorm();
		}
	} customLessOP;
	pcl::PointCloud<PointT> outCloud{ inputCloud };
	std::sort(outCloud.points.begin(), outCloud.points.end(), customLessOP);
	return outCloud;
}

template pcl::PointCloud<pcl::PointXYZ> PointRemoval::hiddenPointRemovalQuick<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& inputCloud, float hiddenThreshold, unsigned int threads);
template pcl::PointCloud<pcl::PointXYZRGB> PointRemoval::hiddenPointRemovalQuick<pcl::PointXYZRGB>(const pcl::PointCloud<pcl::PointXYZRGB>& inputCloud, float hiddenThreshold, unsigned int threads);
template pcl::PointCloud<pcl::PointXYZRGBA> PointRemoval::hiddenPointRemovalQuick<pcl::PointXYZRGBA>(const pcl::PointCloud<pcl::PointXYZRGBA>& inputCloud, float hiddenThreshold, unsigned int threads);
template pcl::PointCloud<pcl::PointXYZRGBNormal> PointRemoval::hiddenPointRemovalQuick<pcl::PointXYZRGBNormal>(const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud, float hiddenThreshold, unsigned int threads);
template pcl::PointCloud<pcl::PointNormal> PointRemoval::hiddenPointRemovalQuick<pcl::PointNormal>(const pcl::PointCloud<pcl::PointNormal>& inputCloud, float hiddenThreshold, unsigned int threads);
//Different setup for multithreading
template<typename PointT>
pcl::PointCloud<PointT> PointRemoval::hiddenPointRemovalQuick(const pcl::PointCloud<PointT>& inputCloud, float hiddenThreshold, unsigned int threads)
{
	if (inputCloud.empty()) {
		std::cout << "hiddenPointRemovalQuick: Input Cloud empty!\n";
		return pcl::PointCloud<PointT>();
	} else if (inputCloud.size() == 1) {
		return pcl::PointCloud<PointT>(inputCloud);
	}
	pcl::PointCloud<PointT> outCloud{};
	outCloud.reserve(inputCloud.size());
	const auto inputCloudSorted = distancePointCloudSorting(inputCloud);
	if (inputCloudSorted.size() > (size_t)threads * 5) {
		//Worth it to use threads
		const int iSize = inputCloudSorted.size();
		const int reducSize = iSize - (iSize % threads);
		const int blockSize = iSize / threads;

		std::vector<std::future<pcl::PointCloud<PointT>>> threadVec{};
		threadVec.reserve(threads);

		for (unsigned int t = 0; t < threads; ++t) {
			threadVec.push_back(std::async(std::launch::async, hiddenPart<PointT>, std::ref(inputCloudSorted), t * blockSize, t * blockSize + blockSize, hiddenThreshold));
		}
		for (unsigned int t = 0; t < threadVec.size(); ++t) {
			outCloud += threadVec[t].get();
		}
		const int remaining = iSize - reducSize;
		if (remaining > 0) {
			outCloud += hiddenPart<PointT>(inputCloudSorted, reducSize, inputCloudSorted.size(), hiddenThreshold);
		}
	}
	else {
		//Not worth it to use multithreading at this point cloud size.
		outCloud += hiddenPart<PointT>(inputCloudSorted, 0, inputCloudSorted.size(), hiddenThreshold);
	}
	outCloud.points.shrink_to_fit();
	outCloud.resize(outCloud.size());
	return outCloud;
}

template <typename L, typename R> inline void append(L& lhs, const R& rhs) { lhs.insert(lhs.end(), rhs.begin(), rhs.end()); }

template pcl::PointCloud<pcl::PointXYZ> PointRemoval::hiddenPointRemovalQuick2<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& inputCloud, float hiddenThreshold, unsigned int threads);
template pcl::PointCloud<pcl::PointXYZRGB> PointRemoval::hiddenPointRemovalQuick2<pcl::PointXYZRGB>(const pcl::PointCloud<pcl::PointXYZRGB>& inputCloud, float hiddenThreshold, unsigned int threads);
template pcl::PointCloud<pcl::PointXYZRGBA> PointRemoval::hiddenPointRemovalQuick2<pcl::PointXYZRGBA>(const pcl::PointCloud<pcl::PointXYZRGBA>& inputCloud, float hiddenThreshold, unsigned int threads);
template pcl::PointCloud<pcl::PointXYZRGBNormal> PointRemoval::hiddenPointRemovalQuick2<pcl::PointXYZRGBNormal>(const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud, float hiddenThreshold, unsigned int threads);
template pcl::PointCloud<pcl::PointNormal> PointRemoval::hiddenPointRemovalQuick2<pcl::PointNormal>(const pcl::PointCloud<pcl::PointNormal>& inputCloud, float hiddenThreshold, unsigned int threads);

// Experimental. Works with Masking and only instantiates the pointcloud to return at the end.
// Interestingly, this method is faster when using more threads than actually available.
// This is likely due to the threading "resolving" being suboptimal. If a core is finished with its block,
// it can start working on the next one, while we are waiting for a different one to finish its first.
// OMP Variant is not faster.
template<typename PointT>
pcl::PointCloud<PointT> PointRemoval::hiddenPointRemovalQuick2(const pcl::PointCloud<PointT>& inputCloud, float hiddenThreshold, unsigned int threads)
{
	if (inputCloud.empty()) {
		std::cout << "hiddenPointRemovalQuick: Input Cloud empty!\n";
		return pcl::PointCloud<PointT>();
	}
	else if (inputCloud.size() == 1) {
		return pcl::PointCloud<PointT>(inputCloud);
	}

	const auto inputCloudSorted = distancePointCloudSorting(inputCloud);
	if (inputCloudSorted.size() > static_cast<size_t>(threads) * 5) {
		//Worth it to use threads
		const int iSize = inputCloudSorted.size();
		const int reducSize = iSize - (iSize % threads);
		const int blockSize = iSize / threads;

		std::vector<std::vector<int>> maskCollection(threads);
		std::vector<int> mask{};
		mask.reserve(iSize);

		std::vector<std::future<std::vector<int>>> threadVec{};
		threadVec.reserve(threads);

		for (unsigned int t = 0; t < threads; ++t) {
			threadVec.push_back(std::async(std::launch::async, hiddenPartMask<PointT>, std::ref(inputCloudSorted), t * blockSize, t * blockSize + blockSize, hiddenThreshold));
		}
		
		for (size_t t = 0; t < threadVec.size(); ++t) {
			append(mask, threadVec[t].get());
		}
		if (iSize - reducSize > 0) {
			append(mask, hiddenPartMask<PointT>(inputCloudSorted, reducSize, inputCloudSorted.size(), hiddenThreshold));
		}
		return pcl::PointCloud<PointT>(inputCloudSorted, mask);
	}
	else {
		//Not worth it to use multithreading at this point cloud size.
		pcl::PointCloud<PointT> outCloud{};
		outCloud.reserve(inputCloud.size());
		outCloud += hiddenPart<PointT>(inputCloudSorted, 0, inputCloudSorted.size(), hiddenThreshold);
		return outCloud;
	}
}

template pcl::PointCloud<pcl::PointXYZ> PointRemoval::hiddenPart<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& inputCloud, int start, int end, float hiddenThreshold);
template pcl::PointCloud<pcl::PointXYZRGB> PointRemoval::hiddenPart<pcl::PointXYZRGB>(const pcl::PointCloud<pcl::PointXYZRGB>& inputCloud, int start, int end, float hiddenThreshold);
template pcl::PointCloud<pcl::PointXYZRGBA> PointRemoval::hiddenPart<pcl::PointXYZRGBA>(const pcl::PointCloud<pcl::PointXYZRGBA>& inputCloud, int start, int end, float hiddenThreshold);
template pcl::PointCloud<pcl::PointXYZRGBNormal> PointRemoval::hiddenPart<pcl::PointXYZRGBNormal>(const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud, int start, int end, float hiddenThreshold);
template pcl::PointCloud<pcl::PointNormal> PointRemoval::hiddenPart<pcl::PointNormal>(const pcl::PointCloud<pcl::PointNormal>& inputCloud, int start, int end, float hiddenThreshold);
//TODO Deprecate
template<typename PointT>
pcl::PointCloud<PointT> PointRemoval::hiddenPart(const pcl::PointCloud<PointT>& inputCloud, int start, int end, float hiddenThreshold)
{
	if (end > inputCloud.size()) {
		std::cout << "HiddenPart Invalid Range " << start << " - " << end << "; inputCloud Size: " << inputCloud.size() << "\n";
		return pcl::PointCloud<PointT>();
	}
	std::vector<int> mask{};
	mask.reserve((size_t)end - (size_t)start);
	for (int i = start; i < end; ++i) {
		if (!hidden3(inputCloud, i, hiddenThreshold)) {
			mask.push_back(i);
		}
	}
	return pcl::PointCloud<PointT>(inputCloud, mask);
}

template<typename PointT>
std::vector<int> PointRemoval::hiddenPartMask(const pcl::PointCloud<PointT>& inputCloud, int start, int end, float hiddenThreshold)
{
	if (end > inputCloud.size()) {
		std::cout << "HiddenPart Invalid Range " << start << " - " << end << "; inputCloud Size: " << inputCloud.size() << "\n";
		return std::vector<int>();
	}
	std::vector<int> mask{};
	mask.reserve((size_t)end - (size_t)start);
	for (int i = start; i < end; ++i) {
		if (!hidden3(inputCloud, i, hiddenThreshold)) {
			mask.push_back(i);
		}
	}
	return mask;
}

template pcl::PointCloud<pcl::PointXYZ> PointRemoval::hiddenPointRemovalByOtherCloud<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& inputCloud, const pcl::PointCloud<pcl::PointXYZ>& obscuringCloud, float hiddenThreshold, unsigned int threads);
template pcl::PointCloud<pcl::PointXYZRGB> PointRemoval::hiddenPointRemovalByOtherCloud<pcl::PointXYZRGB>(const pcl::PointCloud<pcl::PointXYZRGB>& inputCloud, const pcl::PointCloud<pcl::PointXYZRGB>& obscuringCloud, float hiddenThreshold, unsigned int threads);
template pcl::PointCloud<pcl::PointXYZRGBA> PointRemoval::hiddenPointRemovalByOtherCloud<pcl::PointXYZRGBA>(const pcl::PointCloud<pcl::PointXYZRGBA>& inputCloud, const pcl::PointCloud<pcl::PointXYZRGBA>& obscuringCloud, float hiddenThreshold, unsigned int threads);
template pcl::PointCloud<pcl::PointNormal> PointRemoval::hiddenPointRemovalByOtherCloud<pcl::PointNormal>(const pcl::PointCloud<pcl::PointNormal>& inputCloud, const pcl::PointCloud<pcl::PointNormal>& obscuringCloud, float hiddenThreshold, unsigned int threads);
template pcl::PointCloud<pcl::PointXYZRGBNormal> PointRemoval::hiddenPointRemovalByOtherCloud<pcl::PointXYZRGBNormal>(const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud, const pcl::PointCloud<pcl::PointXYZRGBNormal>& obscuringCloud, float hiddenThreshold, unsigned int threads);


template<typename PointT>
pcl::PointCloud<PointT> PointRemoval::hiddenPointRemovalByOtherCloud(const pcl::PointCloud<PointT>& inputCloud, const pcl::PointCloud<PointT>& obscuringCloud, float hiddenThreshold, unsigned int threads)
{
	if (inputCloud.empty()) {
		std::cout << "hiddenPointRemovalByOtherCloud: Input Cloud or obscuring cloud is empty!\n";
		return pcl::PointCloud<PointT>();
	}
	pcl::PointCloud<PointT> outCloud{};
	outCloud.reserve(inputCloud.size());
	if (inputCloud.size() > threads) {
		for (unsigned int i = 0; (i + threads) < inputCloud.size(); i = i + threads) {
			std::vector<std::future<bool>> threadVec;
			threadVec.reserve(threads);
			for (int t = 0; t < threads; ++t) {
				threadVec.push_back(std::async(std::launch::async, hidden2<PointT>, std::ref(obscuringCloud), inputCloud[i + t], hiddenThreshold));
			}
			for (unsigned int t = 0; t < threadVec.size(); ++t) {
				if (!(threadVec[t].get())) {
					outCloud.push_back(inputCloud[i + t]);
				}
			}
		}
		const auto start = inputCloud.size() - (inputCloud.size() % threads) - 1;
		for (unsigned int i = start; i < inputCloud.size(); ++i) {
			if (!hidden2(obscuringCloud, inputCloud[i], hiddenThreshold)) {
				outCloud.push_back(inputCloud[i]);
			}
		}
	}
	else {
		for (unsigned int i = 0; i < inputCloud.size(); ++i) {
			if (!hidden2(obscuringCloud, inputCloud[i], hiddenThreshold)) {
				outCloud.push_back(inputCloud[i]);
			}
		}
	}
	outCloud.resize(outCloud.size());
	return outCloud;
}

/**/

template pcl::PointCloud<pcl::PointXYZ> PointRemoval::removeSpatiallyDuplicatePoints<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& inputCloud);
template pcl::PointCloud<pcl::PointXYZRGB> PointRemoval::removeSpatiallyDuplicatePoints<pcl::PointXYZRGB>(const pcl::PointCloud<pcl::PointXYZRGB>& inputCloud);
template pcl::PointCloud<pcl::PointXYZRGBA> PointRemoval::removeSpatiallyDuplicatePoints<pcl::PointXYZRGBA>(const pcl::PointCloud<pcl::PointXYZRGBA>& inputCloud);
template pcl::PointCloud<pcl::PointNormal> PointRemoval::removeSpatiallyDuplicatePoints<pcl::PointNormal>(const pcl::PointCloud<pcl::PointNormal>& inputCloud);
template pcl::PointCloud<pcl::PointXYZRGBNormal> PointRemoval::removeSpatiallyDuplicatePoints<pcl::PointXYZRGBNormal>(const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud);

//Returns a pointcloud which contains all points from inputCloud, where if there are two points at the same position (XYZ), only one of them is contained in the returned cloud.
template<typename PointT>
pcl::PointCloud<PointT> PointRemoval::removeSpatiallyDuplicatePoints(const pcl::PointCloud<PointT>& inputCloud)
{
	//Converted to Cloud with just XYZ because we are only comparing the positions
	// KdTreeFLANN might not be able to take all point types we want to have available (template param),
	// so this ensures that kdTreeFLANN is not the limiting point.
	if (inputCloud.empty()) {
		std::cout << "remove Spatially Duplicate Points: Input Cloud Empty!\n";
		return pcl::PointCloud<PointT>();
	}
	auto xyzCloud = std::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
	pcl::copyPointCloud(inputCloud, *xyzCloud);
	pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;
	kdtree.setInputCloud(xyzCloud);
	kdtree.setEpsilon(0.0f);
	const int K = 2; // K = 2 because K = 1 -> only finds the point itself.
	std::vector<int> pIDs(K);
	std::vector<float> pSqDists(K);
	pcl::PointCloud<PointT> outCloud{};
	outCloud.reserve(inputCloud.size()); //Likely slightly too much, but better than constantly resizing throughout.

	assert(xyzCloud->size() == inputCloud.size());

	std::vector<int> duplsAdded{};
	duplsAdded.reserve(inputCloud.size() / 10);
	for(size_t i = 0; i < xyzCloud->size(); ++i){
		pIDs.clear();
		pSqDists.clear();
		if (kdtree.nearestKSearch(xyzCloud->points[i], K, pIDs, pSqDists) > 0) {
			if (pSqDists[1] < Constants::EPSILON) {
				if (std::find(duplsAdded.begin(), duplsAdded.end(), i) == duplsAdded.end()) {
					//i wasnt added so far
					if (std::find(duplsAdded.begin(), duplsAdded.end(), pIDs[1]) == duplsAdded.end()) {
						//pIDs[1] wasnt added so far
						//-> Neither one of the two points at the same position was added to the outCloud before.
						//		thus, add one of them to the outCloud and add the two to the duplsAdded vector.
						duplsAdded.push_back(i);
						duplsAdded.push_back(pIDs[1]);
						outCloud.push_back(inputCloud[i]);
					}
				}
			}
			else {
				//The nearest neighbour is far enough away
				outCloud.push_back(inputCloud[i]);
			}
		}
	}
	//std::cout << "duplsToAdd size: " << duplsToAdd.size() << "\n";
	outCloud.points.shrink_to_fit();
	outCloud.resize(outCloud.size());
	return outCloud;
}


template pcl::PointCloud<pcl::PointXYZ> PointRemoval::getFarClippedCloud(const pcl::PointCloud<pcl::PointXYZ>& inputCloud, float farClip);
template pcl::PointCloud<pcl::PointXYZRGB> PointRemoval::getFarClippedCloud(const pcl::PointCloud<pcl::PointXYZRGB>& inputCloud, float farClip);

template<typename PointT>
pcl::PointCloud<PointT> PointRemoval::getFarClippedCloud(const pcl::PointCloud<PointT>& inputCloud, float farClip)
{
	if (inputCloud.empty()) {
		std::cout << "getFarClippedCloud: Input Cloud is empty!\n";
		return pcl::PointCloud<PointT>();
	}
	const auto size = inputCloud.size();
	if (size == 0) {
		return pcl::PointCloud<PointT>();
	}
	pcl::PointCloud<PointT> retCloud {};
	retCloud.reserve(inputCloud.size());

	for (size_t n = 0; n < size; ++n) {
		if (inputCloud[n].getVector3fMap().norm() < farClip) {
			retCloud.push_back(inputCloud[n]);
		}
	}
	retCloud.resize(retCloud.points.size());
	return retCloud;
}

pcl::PointCloud<pcl::PointXYZRGBNormal> PointRemoval::statistOutliersRemoval(const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud, int nrNeighbors, float thresholdFactor)
{
	if (inputCloud.empty()) {
		std::cout << "statistOutliersRemoval input empty\n";
		return pcl::PointCloud<pcl::PointXYZRGBNormal>();
	}
	pcl::StatisticalOutlierRemoval<pcl::PointXYZRGBNormal> sOutlRemoval;
	sOutlRemoval.setInputCloud(std::make_shared<pcl::PointCloud<pcl::PointXYZRGBNormal>>(inputCloud));
	sOutlRemoval.setMeanK(nrNeighbors);
	sOutlRemoval.setStddevMulThresh(thresholdFactor);
	pcl::PointCloud<pcl::PointXYZRGBNormal> outCloud {};
	outCloud.reserve(inputCloud.size());
	sOutlRemoval.filter(outCloud);
	outCloud.points.shrink_to_fit();
	outCloud.resize(outCloud.size());
	return outCloud;
}
