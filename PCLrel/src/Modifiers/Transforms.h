/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#pragma once
#ifndef Transforms_H
#define Transforms_H
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

static class Transforms
{
public:
	template<typename PointT>
	static pcl::PointCloud<PointT> rotatePCloudAroundAxis(const pcl::PointCloud<PointT>& inputCloud, const Eigen::Vector3f axis, float angleInRad);

	//In comparison to rotatePCloudAroundAxis, this first shifts the cloud to the origin, then applies the rotation, then moves it back to where it's centroid was.
	template<typename PointT>
	static pcl::PointCloud<PointT> revolvePCloudAroundAxis(const pcl::PointCloud<PointT>& inputCloud, const Eigen::Vector3f axis, float angleInRad);

	template<typename PointT>
	static pcl::PointCloud<PointT> translatePCloud(const pcl::PointCloud<PointT>& inputCloud, float x, float y, float z);

	template<typename PointT>
	static pcl::PointCloud<PointT> translatePCloud(const pcl::PointCloud<PointT>& inputCloud, const Eigen::Vector3f translateVec);

	template<typename PointT>
	static pcl::PointCloud<PointT> shiftCenterToOrigin(const pcl::PointCloud<PointT>& inputCloud);

	//Only use this with Point Types that actually contain "normal" datafields.
	template<typename PointT>
	static pcl::PointCloud<PointT> translatePCloudWithNormals(const pcl::PointCloud<PointT>& inputCloud, float x, float y, float z);

	template<typename PointT>
	static pcl::PointCloud<PointT> translatePCloudWithNormals(const pcl::PointCloud<PointT>& inputCloud, const Eigen::Vector3f translateVec);

	//Only use this with Point Types that actually contain "normal" datafields.
	template<typename PointT>
	static pcl::PointCloud<PointT> rotatePCloudAroundAxisWithNormals(const pcl::PointCloud<PointT>& inputCloud, const Eigen::Vector3f axis, float angleInRad);
};

#endif /* Transforms_H */
