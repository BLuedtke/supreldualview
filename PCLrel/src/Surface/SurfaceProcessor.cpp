/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#include "SurfaceProcessor.h"
#include <pcl/features/normal_3d_omp.h>
#include <pcl/surface/mls.h>
#include <random>
#include <thread>
#include <pcl/filters/voxel_grid.h>


template pcl::PointCloud<pcl::Normal> SurfaceProcessor::estimateNormals<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& inputCloud, float radius, int kN, float viewX, float viewY, float viewZ);
template pcl::PointCloud<pcl::Normal> SurfaceProcessor::estimateNormals<pcl::PointXYZRGB>(const pcl::PointCloud<pcl::PointXYZRGB>& inputCloud, float radius, int kN, float viewX, float viewY, float viewZ);
template pcl::PointCloud<pcl::Normal> SurfaceProcessor::estimateNormals<pcl::PointXYZRGBA>(const pcl::PointCloud<pcl::PointXYZRGBA>& inputCloud, float radius, int kN, float viewX, float viewY, float viewZ);

//Method for estimating normals, designed to work with pcl::PointXYZ and pcl::PointXYZRGB(A).
// You can use radius or k-nearest neighbor search. Just set the one you do not want to use to 0.
template<typename PointT>
pcl::PointCloud<pcl::Normal> SurfaceProcessor::estimateNormals(const pcl::PointCloud<PointT>& inputCloud, float radius, int kN, float viewX, float viewY, float viewZ)
{
	if (inputCloud.empty()) {
		std::cout << "Estimate Normals: Input Cloud Empty!\n";
		return pcl::PointCloud<pcl::Normal>();
	}
	if (kN == 0 && radius <= 0.0f) {
		std::cout << "Estimate Normals: Parameters for both search methods are 0! Invalid configuration.\n";
		return pcl::PointCloud<pcl::Normal>();
	}
	//From: https://pointclouds.org/documentation/tutorials/normal_estimation.html visited at 2020-12-29
	const auto processor_count = std::thread::hardware_concurrency();
	pcl::NormalEstimationOMP<PointT, pcl::Normal> ne(processor_count);
	ne.setInputCloud(std::make_shared<pcl::PointCloud<PointT>>(inputCloud));
	ne.setSearchMethod(std::make_shared<pcl::search::KdTree<PointT>>());
	if (kN != 0) {
		ne.setKSearch(kN);
	}
	else {
		ne.setRadiusSearch(radius);
	}
	ne.setViewPoint(viewX, viewY, viewZ);
	

	// Compute the features
	pcl::PointCloud<pcl::Normal> outNormals {};
	outNormals.reserve(inputCloud.size());
	ne.compute(outNormals);
	outNormals.resize(outNormals.size());
	return outNormals;
}

template pcl::PointCloud<pcl::Normal> SurfaceProcessor::estimateNormals<pcl::PointXYZ>(pcl::PointCloud<pcl::PointXYZ>&& inputCloud, float radius, int kN, float viewX, float viewY, float viewZ);
template pcl::PointCloud<pcl::Normal> SurfaceProcessor::estimateNormals<pcl::PointXYZRGB>(pcl::PointCloud<pcl::PointXYZRGB>&& inputCloud, float radius, int kN, float viewX, float viewY, float viewZ);
template pcl::PointCloud<pcl::Normal> SurfaceProcessor::estimateNormals<pcl::PointXYZRGBA>(pcl::PointCloud<pcl::PointXYZRGBA>&& inputCloud, float radius, int kN, float viewX, float viewY, float viewZ);

template<typename PointT>
pcl::PointCloud<pcl::Normal> SurfaceProcessor::estimateNormals(pcl::PointCloud<PointT>&& inputCloud, float radius, int kN, float viewX, float viewY, float viewZ)
{
	auto inputLocal = pcl::PointCloud<PointT>{ std::move(inputCloud) };
	const int sizeR = inputLocal.size();
	if (inputLocal.empty()) {
		std::cout << "Estimate Normals: Input Cloud Empty!\n";
		return pcl::PointCloud<pcl::Normal>();
	}
	if (kN == 0 && radius <= 0.0f) {
		std::cout << "Estimate Normals: Parameters for both search methods are 0! Invalid configuration.\n";
		return pcl::PointCloud<pcl::Normal>();
	}
	const auto processor_count = std::thread::hardware_concurrency();
	pcl::NormalEstimationOMP<PointT, pcl::Normal> ne(processor_count);
	ne.setInputCloud(std::make_shared<pcl::PointCloud<PointT>>(std::move(inputLocal)));
	ne.setSearchMethod(std::make_shared<pcl::search::KdTree<PointT>>());
	if (kN != 0) {
		ne.setKSearch(kN);
	}
	else {
		ne.setRadiusSearch(radius);
	}
	ne.setViewPoint(viewX, viewY, viewZ);

	// Compute the features
	pcl::PointCloud<pcl::Normal> outNormals{};
	outNormals.reserve(sizeR);
	ne.compute(outNormals);
	outNormals.resize(outNormals.size());
	return outNormals;
}

template pcl::PointCloud<pcl::PointXYZ> SurfaceProcessor::smoothCloud<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& inputCloud, const float radius);
template pcl::PointCloud<pcl::PointXYZRGB> SurfaceProcessor::smoothCloud<pcl::PointXYZRGB>(const pcl::PointCloud<pcl::PointXYZRGB>& inputCloud, const float radius);
template pcl::PointCloud<pcl::PointXYZRGBA> SurfaceProcessor::smoothCloud<pcl::PointXYZRGBA>(const pcl::PointCloud<pcl::PointXYZRGBA>& inputCloud, const float radius);

//Smoothes a point cloud using Moving Least Squares, Polynomial Order 2, radius set via parameter. If the value is "too small", your
//point cloud will lose a lot of points.
template<typename PointT>
pcl::PointCloud<PointT> SurfaceProcessor::smoothCloud(const pcl::PointCloud<PointT>& inputCloud, const float radius)
{
	if (inputCloud.empty()) {
		std::cout << "Smooth Cloud: Input Cloud is empty!\n";
		return pcl::PointCloud<PointT>();
	}

	const auto processor_count = std::thread::hardware_concurrency();
	pcl::MovingLeastSquares<PointT, PointT> mls;
	mls.setNumberOfThreads(processor_count);
	mls.setComputeNormals(false);
	mls.setInputCloud(std::make_shared<pcl::PointCloud<PointT>>(inputCloud));
	mls.setPolynomialOrder(2);
	mls.setSearchMethod(std::make_shared<pcl::search::KdTree<PointT>>());
	mls.setSearchRadius(radius);

	pcl::PointCloud<PointT> smoothOutCloud {};
	smoothOutCloud.reserve(inputCloud.size());
	mls.process(smoothOutCloud);
	smoothOutCloud.resize(smoothOutCloud.size());
	return smoothOutCloud;
}

template pcl::PointCloud<pcl::PointXYZ> SurfaceProcessor::smoothCloud<pcl::PointXYZ>(pcl::PointCloud<pcl::PointXYZ>&& inputCloud, const float radius);
template pcl::PointCloud<pcl::PointXYZRGB> SurfaceProcessor::smoothCloud<pcl::PointXYZRGB>(pcl::PointCloud<pcl::PointXYZRGB>&& inputCloud, const float radius);
template pcl::PointCloud<pcl::PointXYZRGBA> SurfaceProcessor::smoothCloud<pcl::PointXYZRGBA>(pcl::PointCloud<pcl::PointXYZRGBA>&& inputCloud, const float radius);

template<typename PointT>
pcl::PointCloud<PointT> SurfaceProcessor::smoothCloud(pcl::PointCloud<PointT>&& inputCloud, const float radius)
{
	auto inputLocal = pcl::PointCloud<PointT>{ std::move(inputCloud) };
	const int sizeR = inputLocal.size();
	if (inputLocal.empty()) {
		std::cout << "Smooth Cloud: Input Cloud is empty!\n";
		return pcl::PointCloud<PointT>();
	}

	const auto processor_count = std::thread::hardware_concurrency();
	pcl::MovingLeastSquares<PointT, PointT> mls;
	mls.setNumberOfThreads(processor_count);
	mls.setComputeNormals(false);
	mls.setInputCloud(std::make_shared<pcl::PointCloud<PointT>>(std::move(inputLocal)));
	mls.setPolynomialOrder(2);
	mls.setSearchMethod(std::make_shared<pcl::search::KdTree<PointT>>());
	mls.setSearchRadius(radius);

	pcl::PointCloud<PointT> smoothOutCloud {};
	smoothOutCloud.reserve(sizeR);
	mls.process(smoothOutCloud);
	smoothOutCloud.resize(smoothOutCloud.size());
	return smoothOutCloud;
}


template pcl::PointCloud<pcl::PointXYZ> SurfaceProcessor::applyVoxelGridFilter(const pcl::PointCloud<pcl::PointXYZ>& inputCloud, float voxelSize);
template pcl::PointCloud<pcl::PointXYZRGB> SurfaceProcessor::applyVoxelGridFilter(const pcl::PointCloud<pcl::PointXYZRGB>& inputCloud, float voxelSize);
template pcl::PointCloud<pcl::PointXYZRGBA> SurfaceProcessor::applyVoxelGridFilter(const pcl::PointCloud<pcl::PointXYZRGBA>& inputCloud, float voxelSize);

//Applies a voxel filter on a copy of the input cloud and returns the result.
template<typename PointT>
pcl::PointCloud<PointT> SurfaceProcessor::applyVoxelGridFilter(const pcl::PointCloud<PointT>& inputCloud, float voxelSize)
{
	//std::cout << "Regular applyVoxelGridFilter\n";
	if (inputCloud.empty()) {
		std::cout << "applyVoxelGridFilter WARNING: Input Cloud is empty!\n";
		return pcl::PointCloud<PointT>();
	}
	pcl::VoxelGrid<PointT> vFilter;
	vFilter.setInputCloud(std::make_shared<pcl::PointCloud<PointT>>(inputCloud));
	vFilter.setLeafSize(voxelSize, voxelSize, voxelSize);
	pcl::PointCloud<PointT> outCloud {};
	outCloud.reserve(inputCloud.size());
	vFilter.filter(outCloud);
	outCloud.resize(outCloud.size());
	return outCloud;
}
template pcl::PointCloud<pcl::PointXYZ> SurfaceProcessor::applyVoxelGridFilter(pcl::PointCloud<pcl::PointXYZ>&& inputCloud, float voxelSize);
template pcl::PointCloud<pcl::PointXYZRGB> SurfaceProcessor::applyVoxelGridFilter(pcl::PointCloud<pcl::PointXYZRGB>&& inputCloud, float voxelSize);
template pcl::PointCloud<pcl::PointXYZRGBA> SurfaceProcessor::applyVoxelGridFilter(pcl::PointCloud<pcl::PointXYZRGBA>&& inputCloud, float voxelSize);

template<typename PointT>
pcl::PointCloud<PointT> SurfaceProcessor::applyVoxelGridFilter(pcl::PointCloud<PointT>&& inputCloud, float voxelSize)
{
	//std::cout << "Moved-From applyVoxelGridFilter\n";
	auto inputLocal = pcl::PointCloud<PointT>{ std::move(inputCloud) };
	const int sizeR = inputLocal.size();
	if (inputLocal.empty()) {
		std::cout << "applyVoxelGridFilter WARNING: Input Cloud is empty!\n";
		return pcl::PointCloud<PointT>();
	}
	pcl::VoxelGrid<PointT> vFilter;
	vFilter.setInputCloud(std::make_shared<pcl::PointCloud<PointT>>(std::move(inputLocal)));
	vFilter.setLeafSize(voxelSize, voxelSize, voxelSize);
	pcl::PointCloud<PointT> outCloud{};
	outCloud.reserve(sizeR);
	vFilter.filter(outCloud);
	outCloud.resize(outCloud.size());
	return outCloud;
}

template pcl::PointCloud<pcl::PointNormal> SurfaceProcessor::generateNoisySurfaceFromNormals<pcl::PointNormal>(const pcl::PointCloud<pcl::PointNormal>& inputCloud, float scaleDown, float strengthNormal, float strengthPlane);
template pcl::PointCloud<pcl::PointXYZRGBNormal> SurfaceProcessor::generateNoisySurfaceFromNormals<pcl::PointXYZRGBNormal>(const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud, float scaleDown, float strengthNormal, float strengthPlane);

std::random_device rdmDeviceX;
std::mt19937 genMTX(rdmDeviceX());
std::uniform_real_distribution<float> distrFloatF(-1.0f, 1.0f);

template<typename PointT>
pcl::PointCloud<PointT> SurfaceProcessor::generateNoisySurfaceFromNormals(const pcl::PointCloud<PointT>& inputCloud, float scaleDown, float strengthNormal, float strengthPlane)
{
	//std::cout << "Regular Generate Noisy Surface\n";
	if (inputCloud.empty()) {
		std::cout << "generateNoisySurfaceFromNormals: Can't scramble an empty inputCloud! Returning empty.\n";
		return pcl::PointCloud<PointT>();
	}
	//For every point, set Point = Point + powerN * Normal * RDM[-1,1]
	//For every point, set Point = Point + powerP * TwistedNormal * RDM[-1,1] // -> need to handle 2 dims here!

	pcl::PointCloud<PointT> outCloud {};
	outCloud.reserve(inputCloud.size());
	bool normalOnly = false;
	if (std::fabsf(strengthPlane) <= 0.0f) {
		normalOnly = true;
	}
	
	for (const auto& pt : inputCloud.points) {
		//Eigen Vectors offer very nice overloaded operators here
		// However, danger of confusion: ptNorm = Normal Vector; .norm() method = magnitude of an Eigen::Vector3f!
		auto ptXYZ = Eigen::Vector3f(pt.x, pt.y, pt.z);
		const auto ptNorm = Eigen::Vector3f(pt.normal_x, pt.normal_y, pt.normal_z).normalized();
		//(Random length) Shift along Normal
		ptXYZ = ptXYZ + (ptNorm * scaleDown * strengthNormal * distrFloatF(genMTX));

		if (!normalOnly) {
			//Now for the shift perpendicular to the Normal
			// -> This is a shift on a plane (so 2Dims). Want to shift in both dimensions 
			//		-> Need 2 Vectors, both should be perpendicular to the normal AND perpendicular to each other.
			// Both of these can work alone, but by creating both and comparing differences we avoid 
			//	the case of the "perpendicular" being computed as (0,0,0), which would cause issues.
			const auto perpendNormal1 = Eigen::Vector3f(ptNorm.z(), ptNorm.z(), -ptNorm.x() - ptNorm.y());
			const auto perpendNormal2 = Eigen::Vector3f(-ptNorm.y() - ptNorm.z(), ptNorm.x(), ptNorm.x());

			//perpendNormal2 shall be used if perpendNormal1 squared length is close to 0
			const auto ptNormTwist = (perpendNormal1.squaredNorm() < 0.001f) ? perpendNormal2.normalized() : perpendNormal1.normalized();

			if (ptNormTwist.dot(ptNorm) > 0.001f) {
				//Not perpendicular -> Skip!
				continue;
			}
			//the third direction, both perpendicular to original normal and first twister.
			const auto ptNormTwist2 = ptNorm.cross(ptNormTwist); 
			ptXYZ = ptXYZ + (ptNormTwist * strengthPlane * distrFloatF(genMTX) * scaleDown);
			ptXYZ = ptXYZ + (ptNormTwist2 * strengthPlane * distrFloatF(genMTX) * scaleDown);
		}

		//IMPORTANT. This is the reason why this project needs C++17 to compile.
		// older standards don't have the constexpr for if.
		if constexpr (std::is_same<PointT, pcl::PointXYZRGBNormal>::value) {
			outCloud.push_back(PointT(ptXYZ.x(), ptXYZ.y(), ptXYZ.z(), pt.r, pt.g, pt.b, ptNorm.x(), ptNorm.y(), ptNorm.z()));
		}
		else {
			outCloud.push_back(PointT(ptXYZ.x(), ptXYZ.y(), ptXYZ.z(), ptNorm.x(), ptNorm.y(), ptNorm.z()));
		}
	}
	return outCloud;
}

template pcl::PointCloud<pcl::PointNormal> SurfaceProcessor::generateNoisySurfaceFromNormals<pcl::PointNormal>(pcl::PointCloud<pcl::PointNormal>&& inputCloud, float scaleDown, float strengthNormal, float strengthPlane);
template pcl::PointCloud<pcl::PointXYZRGBNormal> SurfaceProcessor::generateNoisySurfaceFromNormals<pcl::PointXYZRGBNormal>(pcl::PointCloud<pcl::PointXYZRGBNormal>&& inputCloud, float scaleDown, float strengthNormal, float strengthPlane);

template<typename PointT>
pcl::PointCloud<PointT> SurfaceProcessor::generateNoisySurfaceFromNormals(pcl::PointCloud<PointT>&& inputCloud, float scaleDown, float strengthNormal, float strengthPlane)
{
	//std::cout << "Moved-From Generate Noisy Surface\n";
	auto inputLocal = pcl::PointCloud<PointT>{ std::move(inputCloud) };
	if (inputLocal.empty()) {
		std::cout << "generateNoisySurfaceFromNormals: Can't scramble an empty inputCloud! Returning empty.\n";
		return pcl::PointCloud<PointT>();
	}
	//For every point, set Point = Point + powerN * Normal * RDM[-1,1]
	//For every point, set Point = Point + powerP * TwistedNormal * RDM[-1,1] // -> need to handle 2 dims here!

	bool normalOnly = false;
	if (std::fabsf(strengthPlane) <= 0.0f) {
		normalOnly = true;
	}

	pcl::PointCloud<PointT> outCloud{};
	outCloud.reserve(inputLocal.size());

	for (const auto& pt : inputLocal.points) {
		//Eigen Vectors offer very nice overloaded operators here
		// However, danger of confusion: ptNorm = Normal Vector; .norm() method = magnitude of an Eigen::Vector3f!
		auto ptXYZ = Eigen::Vector3f(pt.x, pt.y, pt.z);
		const auto ptNorm = Eigen::Vector3f(pt.normal_x, pt.normal_y, pt.normal_z).normalized();
		
		//(Random length) Shift along Normal
		ptXYZ = ptXYZ + (ptNorm * scaleDown * strengthNormal * distrFloatF(genMTX));

		if (!normalOnly) {
			//Now for the shift perpendicular to the Normal
			// -> This is a shift on a plane (so 2Dims). Want to shift in both dimensions 
			//		-> Need 2 Vectors, both should be perpendicular to the normal AND perpendicular to each other.
			// Both of these can work alone, but by creating both and comparing differences we avoid 
			//	the case of the "perpendicular" being computed as (0,0,0), which would cause issues.
			const auto perpendNormal1 = Eigen::Vector3f(ptNorm.z(), ptNorm.z(), -ptNorm.x() - ptNorm.y());
			const auto perpendNormal2 = Eigen::Vector3f(-ptNorm.y() - ptNorm.z(), ptNorm.x(), ptNorm.x());

			//perpendNormal2 shall be used if perpendNormal1 squared length is close to 0
			const auto ptNormTwist = (perpendNormal1.squaredNorm() < 0.001f) ? perpendNormal2.normalized() : perpendNormal1.normalized();

			if (ptNormTwist.dot(ptNorm) > 0.001f) {
				//Not perpendicular -> Skip!
				continue;
			}
			//the third direction, both perpendicular to original normal and first twister.
			const auto ptNormTwist2 = ptNorm.cross(ptNormTwist);
			ptXYZ = ptXYZ + (ptNormTwist * strengthPlane * distrFloatF(genMTX) * scaleDown);
			ptXYZ = ptXYZ + (ptNormTwist2 * strengthPlane * distrFloatF(genMTX) * scaleDown);
		}

		//IMPORTANT. This is the reason why this project needs C++17 to compile.
		// older standards don't have the constexpr for if.
		if constexpr (std::is_same<PointT, pcl::PointXYZRGBNormal>::value) {
			outCloud.push_back(PointT(ptXYZ.x(), ptXYZ.y(), ptXYZ.z(), pt.r, pt.g, pt.b, ptNorm.x(), ptNorm.y(), ptNorm.z()));
		}
		else {
			outCloud.push_back(PointT(ptXYZ.x(), ptXYZ.y(), ptXYZ.z(), ptNorm.x(), ptNorm.y(), ptNorm.z()));
		}
	}
	return outCloud;
}