/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#pragma once
#ifndef SURFACEPROCESSOR_H
#define SURFACEPROCESSOR_H
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

//Implements processing methods using the surface/surface-like properties of the point cloud.
static class SurfaceProcessor
{
public:
	template<typename PointT>
	static pcl::PointCloud<pcl::Normal> estimateNormals(const pcl::PointCloud<PointT>& inputCloud, float radius, int kN, float viewX = 0.0f, float viewY = 0.0f, float viewZ = 0.0f);
	//Overloaded with rvalue reference for move semantics
	template<typename PointT>
	static pcl::PointCloud<pcl::Normal> estimateNormals(pcl::PointCloud<PointT>&& inputCloud, float radius, int kN, float viewX = 0.0f, float viewY = 0.0f, float viewZ = 0.0f);

	template<typename PointT>
	static pcl::PointCloud<PointT> smoothCloud(const pcl::PointCloud<PointT>& inputCloud, const float radius);
	//Overloaded with rvalue reference for move semantics
	template<typename PointT>
	static pcl::PointCloud<PointT> smoothCloud(pcl::PointCloud<PointT>&& inputCloud, const float radius);

	template<typename PointT>
	static pcl::PointCloud<PointT> applyVoxelGridFilter(const pcl::PointCloud<PointT>& inputCloud, float voxelSize);
	//Overloaded with rvalue reference for move semantics
	template<typename PointT>
	static pcl::PointCloud<PointT> applyVoxelGridFilter(pcl::PointCloud<PointT>&& inputCloud, float voxelSize);

	template<typename PointT>
	static pcl::PointCloud<PointT> generateNoisySurfaceFromNormals(const pcl::PointCloud<PointT>& inputCloud, float scaleDown = 0.01f, float strengthNormal = 1.0f, float strengthPlane = 1.0f);
	//Overloaded with rvalue reference for move semantics
	template<typename PointT>
	static pcl::PointCloud<PointT> generateNoisySurfaceFromNormals(pcl::PointCloud<PointT>&& inputCloud, float scaleDown = 0.01f, float strengthNormal = 1.0f, float strengthPlane = 1.0f);
};
#endif /* SURFACEPROCESSOR_H */
