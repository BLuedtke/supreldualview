/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#pragma once
#ifndef InitialGuesstimator_H
#define InitialGuesstimator_H
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)

#include <map>
#include <Eigen/StdVector>
#include <iostream>
#include <tuple>
#include <algorithm>

class Segment;
class InitialGuesstimator
{
public:
	InitialGuesstimator() = default;
	InitialGuesstimator(bool cGreedy, bool cUseMethod2, bool cUseABBs, bool cVerbose, unsigned int cBruteForceMaxN, float cAngleStepSize, bool allowChangeYPosition);
	
	
	//Setters
	void setInputSegments(const std::map<int, Segment>& seg1Map, const std::map<int, Segment>& seg2Map);
	void setUseMethodOneOrTwo(unsigned int methodNumber);
	void setUseGreedySearchForM1(bool useGreedy);
	void setUseABBsOverCentroids(bool useABBs);
	void setMaximumNumberOfSegmentsToBruteForce(unsigned int maxNumberToBruteForce);
	void setAngleStepSize(float angleStepSize);
	void setVerbosity(bool verbosity);
	void setAllowChangeOfYPosition(bool allowYPosChange);

	//Reset Input Segments
	void resetScene();
	//Compute
	void computeInitialGuess();
	
	//Getters
	float getInitialGuessAngle() const;
	Eigen::Vector3f getInitialGuessTranslation() const;
	std::map<int,int> getInitialGuessMatches() const;

private:
	//Settings
	bool greedy{ false };
	bool useMethod2{ true };
	bool useABBs{ true };
	bool verbose{ false };
	unsigned int bruteForceMaxN{ 9 };
	float angleStepSize{ 10.0f };
	//Experimental. Does not prevent the y-differences impacting the optimisation score, but does no apply it
	// to the gTranslation. Why? Because we assume the height is already mostly aligned. Use with care
	bool allowChangeYPosition{ true };
	
	//Segment Data
	std::map<int, Segment> seg1Map{};
	std::map<int, Segment> seg2Map{};

	//Initial guess Values once computed
	float gAngle{ 0.0f };
	Eigen::Vector3f gTranslation{0.0f,0.0f,0.0f};
	std::map<int, int> gMatches{};

	//Methods for performing the guesses
	bool performGuessMethodOne(bool searchGreedy = false, int nBestRotations = 5);
	bool performGuessMethodTwo();
	
	//For Brute Force Variants
	std::vector<std::map<int, int>> getAllCombinations() const;
	std::vector<std::map<int, int>> getAllCombinations(const std::vector<int>& aVec, const std::vector<int>& bVec) const;

	//For Method 1
	std::map<int, std::vector<float>> getAngleMap(const std::map<int, Segment>& segMap);
	std::vector<std::pair<float, std::map<int, int>>> getAngleAndMatchesGreedy(const std::map<int, std::vector<float>>& segAngles1, const std::map<int, std::vector<float>>& segAngles2) const;
	std::vector<std::pair<float, std::map<int, int>>> getAngleAndMatchesBruteForce(const std::map<int, std::vector<float>>& segAngles1, const std::map<int, std::vector<float>>& segAngles2) const;
	template <class Ta, class Tb>
	std::pair<float, int> getDiffAngleSumMapMinForPermutationsT(const Ta& segAngles1, const Ta& segAngles2, const Tb& permutations) const;

	std::pair<float, std::map<int, int>> getDiffAngleSumMapWithMatch(const std::map<int, std::vector<float>>& segAngles1, const std::map<int, std::vector<float>>& segAngles2) const;
	std::pair<float, std::map<int, int>> getDiffAngleSumMapWithMatchReverse(const std::map<int, std::vector<float>>& segAngles1, const std::map<int, std::vector<float>>& segAngles2) const;
	
	float getDiffAngleSumSingle(const std::vector<float>& angles1, const std::vector<float>& angles2) const;

	// Also for method 1, this modifies the gMatches member, so not const!
	void optimizeM1MatchesBasedOnPosition();

	//For Method 2 and Method 1
	std::pair<float, Eigen::Vector3f> optimizeVecResult(const std::vector<std::pair<Eigen::Vector3f, Eigen::Vector3f>>& centerVecPairsT, int maxIterations = 10) const;
	//Moved-From Overload for performance (quite a decent difference)
	std::pair<float, Eigen::Vector3f> optimizeVecResult(std::vector<std::pair<Eigen::Vector3f, Eigen::Vector3f>>&& centerVecPairsT, int maxIterations = 10) const;
};
#endif /* InitialGuesstimator_H */

template<class Ta, class Tb>
inline std::pair<float, int> InitialGuesstimator::getDiffAngleSumMapMinForPermutationsT(const Ta& segAngles1, const Ta& segAngles2, const Tb& permutations) const
{
	if (permutations.empty()) {
		return { -1.1f, -1 };
	}
	int bestPos = -1;
	float lowestSum = -1.0f;
	for (std::uint32_t i = 0; i < permutations.size(); ++i) {
		float mapSum = 0.0f;
		for (const auto& matchedPair : permutations[i]) {
			if (matchedPair.first != -1 && matchedPair.second != -1) {
				mapSum += getDiffAngleSumSingle(segAngles1.at(matchedPair.first), segAngles2.at(matchedPair.second));
			}
		}
		if (mapSum < lowestSum || bestPos == -1) {
			lowestSum = mapSum;
			bestPos = i;
		}
	}
	return std::make_pair(lowestSum, bestPos);
}