/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#include "ICPRegistration.h"

#include <pcl/registration/default_convergence_criteria.h>
#include <pcl/registration/icp.h>
#include <iostream>
#include <memory>
#include <string>

std::string convergenceEnumAsString(int enumVal)
{
	switch (enumVal)
	{
	case 0: return std::string("CONVERGENCE_CRITERIA_NOT_CONVERGED"); break;
	case 1: return std::string("CONVERGENCE_CRITERIA_ITERATIONS"); break;
	case 2: return std::string("CONVERGENCE_CRITERIA_TRANSFORM"); break;
	case 3: return std::string("CONVERGENCE_CRITERIA_ABS_MSE"); break;
	case 4: return std::string("CONVERGENCE_CRITERIA_REL_MSE"); break;
	case 5: return std::string("CONVERGENCE_CRITERIA_NO_CORRESPONDENCES"); break;
	case 6: return std::string("CONVERGENCE_CRITERIA_FAILURE_AFTER_MAX_ITERATIONS"); break;
	default:return std::string("UNKNOWN_CONVERGENCE_STATE");
		break;
	}
}

template pcl::PointCloud<pcl::PointXYZRGBNormal> ICPRegistration::registerWithNormalsICPN<pcl::PointXYZRGBNormal>(const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud1, const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud2, float maxCorrspDist, bool useReciprocal, bool useSymmetric, Eigen::Matrix4f& finalTransformOut);
template pcl::PointCloud<pcl::PointNormal> ICPRegistration::registerWithNormalsICPN<pcl::PointNormal>(const pcl::PointCloud<pcl::PointNormal>& inputCloud1, const pcl::PointCloud<pcl::PointNormal>& inputCloud2, float maxCorrspDist, bool useReciprocal, bool useSymmetric, Eigen::Matrix4f& finalTransformOut);

template<typename PointT>
pcl::PointCloud<PointT> ICPRegistration::registerWithNormalsICPN(const pcl::PointCloud<PointT>& inputCloud1, const pcl::PointCloud<PointT>& inputCloud2, float maxCorrspDist, bool useReciprocal, bool useSymmetric, Eigen::Matrix4f& finalTransformOut)
{
	if (inputCloud1.empty() || inputCloud2.empty()) {
		std::cout << "registerWithNormalsICPN: At least one input cloud is empty!\n";
		return pcl::PointCloud<PointT>();
	}
	//std::cout << "Starting ICPN" << std::endl;
	pcl::IterativeClosestPointWithNormals<PointT, PointT> icpN;
	icpN.setInputSource(std::make_shared<pcl::PointCloud<PointT>>(inputCloud1));
	icpN.setInputTarget(std::make_shared<pcl::PointCloud<PointT>>(inputCloud2));

	// correspondences with higher distances will be ignored -> Important. Best Val Influenced by Sensor and Distance from objects.
	icpN.setMaxCorrespondenceDistance(maxCorrspDist);
	// Set the maximum number of iterations (criterion 1)
	icpN.setMaximumIterations(50);
	// Set the transformation epsilon (criterion 2)
	icpN.setTransformationEpsilon(1e-8);
	// Set the euclidean distance difference epsilon (criterion 3)
	icpN.setEuclideanFitnessEpsilon(1.0);
	//icpN.setUseReciprocalCorrespondences(true);
	
	icpN.setUseSymmetricObjective(useSymmetric);

	icpN.setUseReciprocalCorrespondences(useReciprocal);

	//icpN.setEnforceSameDirectionNormals(true); //?
	
	// Perform the alignment
	pcl::PointCloud<PointT> sourceRegistered {};
	icpN.align(sourceRegistered); //out aligned
	finalTransformOut = icpN.getFinalTransformation();
	//std::cout << "        Relative MSE Crit: " << icpN.getConvergeCriteria()->getRelativeMSE() << "\n";
	
	//std::cout << "    Convergence State:   " << convergenceEnumAsString(static_cast<int>(icpN.getConvergeCriteria()->getConvergenceState())) << "\n";
	//std::cout << "    Fitness Score ICPN:  " << icpN.getFitnessScore() << "\n";

	return sourceRegistered;
}

