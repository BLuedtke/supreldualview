/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#include "InitialGuesstimator.h"

//#include <iostream>
#include <iomanip>

//#include <tuple>
#include <stdexcept>
#include <exception>
#include "../Segmentation/Segment.h"

//Testing
#include <chrono>

#ifndef DEG2RAD
#define DEG2RAD(x) ((x)*0.017453293)
#endif
#ifndef RAD2DEG
#define RAD2DEG(x) ((x)*57.29578)
#endif


using std::cout;
using std::setw;
using std::get;
using Eigen::Vector3f;

namespace CustomGuessHelpers {
	bool firstElemComparison(const std::pair<float, std::tuple<float, Vector3f, int>>& a, const std::pair<float, std::tuple<float, Vector3f, int>>& b)
	{
		return a.first < b.first;
	}

	inline unsigned int factorial(unsigned int n)
	{
		return (n == 0 || n == 1) ? 1 : n * factorial(n - 1);
	}
}

//Parameterized Constructor
InitialGuesstimator::InitialGuesstimator(bool cGreedy, bool cUseMethod2, bool cUseABBs, bool cVerbose, unsigned int cBruteForceMaxN, float cAngleStepSize, bool cAllowChangeYPosition) :
	greedy{ cGreedy }, useMethod2{ cUseMethod2 }, useABBs{ cUseABBs }, verbose{ cVerbose }, bruteForceMaxN{ cBruteForceMaxN }, angleStepSize{ cAngleStepSize }, allowChangeYPosition{cAllowChangeYPosition}
{
	//All other init is taken care of in the header/definition.
}


//Throws runtime exception if input segment map is empty.
void InitialGuesstimator::setInputSegments(const std::map<int, Segment>& seg1MapT, const std::map<int, Segment>& seg2MapT)
{
	if (seg1MapT.empty() || seg2MapT.empty()) {
		cout << "ERROR: NOT ALLOWED TO SET AN EMPTY MAP AS INPUT FOR INITIALGUESSTIMATOR\n";
		std::throw_with_nested(std::invalid_argument("Setting empty Segment maps as Input in InitialGuesstimator is not allowed"));
	}
	seg1Map = seg1MapT;
	seg2Map = seg2MapT;
}

//This will also reset the saved Guess values.
void InitialGuesstimator::resetScene()
{
	seg1Map.clear();
	seg2Map.clear();
	gAngle = 0.0f;
	gTranslation = Vector3f(0.0f, 0.0f, 0.0f);
	gMatches.clear();
}

void InitialGuesstimator::computeInitialGuess()
{
	if (seg1Map.empty() || seg2Map.empty()) {
		cout << "At least one view does not have any segments. No initial guess possible\n";
		std::throw_with_nested(std::runtime_error("At least one view does not have any segments. No initial guess possible"));
	}
	if (!greedy) {
		const int minSegSize = std::min(seg1Map.size(), seg2Map.size());
		if (minSegSize > bruteForceMaxN) {
			cout << "Too many Segments to brute force this! At least based on the setMaximumNumberOfSegmentsToBruteForce Setting.\n";
			cout << "Automatically Switching To Method 1 Using Greedy Search\n";
			performGuessMethodOne(true);
		}
		else {
			bool guessSuccess = true;
			if (useMethod2) {
				guessSuccess = performGuessMethodTwo();
			}
			else {
				guessSuccess = performGuessMethodOne(false);
			}
			if (!guessSuccess) {
				cout << "WARNING: Failed to perform Initial Guess in FIRST Attempt. Falling Back to Method 1, Greedy.\n";
				guessSuccess = performGuessMethodOne(true);
				if (!guessSuccess) {
					cout << "WARNING: Failed to perform Initial Guess in SECOND Attempt. NO MORE FALLBACKS.\n";
				} else if(guessSuccess && verbose){
					cout << "The Initial Guess was successfully calculated in the second attempt and is now available via the getters.\n";
				}
			}
			else if(guessSuccess && verbose){
				cout << "The Initial Guess was successfully calculated and is now available via the getters.\n";
			}
		}
	}
	else {
		bool guessSuccess = performGuessMethodOne(greedy);
		if (guessSuccess && verbose) {
			cout << "The Initial Guess was successfully calculated and is now available via the getters.\n";
		}
	}
	//Experimental
	if (!allowChangeYPosition) {
		gTranslation.y() = 0.0f;
	}
}

//Returns the guessed Angle around the y-Axis for view2Segments.
// You will probably need to invert it before applying it, though
// that depends on your use-case.
float InitialGuesstimator::getInitialGuessAngle() const
{
	return gAngle;
}

Vector3f InitialGuesstimator::getInitialGuessTranslation() const
{
	return gTranslation;
}

std::map<int, int> InitialGuesstimator::getInitialGuessMatches() const
{
	return gMatches;
}

//Performs an initial guess with Method 1, either using greedy Search or Brute-Forcing all permutations.
bool InitialGuesstimator::performGuessMethodOne(bool searchGreedy, int nBestRotations)
{
	if (seg1Map.size() == 1 && seg2Map.size() == 1) {
		gMatches.clear();
		gMatches.insert({ seg1Map.begin()->first , seg2Map.begin()->first });
		gAngle = 0.0f;
		//Maybe this could be expanded in the future - some guess derivation might be 
		// possible even with only one segment in each.
		if (useABBs) {
			gTranslation = seg1Map.begin()->second.getABBMiddle() - seg2Map.begin()->second.getABBMiddle();
		} else {
			gTranslation = seg1Map.begin()->second.getHullCenter() - seg2Map.begin()->second.getHullCenter();
		}
		return true;
	}
	//Get the angle maps
	const auto seg1Angles = getAngleMap(seg1Map);
	const auto seg2Angles = getAngleMap(seg2Map);
	
	std::vector<std::pair<float, std::map<int, int>>> rotationGuesses{};
	if (seg1Angles.empty() || seg2Angles.empty()) {
		cout << "M1 Special Case: No Angles for one of the views (probably only has 1 segment).\n";
		//Crude fix if one View only has one segment
		if (seg1Angles.empty() && !seg2Angles.empty() && seg1Map.size() == 1) {
			for (const auto& cSeg2 : seg2Angles)
			{
				rotationGuesses.push_back({ 0.0f, {{seg1Map.begin()->first, cSeg2.first}} });
			}
		} else if (!seg1Angles.empty() && seg2Angles.empty() && seg2Map.size() == 1) {
			for (const auto& cSeg1 : seg1Angles)
			{
				rotationGuesses.push_back({ 0.0f, {{cSeg1.first, seg2Map.begin()->first}} });
			}
		} else {
			cout << "WARNING: InitialGuesstimator Method 1 Could not Find an initial guess!";
			cout << " One of the Angles List was length 0, even though the segMap for that list has a length > 1\n";
			return false;
		}
	}
	else {
		//Good case: Enough Angles/Segments in both views
		rotationGuesses = (searchGreedy) ? getAngleAndMatchesGreedy(seg1Angles, seg2Angles) : getAngleAndMatchesBruteForce(seg1Angles, seg2Angles);
	}
	
	if (rotationGuesses.empty()) {
		cout << "WARNING: performGuessMethodOne got no rotationguesses it could work with!\n";
		return false;
	}
	
	std::unordered_map<int, Vector3f> ctrSeg1Map {};
	for (const auto& entry : seg1Map) {
		ctrSeg1Map.insert_or_assign(entry.first, (useABBs) ? entry.second.getABBMiddle() : entry.second.getHullCenter());
	}
	//TODO how does one properly do this in C++ again? Max_Num or so?
	float bestImprovedDistanceSum = 9999999999.0f; 
	float bestAngle = -1.1f;
	Vector3f bestTotalTranslation{ 0.0f, 0.0f, 0.0f };
	std::map<int, int> matches {};
	const int minT = std::min(static_cast<int>(rotationGuesses.size()), nBestRotations);
	
	for (size_t i = 0; i < minT; ++i) {
		const auto& guessEntry = rotationGuesses[i];
		const double angleRad = DEG2RAD(-guessEntry.first);

		//Apply the initial guess angle for this loop iteration
		std::unordered_map<int, Vector3f> rotatedCtrs2_UNSORTED{};
		Eigen::Affine3f transform = Eigen::Affine3f::Identity();
		transform.rotate(Eigen::AngleAxisf(angleRad, Vector3f::UnitY()));

		for (const auto& seg2s : seg2Map) {
			Vector3f preRC = (useABBs) ? seg2s.second.getABBMiddle() : seg2s.second.getHullCenter();
			preRC = transform * preRC;
			rotatedCtrs2_UNSORTED.insert_or_assign(seg2s.first, std::move(preRC));
		}
		//Build a vector which holds the two centers of the segments that are proposed to be matched
		std::vector<std::pair<Vector3f, Vector3f>> centerVecPairs {};
		centerVecPairs.reserve(guessEntry.second.size());
		for (const auto& entry : guessEntry.second) {
			if (entry.first != -1 && entry.second != -1) {
				centerVecPairs.push_back({ ctrSeg1Map.at(entry.first), rotatedCtrs2_UNSORTED.at(entry.second) });
			}
		}
		//low maxIterations count is intentional
		const std::pair<float, Vector3f> localConfig { optimizeVecResult(std::move(centerVecPairs), 4) };
		
		if (localConfig.first < bestImprovedDistanceSum && localConfig.first > -1.0f) {
			bestAngle = guessEntry.first;
			bestImprovedDistanceSum = localConfig.first;
			bestTotalTranslation = localConfig.second;
			matches = guessEntry.second;
		}
	}
	if (matches.empty() || bestAngle < -1.0f) {
		cout << "WARNING: InitialGuesstimator Method 1 Could not Find an initial guess!\n";
		return false;
	}
	gAngle = bestAngle;
	gTranslation = bestTotalTranslation;
	gMatches = matches;
	optimizeM1MatchesBasedOnPosition();
	return true;
}

//For all possible match configurations, goes through rotating view2Segments (doesnt modify seg2Map though)
// and checks which match configuration allows the best position optimization
bool InitialGuesstimator::performGuessMethodTwo()
{	
	if (seg1Map.size() == 1 && seg2Map.size() == 1) {
		this->gMatches.clear();
		this->gMatches.insert({ seg1Map.begin()->first , seg2Map.begin()->first });
		this->gAngle = 0.0f;
		//Maybe this could be expanded in the future - some guess derivation might be possible even with only one segment in each.
		this->gTranslation = seg1Map.begin()->second.getABBMiddle() - seg2Map.begin()->second.getABBMiddle();
		return true;
	}
	const auto permutations = getAllCombinations();

	if (permutations.size() == 0) {
		cout << "Number of possible match permutations returned 0. Method 2 Unsuccessful\n";
		return false;
	}
	std::unordered_map<int, Vector3f> ctrSeg1Map {};
	for (const auto& entry : seg1Map) {
		ctrSeg1Map.insert_or_assign(entry.first, (useABBs) ? entry.second.getABBMiddle() : entry.second.getHullCenter());
	}
	std::vector<std::pair<float, std::tuple<float, Vector3f, int>>> rotationBestScores {};
	rotationBestScores.reserve(static_cast<size_t>(360.0f / angleStepSize) + (size_t)1);
	std::unordered_map<int, Vector3f> rotatedCtrs2_UNSORTED{};
	float currentAngle = 0.0f;
	while (currentAngle < 360.0f)
	{
		currentAngle += angleStepSize;

		Eigen::Affine3f transform(Eigen::AngleAxisf(DEG2RAD(currentAngle), Vector3f::UnitY()));
		rotatedCtrs2_UNSORTED.clear();
		for (const auto& seg2s : seg2Map) {
			Vector3f preRC = (useABBs) ? seg2s.second.getABBMiddle() : seg2s.second.getHullCenter();
			preRC = transform * preRC;
			rotatedCtrs2_UNSORTED.insert_or_assign(seg2s.first, preRC);
		}
		std::pair<int, std::pair<float, Vector3f>> bestConfig{ -1 , {0.0f, Vector3f{}} };
		for (size_t i = 0; i < permutations.size(); ++i) {
			std::vector<std::pair<Vector3f, Vector3f>> centerVecPairs {};
			centerVecPairs.reserve(permutations[i].size());
			for (const auto& entry : permutations[i]) {
				if (entry.first != -1 && entry.second != -1) {
					centerVecPairs.push_back({ ctrSeg1Map.at(entry.first), rotatedCtrs2_UNSORTED.at(entry.second) });
				}
			}
			const std::pair<float, Vector3f> localConfig { optimizeVecResult(std::move(centerVecPairs), 7) };
			
			//if finalSumDists is lower or we are in first iteration
			if (localConfig.first > -1.0f && (localConfig.first < bestConfig.second.first || bestConfig.first == -1)) { 
				bestConfig.first = i;
				bestConfig.second = localConfig;
			}
		}

		//bestConfig now holds the data of the best scoring permutation for this rotation angle
		//Because of how we use/apply the angle, we need to invert it here.
		const float convertedAngle = (-currentAngle) + 360.0f;
		if (bestConfig.first != -1) {
			rotationBestScores.push_back({ bestConfig.second.first, {convertedAngle, bestConfig.second.second, bestConfig.first} });
		}
	}
	if (rotationBestScores.empty()) {
		cout << "FAILED Matching: No possible combination found.\n";
		return false;
	}
	//By sorting this Vector, the lowest (best) scores will be in the front.
	std::sort(rotationBestScores.begin(), rotationBestScores.end(), CustomGuessHelpers::firstElemComparison);
	
	if (verbose) {
		int k = (rotationBestScores.size() > 5) ? 5 : rotationBestScores.size();
		cout << "5 BEST CANDIDATES: \n";
		for (int c = 0; c < k; ++c) {
			cout << " " << "Angle: " << setw(5) << get<0>(rotationBestScores[c].second) << "; DistS: " << setw(11) << rotationBestScores[c].first << "\n";
		}
	}
	// {DistanceScore, {angle, Finaltranslation, PermutationIndex}}
	if (get<2>(rotationBestScores.begin()->second) == -1) {
		cout << "Best match has an Invalid MatchMap ID. Something went wrong here. No initial guess possible.\n";
		return false;
	}
	this->gAngle = get<0>(rotationBestScores.begin()->second);
	this->gTranslation = get<1>(rotationBestScores.begin()->second);
	this->gMatches = permutations.at(get<2>(rotationBestScores.begin()->second));
	return true;
}

//Helper for M2 and M1 when using brute force
//This uses ordered maps because unordered_map takes 2-3x the memory for n=9
std::vector<std::map<int, int>> InitialGuesstimator::getAllCombinations() const
{
	const int minSegSize = std::min(seg1Map.size(), seg2Map.size());
	if (minSegSize > bruteForceMaxN) {
		cout << "Initial Guess: Too many Segments to brute force this.\n";
		return std::vector<std::map<int, int>>();
	}
	std::vector<int> aVec {};
	aVec.reserve(seg1Map.size());
	for (const auto& entry : seg1Map) {
		aVec.push_back(entry.first);
	}
	std::vector<int> bVec {};
	bVec.reserve(seg2Map.size());
	for (const auto& entry : seg2Map) {
		bVec.push_back(entry.first);
	}
	return getAllCombinations(aVec, bVec);
}


//Helper for M2 and M1 when using brute force; recursive
//This uses ordered maps because unordered_map takes 2-3x the memory for n=9
std::vector<std::map<int, int>> InitialGuesstimator::getAllCombinations(const std::vector<int>& aVec, const std::vector<int>& bVec) const
{
	std::vector<std::map<int, int>> ret {};
	if (aVec.empty() || bVec.empty()) {
		return std::vector<std::map<int, int>>();
	}
	else if (aVec.size() == 1 && bVec.size() == 1) {
		ret.push_back({ { aVec[0], bVec[0] } });
		return ret;
	}
	else if (aVec.size() == 1 && bVec.size() > 1) {
		for (size_t i = 0; i < bVec.size(); ++i) {
			ret.push_back({ { aVec[0], bVec[i] } });
		}
		return ret;
	}
	else if (aVec.size() > 1 && bVec.size() == 1) {
		for (size_t i = 0; i < aVec.size(); ++i) {
			std::map<int, int> retMap = { {aVec[i], bVec[0] } };
			for (size_t n = 0; n < aVec.size(); ++n) {
				if (n != i) {
					retMap.insert({ aVec[n],-1 });
				}
			}
			ret.push_back(retMap);
		}
		return ret;
	}
	else
	{
		//Try to reserve enough memory for all combinations, but at most 9! (9 factorial) to lower risk of possible bad_alloc exceptions.
		const auto reserveAttempt = CustomGuessHelpers::factorial(std::min(static_cast<unsigned int>(aVec.size()), static_cast<unsigned int>(9)));
		ret.reserve(reserveAttempt);
		// Pick the first element of aVec
		// Choose all possible combinations with bVec elements
		const int aChoice = aVec[0];
		const auto aVecCopy = std::vector<int>(aVec.begin() + 1, aVec.end());
		for (std::uint32_t i = 0; i < bVec.size(); ++i) {
			//Current Combination: aVec[0] with bVec[i]
			const int bChoice = bVec[i];
			std::vector<int> bVecCopy{ bVec };

			if (i != bVecCopy.size() - 1) {
				std::swap(bVecCopy[i], bVecCopy[bVecCopy.size() - 1]);
			}
			//Swap + Pop_back is much much faster than using "erase"
			bVecCopy.pop_back();

			//Recursive Call
			// Suprisingly, implementing a version were bVecCopy was moved instead wasn't really faster at all.
			auto allPoss = getAllCombinations(aVecCopy, bVecCopy);

			if (!allPoss.empty()) {
				//Insert the choice we made before we made the recursive call
				for (auto& elemMap : allPoss) {
					elemMap.insert({ aChoice, bChoice });
				}
				//This is the std::move from <algorithm>, not from <utility>
				std::move(allPoss.begin(), allPoss.end(), std::back_inserter(ret));
			}
			else {
				ret.push_back({ { aChoice, bChoice } });
			}
		}
		return ret;
	}
}

//Gets Angles (i.e. "direction relations") from each segment to the other segments in that map
std::map<int, std::vector<float>> InitialGuesstimator::getAngleMap(const std::map<int, Segment>& segMap)
{
	std::map<int, std::vector<float>> segAngles {};
	for (const auto& seg : segMap) {
		//Might be worth a check if this works better with ABBCenter
		const auto centerI = seg.second.getHullCenter(); 
		const Eigen::Vector2f centerI2D{ centerI.x(), centerI.z() };
		for (const auto& seg2 : segMap) {
			if (seg.first != seg2.first) {
				const auto centerN = seg2.second.getHullCenter();
				Eigen::Vector2f centerN2D{ centerN.x(), centerN.z() };
				centerN2D = (centerN2D - centerI2D).normalized();
				segAngles[seg.first].push_back(std::atan2f(centerN2D.y(), centerN2D.x()));
			}
		}
	}
	for (auto& entry : segAngles) {
		std::sort(entry.second.begin(), entry.second.end());
	}
	return segAngles;
}

template<class T>
inline void rotateMapAngles(T& mapT, float step) {
	for (auto& entry : mapT) {
		for (auto& angle : entry.second) {
			angle += step;
			if (angle > 180.0f) {
				angle -= 360.0f;
			}
			else if (angle < -180.0f) {
				angle += 360.0f;
			}
		}
	}
}

//Searches for best Matching Config for different Rotations (based on obj var angleStepSize) for their similarity w.r.t. direction relations.
//Returns a collection of best AngleDistanceValue and best corresp. Matches for each checked rotation.
std::vector<std::pair<float, std::map<int, int>>> InitialGuesstimator::getAngleAndMatchesGreedy(const std::map<int, std::vector<float>>& segAngles1, const std::map<int, std::vector<float>>& segAngles2) const
{
	if (segAngles1.empty() || segAngles2.empty()) {
		//TODO add warning/log
		return std::vector<std::pair<float, std::map<int, int>>>();
	}
	//Will be used to hold/save intermediate results.
	// In particular: Used because here we can use std::sort as we'd like.
	std::vector<std::tuple<float, float, std::map<int, int>>> resVec {};
	const int stepCount = static_cast<int>(360.0f / angleStepSize);
	resVec.reserve(stepCount);

	//Copy to be able to rotate stuff around
	auto map2Copy{ segAngles2 };
	//i=1 because else we'd try 360� at the end.
	for (unsigned int i = 1; i < stepCount; ++i) { 
		
		rotateMapAngles(map2Copy, static_cast<float>(stepCount));

		auto diffScorePair = getDiffAngleSumMapWithMatch(segAngles1, map2Copy);
		auto diffScorePair2 = getDiffAngleSumMapWithMatchReverse(segAngles1, map2Copy);

		if ((diffScorePair.first <= -1.0f && diffScorePair2.first > -1.0f) || (diffScorePair2.first < diffScorePair.first && diffScorePair2.first > -1.0f)) {
			diffScorePair = std::move(diffScorePair2);
		}
		if (diffScorePair.first > -1.0f) {
			resVec.push_back({ diffScorePair.first, angleStepSize * static_cast<float>(i),  diffScorePair.second });
		}
	}
	//Return the sortet results.
	std::sort(resVec.begin(), resVec.end());
	std::vector<std::pair<float, std::map<int, int>>> resVecReduced {};
	resVecReduced.reserve(resVec.size());

	for (size_t i = 0; i < resVec.size(); ++i) {
		auto& pMap = std::get<2>(resVec[i]);
		//Add -1 matches for unmatched segments in view 1
		if (pMap.size() < segAngles1.size()) {
			for (const auto& tSeg1s : segAngles1) {
				if (pMap.count(tSeg1s.first) == 0) {
					pMap.insert({ tSeg1s.first, -1 });
				}
			}
		}
		resVecReduced.push_back({ std::get<1>(resVec[i]), std::get<2>(resVec[i]) });
	}

	return resVecReduced;
}

//Tests all Matching Permutations and Rotations (based on obj var angleStepSize) for their similarity w.r.t. direction relations
//Returns a collection of best AngleDistanceValue and best corresp. Matches for each checked rotation.
std::vector<std::pair<float, std::map<int, int>>> InitialGuesstimator::getAngleAndMatchesBruteForce(const std::map<int, std::vector<float>>& segAngles1, 
																									const std::map<int, std::vector<float>>& segAngles2) const
{
	if (segAngles1.empty() || segAngles2.empty()) {
		cout << "WARNING INITIAL GUESS: Angles Empty - Can't determine match through rotation relations.\n";
		return std::vector<std::pair<float, std::map<int, int>>>();
	}

	auto permutations = getAllCombinations();
	if (permutations.size() == 0) {
		cout << "WARNING INITIAL GUESS: Number of possible match permutations returned 0. Guess Method 1 Unsuccessful\n";
		return std::vector<std::pair<float, std::map<int, int>>>();
	}
	
	//Will be used to hold/save intermediate results.
	std::vector<std::tuple<float, float, int>> resVec {};
	const int stepCount = static_cast<int>(360.0f / angleStepSize);
	resVec.reserve(stepCount);

	//Copy to be able to rotate stuff around
	auto map2Copy{ segAngles2 };
	//i=1 because else we'd try 360� at the end.
	for (unsigned int i = 1; i < stepCount; ++i) {
		rotateMapAngles(map2Copy, angleStepSize);
		const auto diffScorePair = getDiffAngleSumMapMinForPermutationsT(segAngles1, map2Copy, permutations);
		if (diffScorePair.first > -1.0f && diffScorePair.second != -1) {
			resVec.push_back({ diffScorePair.first, angleStepSize * static_cast<float>(i),  diffScorePair.second });
		}
	}
	//Sort the results -> Best Rotation+Matching at the front
	std::vector<std::pair<float, std::map<int, int>>> resVecReduced {};
	resVecReduced.reserve(resVec.size());
	std::sort(resVec.begin(), resVec.end());
	for (size_t i = 0; i < resVec.size(); ++i) {
		const auto targetPermutation = std::get<2>(resVec[i]);
		if (targetPermutation < permutations.size() && targetPermutation >= 0) {
			if (permutations[targetPermutation].size() < segAngles1.size()) {
				for (const auto& tSeg1s : segAngles1) {
					if (permutations[targetPermutation].count(tSeg1s.first) == 0) {
						permutations[targetPermutation].insert({ tSeg1s.first, -1 });
					}
				}
			}
			resVecReduced.push_back({ std::get<1>(resVec[i]), permutations[targetPermutation] });
		}
	}
	return resVecReduced;
}


std::pair<float, std::map<int, int>> InitialGuesstimator::getDiffAngleSumMapWithMatch(const std::map<int, std::vector<float>>& segAngles1, 
																						const std::map<int, std::vector<float>>& segAngles2) const
{
	if (segAngles1.empty() || segAngles2.empty()) {
		return { -1.1f, std::map<int,int>() };
	}
	std::vector<int> alreadyTaken {};
	alreadyTaken.reserve(segAngles2.size());
	float minSumHypoTotal = 0.0f;
	int idBest = -1;

	std::map<int, int> matches {};
	for (const auto& angleS1 : segAngles1) {
		if (alreadyTaken.size() >= segAngles2.size()) {
			//There are more segments in segAngles1 than in segAngles2, thus there are not enough for all to have a match.
			// For the "hanging" last segment(s), we don't have a good way of matching.
			matches.insert({ angleS1.first, -1 });
		}
		else {
			//2*worst case as the initial value to ensure at least one is better/lower
			float localMin = (angleS1.second.size() + 1) * 360.f; 
			for (const auto& seg2Entry : segAngles2) {
				//If not already taken, check how "good" this match would be.
				if (std::find(alreadyTaken.begin(), alreadyTaken.end(), seg2Entry.first) == alreadyTaken.end()) {
					const auto sumSingle = getDiffAngleSumSingle(angleS1.second, seg2Entry.second);
					if (sumSingle < localMin && sumSingle > -1.0f) {
						localMin = sumSingle;
						idBest = seg2Entry.first;
					}
				}
			}
			minSumHypoTotal += localMin;
			matches[angleS1.first] = idBest;
			if (idBest != -1) {
				alreadyTaken.push_back(idBest);
			}
			idBest = -1;
		}
	}
	return { minSumHypoTotal, matches };
}

//Some Same Code as without Reverse, but taken apart as a test for now.
//Due to the outer loop being different, it is challening to not have this duplicate code here.
std::pair<float, std::map<int, int>> InitialGuesstimator::getDiffAngleSumMapWithMatchReverse(const std::map<int, std::vector<float>>& segAngles1, 
																				const std::map<int, std::vector<float>>& segAngles2) const
{
	if (segAngles1.empty() || segAngles2.empty()) {
		return { -1.1f, std::map<int,int>() };
	}
	std::map<int, int> matches {};
	std::vector<int> alreadyTaken {};
	alreadyTaken.reserve(segAngles2.size());
	float minSumHypoTotal = 0.0f;
	int idBest = -1;
	for (auto cITS1 = segAngles1.crbegin(); cITS1 != segAngles1.crend(); ++cITS1) {
		if (alreadyTaken.size() >= segAngles2.size()) {
			//There are more segments in segAngles1 than in segAngles2, thus there are not enough for all to have a match.
			// For the "hanging" last segment(s), we thus don't have a good way of matching.
			matches.insert({ cITS1->first, -1 });
		}
		else {
			//2*worst case as the initial value to ensure at least one is better/lower
			float localMin = (cITS1->second.size() + 1) * 360.f; 
			
			for (const auto& seg2Entry : segAngles2) {
				//If not already taken, check how "good" this match would be.
				if (std::find(alreadyTaken.begin(), alreadyTaken.end(), seg2Entry.first) == alreadyTaken.end()) {
					const float sumSingle = getDiffAngleSumSingle(cITS1->second, seg2Entry.second);
					if (sumSingle < localMin && sumSingle > -1.0f) {
						localMin = sumSingle;
						idBest = seg2Entry.first;
					}
				}
			}
			minSumHypoTotal += localMin;
			matches[cITS1->first] = idBest;
			if (idBest != -1) {
				alreadyTaken.push_back(idBest);
			}
			idBest = -1;
		}
	}
	return { minSumHypoTotal, matches };
}

//Returns the Difference Sum between the Angles contained in the two input vectors.
//Assumes that the angle vectors are sorted
float InitialGuesstimator::getDiffAngleSumSingle(const std::vector<float>& angles1, const std::vector<float>& angles2) const
{
	const auto minLength = std::min(angles1.size(), angles2.size());
	//If one segment has more angles, ignore. Naive choice.
	float sumDiffs = 0.0f;
	for (size_t i = 0; i < minLength; ++i) {
		float diff = std::fabsf(angles1[i] - angles2[i]);
		diff = (diff > 180.0f) ? 360.0f - diff : diff;
		//If difference is small, set it to 0. This is superior in accuracy based on my tests
		// than just not doing anything or using some kind of squared differences.
		if (diff < (angleStepSize / 1.5f)) {
			diff = 0.0f;
		}
		sumDiffs += diff;
	}
	return sumDiffs;
}

//ONLY USE THIS AFTER METHOD 1 HAS SET THE gAngles, gTranslation, gMatches MEMBERS!!!!
//Checks if we can improve the sum of distances between matched segments by swapping matches.
// Only swaps if the the two swap candidates are the "ideal" candidates, so this does not always
// result in the lowest sum theoretically possible.
void InitialGuesstimator::optimizeM1MatchesBasedOnPosition()
{
	//cout << "Optimize M1 Matches\n";
	if (seg1Map.empty() || seg2Map.empty() || gMatches.empty()) {
		cout << "WARNING: Cannot optimize matches if either seg1Map, seg2Map or gMatches is empty! Nothing was changed.\n";
		return;
	}
	//Apply the rotation to a copy of the segments from view 2.
	std::map<int, Segment> seg2MapWithAlignment{ seg2Map };
	for (auto it = seg2MapWithAlignment.begin(); it != seg2MapWithAlignment.end(); ++it) {
		it->second.rotateThenTranslateSegment(Vector3f::UnitY(), DEG2RAD(-gAngle), gTranslation);
	}
	//Need to record: Match isnt the closest one. Which one is the closest one?
	//ID for Seg1, ID for current match in Seg2Map, Best ID from Seg2Map
	std::vector<std::tuple<int, int, int>> swapWishes {};
	
	for (const auto& seg1 : seg1Map) {
		if (gMatches.count(seg1.first) == 0) {
			//This Segment in View 1 does not have a designated match.
			// Exempt it from swapping as this is difficult to handle in a way that makes sense for segment matching.
			continue;
		}
		std::vector<std::pair<float, int>> distToIdVec {};
		distToIdVec.reserve(seg2MapWithAlignment.size());
		for (const auto& seg2 : seg2MapWithAlignment) {
			const float dist = (useABBs) ? (seg1.second.getABBMiddle() - seg2.second.getABBMiddle()).norm() : (seg1.second.getHullCenter() - seg2.second.getHullCenter()).norm();
			distToIdVec.push_back({ dist, seg2.second.getId() });
		}
		std::sort(distToIdVec.begin(), distToIdVec.end());
		float distToMatch = 0.0f;
		int matchPosition = 0;
		for (int i = 0; i < distToIdVec.size(); ++i) {
			if (distToIdVec[i].second == gMatches.at(seg1.first)) {
				distToMatch = distToIdVec[i].first;
				matchPosition = i;
			}
		}
		if (distToIdVec[0].second != gMatches.at(seg1.first) && gMatches.at(seg1.first) != -1) {
			swapWishes.push_back({ seg1.first, gMatches.at(seg1.first), distToIdVec[0].second });
		}
	}
	//cout << "Number of wishes: " << swapWishes.size() << "\n";
	int sCounter = 0; //will be reset when size changes
	int performedMatchesCounter = 0;
	while (swapWishes.size() > 1 && sCounter <= swapWishes.size())
	{
		sCounter++;
		const std::tuple<int, int, int> firstEntry = swapWishes[0];
		bool foundMatch = false;
		int matchpos = 0;
		for (size_t n = 1; n < swapWishes.size(); ++n)
		{
			const auto& matchEntry = swapWishes[n];
			if (get<2>(firstEntry) == get<1>(matchEntry) && get<2>(matchEntry) == get<1>(firstEntry)) {
				if (verbose) {
					cout << "Re-Match: V1S " << get<0>(firstEntry) << " gets V2S " << get<2>(firstEntry) << ", V1S " << get<0>(matchEntry) << " gets V2S " << get<2>(matchEntry) << "\n";	
				}
				const auto tempSave = gMatches[get<0>(firstEntry)];
				gMatches[get<0>(firstEntry)] = gMatches[get<0>(matchEntry)];
				gMatches[get<0>(matchEntry)] = tempSave;
				foundMatch = true;
				matchpos = n;
				performedMatchesCounter++;
				break; //BREAKS THE INNER (FOR) LOOP
			}
		}
		if (foundMatch) {
			//Remove the matched one and then the firstEntry.
			swapWishes.erase(swapWishes.begin() + matchpos);
			sCounter = 0;
		}
		else {
			if (swapWishes.size() == 2) {
				// only 1 possibility but no match in this iteration -> Stop trying.
				break;  //BREAKS THE WHILE LOOP
			}
			else {
				//Move the first element to last spot in Vector so that a different one will be tested first next loop.
				std::swap(swapWishes[0], swapWishes[swapWishes.size() - 1]);
			}
		}
	}
	//cout << "Optimize M1 Matches Finished\n";
}

//Minimizes the sum of differences of Positions between the pairs of Vector3f's as given in the first parameter.
//DEPRECATED
std::pair<float, Eigen::Vector3f> InitialGuesstimator::optimizeVecResult(const std::vector<std::pair<Eigen::Vector3f, Eigen::Vector3f>>& centerVecPairsT, int maxIterations) const
{
	if (centerVecPairsT.empty()) {
		auto ret = std::pair<float, Vector3f>();
		ret.first = -1.1f;
		return ret;
	}
	std::vector<std::pair<Vector3f, Vector3f>> centerVecPairs{ centerVecPairsT };
	int optCount = 0;
	float thresholdDist = 0.0001f;
	float lastTranslatDistSq = 1.0f;
	Vector3f totalTranslation{ 0.0f, 0.0f, 0.0f };
	while (optCount < maxIterations && lastTranslatDistSq > thresholdDist)
	{
		optCount++;
		Vector3f avgDiff{ 0.0f, 0.0f, 0.0f };
		for (std::uint32_t n = 0; n < centerVecPairs.size(); ++n) {
			avgDiff += (centerVecPairs[n].first - centerVecPairs[n].second);
		}
		avgDiff = avgDiff / static_cast<float>(centerVecPairs.size());
		for (std::uint32_t n = 0; n < centerVecPairs.size(); ++n) {
			centerVecPairs[n].second = centerVecPairs[n].second + avgDiff;
		}
		lastTranslatDistSq = avgDiff.squaredNorm();
		totalTranslation += avgDiff;
	}
	float finalSumDists = 0.0f;
	for (std::uint32_t n = 0; n < centerVecPairs.size(); ++n) {
		//finalSumDists = finalSumDists + ((centerVecPairs[n].first - centerVecPairs[n].second).norm());
		finalSumDists += ((centerVecPairs[n].first - centerVecPairs[n].second).squaredNorm());
	}
	return { finalSumDists, totalTranslation };
}

//Minimizes the sum of differences of Positions between the pairs of Vector3f's as given in the first parameter.
std::pair<float, Eigen::Vector3f> InitialGuesstimator::optimizeVecResult(std::vector<std::pair<Eigen::Vector3f, Eigen::Vector3f>>&& centerVecPairsT, int maxIterations) const
{
	auto localCenterVecPairs = std::vector<std::pair<Eigen::Vector3f, Eigen::Vector3f>>{ std::move(centerVecPairsT) };
	if (localCenterVecPairs.empty()) {
		return { -1.1f,Vector3f{} };
	}
	const float thresholdDist = 0.001f;
	Vector3f totalTranslation{ 0.0f, 0.0f, 0.0f };
	for (int i = 0; i < maxIterations; ++i) {
		Vector3f avgDiff{ 0.0f, 0.0f, 0.0f };
		for (unsigned int n = 0; n < localCenterVecPairs.size(); ++n) {
			avgDiff += (localCenterVecPairs[n].first - localCenterVecPairs[n].second);
		}
		avgDiff = avgDiff / static_cast<float>(localCenterVecPairs.size());
		for (unsigned int n = 0; n < localCenterVecPairs.size(); ++n) {
			localCenterVecPairs[n].second += avgDiff;
		}
		if (avgDiff.squaredNorm() < thresholdDist) {
			break;
		}
		totalTranslation += avgDiff;
	}
	float finalSumDists = 0.0f;
	for (unsigned int n = 0; n < localCenterVecPairs.size(); ++n) {
		finalSumDists += ((localCenterVecPairs[n].first - localCenterVecPairs[n].second).squaredNorm());
	}
	return { finalSumDists, totalTranslation };
}


//Pass 1 for method one and 2 for method two. Throws a runtime exception if it's something else.
void InitialGuesstimator::setUseMethodOneOrTwo(unsigned int methodNumber)
{
	if (methodNumber == 1) {
		this->useMethod2 = false;
	}
	else if (methodNumber == 2) {
		this->useMethod2 = true;
	}
	else {
		cout << "ERROR: NOT ALLOWED TO SET A METHOD OTHER THAN '1' or '2' FOR INITIALGUESSTIMATOR\n";
		std::throw_with_nested(std::invalid_argument("Attempt to set a guess method != 1 and != 2 in InitialGuesstimator; not allowed"));
	}
}

void InitialGuesstimator::setUseGreedySearchForM1(bool prefer)
{
	greedy = prefer;
}

void InitialGuesstimator::setUseABBsOverCentroids(bool preferABBs)
{
	this->useABBs = preferABBs;
}

void InitialGuesstimator::setMaximumNumberOfSegmentsToBruteForce(unsigned int maxNumberToBruteForce)
{
	this->bruteForceMaxN = maxNumberToBruteForce;
}

void InitialGuesstimator::setAngleStepSize(float angleStepSize)
{
	this->angleStepSize = angleStepSize;
}

void InitialGuesstimator::setVerbosity(bool verbosity)
{
	this->verbose = verbosity;
}

void InitialGuesstimator::setAllowChangeOfYPosition(bool allowYPosChange)
{
	this->allowChangeYPosition = allowYPosChange;
}
