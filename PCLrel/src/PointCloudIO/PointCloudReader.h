/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#pragma once
#ifndef PointCloudReader_H
#define PointCloudReader_H
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <string>

static class PointCloudReader
{
public:

	//TODO template all of these.

	static pcl::PointCloud<pcl::PointXYZ> getSimpleCloudFromPCD(const std::string& filename);
	static pcl::PointCloud<pcl::PointXYZ> getSimpleCloudFromPLY(const std::string& filename);
	static pcl::PointCloud<pcl::PointXYZRGB> getColorCloudFromPCD(const std::string& filename);
	static pcl::PointCloud<pcl::PointXYZRGB> getColorCloudFromPLY(const std::string& filename);
	static pcl::PointCloud<pcl::PointNormal> getPointNormalCloudFromPLY(const std::string &filename);
	static pcl::PointCloud<pcl::PointXYZRGBNormal> getPointNormalColorCloudFromPLY(const std::string& filename);
};
#endif /* PointCloudReader_H */
