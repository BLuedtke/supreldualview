/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#pragma once
#ifndef BLENDERVIEWGENERATOR_H
#define BLENDERVIEWGENERATOR_H
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <string>
#include <vector>

class BlenderViewGenerator
{
public:
	void loadBlenderScene(const std::string& filepathPLYFILE);
	void loadBlenderSegments(const std::string& filepathFOLDERLOCATION);
	void reset();
	
	pcl::PointCloud<pcl::PointXYZRGBNormal> getViewOnScene(const Eigen::Vector3f& viewPointTranslate, float viewYAngleRotationDEG, float noiseStrengthN, float noiseStrengthP);
	pcl::PointCloud<pcl::PointXYZRGBNormal> getFullScene(float noiseStrengthN, float noiseStrengthP) const;

	std::vector<pcl::PointCloud<pcl::PointXYZRGBNormal>> getViewOnSegments(const Eigen::Vector3f& viewPointTranslate, float viewYAngleRotationDEG, float noiseStrengthN, float noiseStrengthP) const;
	std::vector<pcl::PointCloud<pcl::PointXYZRGBNormal>> getFullSegments(float noiseStrengthN, float noiseStrengthP) const;
	
	void setHiddenPointRemovalSensitivity(float sensitivity);
	float getHiddenPointRemovalSensitivity() const;

	void setThreadCount(unsigned int threads);
	unsigned int getThreadCount() const;

private:
	bool wholeSceneMode{ true };
	pcl::PointCloud<pcl::PointXYZRGBNormal> sceneCloud{};
	std::vector<pcl::PointCloud<pcl::PointXYZRGBNormal>> segmentClouds{};
	
	float hiddenPtSensitivity = 0.004f; //Magic number, sorry
	unsigned int threadCount = 8; //TODO think about a better default

	pcl::PointCloud<pcl::PointXYZRGBNormal> getViewOnPCloud(const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud, const Eigen::Vector3f& viewPointTranslate, float viewYAngleRotationDEG, float noiseStrengthN, float noiseStrengthP) const;
	pcl::PointCloud<pcl::PointXYZRGBNormal> getViewOnSegment(int index, const Eigen::Vector3f& viewPtTranslate, float viewYAngleRotDEG, float noiseStrN, float noiseStrP) const;
};
#endif /* BLENDERVIEWGENERATOR_H */
