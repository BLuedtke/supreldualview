/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#include "PointCloudReader.h"
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>

pcl::PointCloud<pcl::PointXYZ> PointCloudReader::getSimpleCloudFromPCD(const std::string& filename)
{
	using PointT = pcl::PointXYZ;
	pcl::PCLPointCloud2 loadedPointCloud;
	if (pcl::io::loadPCDFile(filename, loadedPointCloud) < 0) {
		std::cout << "Error loading point cloud " << filename << std::endl;
		return pcl::PointCloud<PointT>();
	}
	pcl::PointCloud<PointT> outputCloud{};
	pcl::fromPCLPointCloud2(loadedPointCloud, outputCloud);
	outputCloud.resize(outputCloud.size());
	return outputCloud;
}

pcl::PointCloud<pcl::PointXYZ> PointCloudReader::getSimpleCloudFromPLY(const std::string& filename)
{
	using PointT = pcl::PointXYZ;
	pcl::PCLPointCloud2 loadedPointCloud;
	if (pcl::io::loadPLYFile(filename, loadedPointCloud) < 0) {
		std::cout << "Error loading point cloud " << filename << std::endl;
		return pcl::PointCloud<PointT>();
	}
	pcl::PointCloud<PointT> outputCloud{};
	pcl::fromPCLPointCloud2(loadedPointCloud, outputCloud);
	outputCloud.resize(outputCloud.size());
	return outputCloud;
}

pcl::PointCloud<pcl::PointXYZRGB> PointCloudReader::getColorCloudFromPCD(const std::string& filename)
{
	using PointT = pcl::PointXYZRGB;
	pcl::PCLPointCloud2 loadedPointCloud;
	if (pcl::io::loadPCDFile(filename, loadedPointCloud) < 0) {
		std::cout << "Error loading point cloud " << filename << std::endl;
		return pcl::PointCloud<PointT>();
	}
	pcl::PointCloud<PointT> outputCloud{};
	pcl::fromPCLPointCloud2(loadedPointCloud, outputCloud);
	outputCloud.resize(outputCloud.size());
	return outputCloud;
}

pcl::PointCloud<pcl::PointXYZRGB> PointCloudReader::getColorCloudFromPLY(const std::string& filename)
{
	using PointT = pcl::PointXYZRGB;
	pcl::PCLPointCloud2 loadedPointCloud;
	if (pcl::io::loadPLYFile(filename, loadedPointCloud) < 0) {
		std::cout << "Error loading point cloud " << filename << std::endl;
		return pcl::PointCloud<PointT>();
	}
	pcl::PointCloud<PointT> outputCloud{};
	pcl::fromPCLPointCloud2(loadedPointCloud, outputCloud);
	outputCloud.resize(outputCloud.size());
	return outputCloud;
}

pcl::PointCloud<pcl::PointNormal> PointCloudReader::getPointNormalCloudFromPLY(const std::string& filename)
{
	using PointN = pcl::PointNormal;
	pcl::PCLPointCloud2 loadedPointCloud;
	if (pcl::io::loadPLYFile(filename, loadedPointCloud) < 0) {
		std::cout << "Error loading point cloud " << filename << std::endl;
		return pcl::PointCloud<PointN>();
	}
	pcl::PointCloud<PointN> outputCloud{};
	pcl::fromPCLPointCloud2(loadedPointCloud, outputCloud);
	outputCloud.resize(outputCloud.size());
	return outputCloud;
}


pcl::PointCloud<pcl::PointXYZRGBNormal> PointCloudReader::getPointNormalColorCloudFromPLY(const std::string& filename)
{
	using PointN = pcl::PointXYZRGBNormal;
	pcl::PCLPointCloud2 loadedPointCloud;
	if (pcl::io::loadPLYFile(filename, loadedPointCloud) < 0) {
		std::cout << "Error loading point cloud " << filename << std::endl;
		return pcl::PointCloud<PointN>();
	}
	pcl::PointCloud<PointN> outputCloud{};
	pcl::fromPCLPointCloud2(loadedPointCloud, outputCloud);
	outputCloud.resize(outputCloud.size());
	return outputCloud;
}