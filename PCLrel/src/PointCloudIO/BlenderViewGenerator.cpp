/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#include "BlenderViewGenerator.h"
#include "../Surface/SurfaceProcessor.h";
#include "../Modifiers/Transforms.h";
#include "../Modifiers/PointRemoval.h";
#include "PointCloudReader.h"

using Eigen::Vector3f;
using std::cout;

void BlenderViewGenerator::loadBlenderScene(const std::string& filepathPLYFILE)
{
	wholeSceneMode = true;
	sceneCloud.clear();
	segmentClouds.clear();
	sceneCloud = PointCloudReader::getPointNormalColorCloudFromPLY(filepathPLYFILE);
	sceneCloud = PointRemoval::removeSpatiallyDuplicatePoints(sceneCloud);
	if (sceneCloud.empty()) {
		cout << "BlenderViewGenerator loadBlenderScene ERROR | UNABLE TO LOAD A VALID SCENE WITH THE PATH GIVEN.\n";
	}
}

//Assumes that the first segment cloud in the folder is called "Segment1.ply"
//Do NOT include a trailing slash.
//TODO convert this to std::filesystem::path
void BlenderViewGenerator::loadBlenderSegments(const std::string& filepathFOLDERLOCATION)
{
	wholeSceneMode = false;
	sceneCloud.clear();
	segmentClouds.clear();
	
	std::string base = filepathFOLDERLOCATION;
	if (base.find_last_of("/") == base.size() - 1) {
		base += "Segment";
	} else {
		base += "/Segment";
	}
	bool found = true;
	int counter = 0;
	do
	{
		++counter;
		auto sCloud = PointCloudReader::getPointNormalColorCloudFromPLY(base + std::to_string(counter) + ".ply");
		if (sCloud.empty()) {
			found = false;
		} else {
			sCloud = PointRemoval::removeSpatiallyDuplicatePoints(sCloud);
			segmentClouds.push_back(std::move(sCloud));
		}
	} while (found);
	cout << "BlenderViewGenerator loadBlenderSegments: Loaded " << counter << " segments.\n";
}


void BlenderViewGenerator::reset()
{
	cout << "BlenderViewGenerator: reset\n";
	wholeSceneMode = true;
	sceneCloud.clear();
	segmentClouds.clear();
}

pcl::PointCloud<pcl::PointXYZRGBNormal> BlenderViewGenerator::getViewOnScene(const Eigen::Vector3f& viewPointTranslate, float viewYAngleRotationDEG, float noiseStrengthN, float noiseStrengthP)
{
	if (sceneCloud.empty()) {
		cout << "BlenderViewGenerator: getViewOnScene WARNING | NO SCENE WAS LOADED AND INITIALIZED.\n";
		return pcl::PointCloud<pcl::PointXYZRGBNormal>();
	}
	if (!wholeSceneMode) {
		cout << "BlenderViewGenerator: getViewOnScene WARNING | CALLED getViewOnScene BUT SEGMENT FOLDER WAS LOADED, NOT A SCENE.\n";
		return pcl::PointCloud<pcl::PointXYZRGBNormal>();
	}
	//auto view{ getViewOnPCloud(sceneCloud, viewPointTranslate, viewYAngleRotationDEG, noiseStrengthN, noiseStrengthP) };
	/*
	if (view.empty()) {
		std::cerr << "BlenderViewGenerator: getViewOnScene ERROR | FAILED GENERATION - Returning empty.\n";
	}/**/
	return getViewOnPCloud(sceneCloud, viewPointTranslate, viewYAngleRotationDEG, noiseStrengthN, noiseStrengthP);
}

pcl::PointCloud<pcl::PointXYZRGBNormal> BlenderViewGenerator::getFullScene(float noiseStrengthN, float noiseStrengthP) const
{
	if (sceneCloud.empty()) {
		cout << "BlenderViewGenerator: getFullScene WARNING | NO SCENE WAS LOADED AND INITIALIZED.\n";
		return pcl::PointCloud<pcl::PointXYZRGBNormal>();
	}
	if (!wholeSceneMode) {
		cout << "BlenderViewGenerator: getFullScene WARNING | CALLED getViewOnScene BUT SEGMENT FOLDER WAS LOADED, NOT A SCENE.\n";
		return pcl::PointCloud<pcl::PointXYZRGBNormal>();
	}
	if (std::fabsf(noiseStrengthN) > 0.0f || std::fabsf(noiseStrengthP) > 0.0f) {
		return SurfaceProcessor::generateNoisySurfaceFromNormals(sceneCloud, 0.01f, noiseStrengthN, noiseStrengthP);
	}
	else {
		return sceneCloud;
	}
}


std::vector<pcl::PointCloud<pcl::PointXYZRGBNormal>> BlenderViewGenerator::getViewOnSegments(const Eigen::Vector3f& viewPointTranslate, float viewYAngleRotationDEG, float noiseStrengthN, float noiseStrengthP) const
{
	if (segmentClouds.empty()) {
		cout << "BlenderViewGenerator: getViewOnSegments WARNING | NO SEGMENT FOLDER WAS LOADED AND INITIALIZED.\n";
	}
	if (wholeSceneMode) {
		cout << "BlenderViewGenerator: getViewOnSegments WARNING | CALLED getViewOnSegments BUT SCENE WAS LOADED, NOT A SEGMENT FOLDER.\n";
	}
	std::vector<pcl::PointCloud<pcl::PointXYZRGBNormal>> retVec{};
	retVec.reserve(segmentClouds.size());
	for (int i = 0; i < segmentClouds.size(); ++i) {
		//Empty clouds SHOULD be pushed back here, otherwise can't perform matching later (count would be off).
		retVec.push_back(getViewOnSegment(i, viewPointTranslate, viewYAngleRotationDEG, noiseStrengthN, noiseStrengthP));
	}
	if (retVec.empty()) {
		cout << "BlenderViewGenerator: getViewOnSegments WARNING | Internal Error: Returning Empty Segment Cloud Vector due to unknown reason.\n";
	}
	return retVec;
}


std::vector<pcl::PointCloud<pcl::PointXYZRGBNormal>> BlenderViewGenerator::getFullSegments(float noiseStrengthN, float noiseStrengthP) const
{
	if (segmentClouds.empty()) {
		cout << "BlenderViewGenerator: getFullSegments WARNING | NO SEGMENT FOLDER WAS LOADED AND INITIALIZED.\n";
	}
	if (wholeSceneMode) {
		cout << "BlenderViewGenerator: getFullSegments WARNING | CALLED getFullSegments BUT SCENE WAS LOADED, NOT A SEGMENT FOLDER.\n";
	}
	std::vector<pcl::PointCloud<pcl::PointXYZRGBNormal>> retVec{};
	retVec.reserve(segmentClouds.size());
	for (int i = 0; i < segmentClouds.size(); ++i) {
		if (std::fabsf(noiseStrengthN) > 0.0f || std::fabsf(noiseStrengthP) > 0.0f) {
			retVec.push_back(SurfaceProcessor::generateNoisySurfaceFromNormals(segmentClouds.at(i), 0.01f, noiseStrengthN, noiseStrengthP));
		}
		else {
			retVec.push_back(segmentClouds.at(i));
		}
	}
	if (retVec.empty()) {
		cout << "BlenderViewGenerator: getFullSegments WARNING | Internal Error: Returning Empty Segment Cloud Vector due to unknown reason.\n";
	}
	return retVec;
}


pcl::PointCloud<pcl::PointXYZRGBNormal> BlenderViewGenerator::getViewOnPCloud(const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud, const Eigen::Vector3f& viewPointTranslate, float viewYAngleRotationDEG, float noiseStrengthN, float noiseStrengthP) const
{
	if (inputCloud.empty()) {
		return pcl::PointCloud<pcl::PointXYZRGBNormal>();
	}
	pcl::PointCloud<pcl::PointXYZRGBNormal> view{ inputCloud };
	if (std::fabsf(noiseStrengthN) > 0.0f || std::fabsf(noiseStrengthP) > 0.0f) {
		view = SurfaceProcessor::generateNoisySurfaceFromNormals(view, 0.01f, noiseStrengthN, noiseStrengthP);
	}
	view = Transforms::translatePCloudWithNormals(view, 1, 0, 1);
	view = Transforms::rotatePCloudAroundAxisWithNormals(view, Eigen::Vector3f::UnitY(), DEG2RAD(viewYAngleRotationDEG));
	view = Transforms::translatePCloudWithNormals(view, -1, 0, -1);
	view = Transforms::translatePCloudWithNormals(view, -viewPointTranslate);
	view = PointRemoval::backfaceCulling(view, 0.02f);
	//Interestingly, this method is faster when using more threads than actually available.
	view = PointRemoval::hiddenPointRemovalQuick2<pcl::PointXYZRGBNormal>(view, hiddenPtSensitivity, threadCount*8);
	return Transforms::translatePCloudWithNormals(view, viewPointTranslate);
}

pcl::PointCloud<pcl::PointXYZRGBNormal> BlenderViewGenerator::getViewOnSegment(int index, const Eigen::Vector3f& viewPtTranslate, float viewYAngleRotDEG, float noiseStrN, float noiseStrP) const
{
	if (segmentClouds.size() <= index) {
		cout << "BlenderViewGenerator: getViewOnSegment WARNING | INVALID INDEX\n";
		return pcl::PointCloud<pcl::PointXYZRGBNormal>();
	}
	pcl::PointCloud<pcl::PointXYZRGBNormal> allOthers{};
	for (int i = 0; i < segmentClouds.size(); ++i) {
		if (i != index) {
			allOthers += segmentClouds.at(i);
		}
	}
	auto view = segmentClouds.at(index);
	if (std::fabsf(noiseStrN) > 0.0f || std::fabsf(noiseStrP) > 0.0f) {
		view = SurfaceProcessor::generateNoisySurfaceFromNormals(view, 0.01f, noiseStrN, noiseStrP);
	}
	view = Transforms::translatePCloudWithNormals(view, 1, 0, 1);
	view = Transforms::rotatePCloudAroundAxisWithNormals(view, Eigen::Vector3f::UnitY(), DEG2RAD(viewYAngleRotDEG));
	view = Transforms::translatePCloudWithNormals(view, -1, 0, -1);
	view = Transforms::translatePCloudWithNormals(view, -viewPtTranslate);
	view = PointRemoval::backfaceCulling(view, 0.01);
	view = PointRemoval::hiddenPointRemovalByOtherCloud(view, allOthers, hiddenPtSensitivity, threadCount);
	return Transforms::translatePCloudWithNormals(view, viewPtTranslate);
}

//############################ SETTINGS #############################

void BlenderViewGenerator::setHiddenPointRemovalSensitivity(float sensitivity)
{
	this->hiddenPtSensitivity = sensitivity;
}

float BlenderViewGenerator::getHiddenPointRemovalSensitivity() const
{
	return hiddenPtSensitivity;
}

void BlenderViewGenerator::setThreadCount(unsigned int threads)
{
	this->threadCount = threads;
}

unsigned int BlenderViewGenerator::getThreadCount() const
{
	return this->threadCount;
}
