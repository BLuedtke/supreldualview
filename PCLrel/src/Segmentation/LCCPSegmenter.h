/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#pragma once
#ifndef LCCPSegmenter_H
#define LCCPSegmenter_H
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

class LCCPSegmentationConfig;

static class LCCPSegmenter
{
public:

	template<typename PointT>
	static pcl::PointCloud<pcl::PointXYZL> segmentLCCP(const pcl::PointCloud<PointT>& inCloud, pcl::PointCloud<pcl::Normal>& inNormals, const LCCPSegmentationConfig& conf);

	
	template<typename PointT>
	static pcl::PointCloud<pcl::PointXYZL> segmentLCCP(const pcl::PointCloud<PointT>& inputCloud, const LCCPSegmentationConfig& conf);

	static int countLabels(const pcl::PointCloud<pcl::PointXYZL>& inputCloud);

private:

	static void orderLabels(pcl::PointCloud<pcl::PointXYZL>& inOutCloud);
	static void removeSmallSegments(pcl::PointCloud<pcl::PointXYZL>& inOutCloud, int sizeThreshold);

};
#endif /* LCCPSegmenter_H */
