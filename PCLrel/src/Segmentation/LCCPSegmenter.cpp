/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#include "LCCPSegmenter.h"
#include "LCCPSegmentationConfig.h"
#include <pcl/segmentation/lccp_segmentation.h>

template pcl::PointCloud<pcl::PointXYZL> LCCPSegmenter::segmentLCCP<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& inCloud, pcl::PointCloud<pcl::Normal>& inNormals, const LCCPSegmentationConfig& conf);
template pcl::PointCloud<pcl::PointXYZL> LCCPSegmenter::segmentLCCP<pcl::PointXYZRGB>(const pcl::PointCloud<pcl::PointXYZRGB>& inCloud, pcl::PointCloud<pcl::Normal>& inNormals, const LCCPSegmentationConfig& conf);
template pcl::PointCloud<pcl::PointXYZL> LCCPSegmenter::segmentLCCP<pcl::PointXYZRGBA>(const pcl::PointCloud<pcl::PointXYZRGBA>& inCloud, pcl::PointCloud<pcl::Normal>& inNormals, const LCCPSegmentationConfig& conf);

//LCCP Segmentation, templated. PointT is recommended to be PointXYZ; other Point Types have not been tested so far.
template<typename PointT>
inline pcl::PointCloud<pcl::PointXYZL> LCCPSegmenter::segmentLCCP(const pcl::PointCloud<PointT>& inCloud, pcl::PointCloud<pcl::Normal>& inNormals, const LCCPSegmentationConfig& conf)
{
	if (inCloud.empty() || inNormals.empty()) {
		std::cout << "segmentLCCP: Input cloud (or input normals) is empty!\n";
		return pcl::PointCloud<pcl::PointXYZL>();
	}
	pcl::SupervoxelClustering<PointT> super(conf.voxel_resolution, conf.seed_resolution);
	super.setUseSingleCameraTransform(false);
	super.setInputCloud(std::make_shared<pcl::PointCloud<PointT>>(inCloud));
	super.setNormalCloud(std::make_shared<pcl::PointCloud<pcl::Normal>>(inNormals));

	super.setSpatialImportance(conf.spatial_importance);
	super.setNormalImportance(conf.normal_importance);
	super.setColorImportance(conf.color_importance);

	std::map<std::uint32_t, std::shared_ptr<pcl::Supervoxel<PointT>>> supervoxel_clusters;
	super.extract(supervoxel_clusters);
	std::multimap<std::uint32_t, std::uint32_t> supervoxel_adjacency;
	super.getSupervoxelAdjacency(supervoxel_adjacency);
	pcl::LCCPSegmentation<PointT> lccp;

	lccp.setInputSupervoxels(supervoxel_clusters, supervoxel_adjacency);

	lccp.setConcavityToleranceThreshold(conf.concavity_tolerance_angle);
	lccp.setSanityCheck(conf.use_sanity_criterion);
	lccp.setSmoothnessCheck(true, conf.voxel_resolution, conf.seed_resolution, conf.smoothness_threshold);
	lccp.setKFactor(conf.use_extended_convexity ? 1 : 0);
	lccp.setMinSegmentSize(conf.min_segment_size);
	lccp.segment();

	pcl::PointCloud<pcl::PointXYZL> labeledCloud{ *super.getLabeledCloud() };
	lccp.relabelCloud(labeledCloud);
	labeledCloud.points.shrink_to_fit();
	return labeledCloud;
}

template pcl::PointCloud<pcl::PointXYZL> LCCPSegmenter::segmentLCCP<pcl::PointNormal>(const pcl::PointCloud<pcl::PointNormal>& inputCloud, const LCCPSegmentationConfig& conf);
template pcl::PointCloud<pcl::PointXYZL> LCCPSegmenter::segmentLCCP<pcl::PointXYZRGBNormal>(const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud, const LCCPSegmentationConfig& conf);

template<typename PointT>
pcl::PointCloud<pcl::PointXYZL> LCCPSegmenter::segmentLCCP(const pcl::PointCloud<PointT>& inputCloud, const LCCPSegmentationConfig& conf)
{
	if (inputCloud.empty()) {
		std::cout << "segmentLCCP: Input Cloud is empty!\n";
		return pcl::PointCloud<pcl::PointXYZL>();
	}
	pcl::PointCloud<pcl::Normal> sep1N{};
	pcl::copyPointCloud(inputCloud, sep1N);
	if constexpr (std::is_same<PointT, pcl::PointXYZRGBNormal>::value) {
		pcl::PointCloud<pcl::PointXYZRGB> sep1 {};
		pcl::copyPointCloud(inputCloud, sep1);
		return segmentLCCP(sep1, sep1N, conf);
	}
	else if constexpr (std::is_same<PointT, pcl::PointNormal>::value) {
		pcl::PointCloud<pcl::PointXYZ> sep1 {};
		pcl::copyPointCloud(inputCloud, sep1);
		return segmentLCCP(sep1, sep1N, conf);
	}
	return pcl::PointCloud<pcl::PointXYZL>();
}


int LCCPSegmenter::countLabels(const pcl::PointCloud<pcl::PointXYZL>& inputCloud)
{
	if (inputCloud.empty()) {
		std::cout << "countLabels: Input Cloud is empty!\n";
		return 0;
	}
	std::vector<int> labels {};
	for (size_t i = 0; i < inputCloud.size(); i++)
	{
		if (!(std::find(labels.begin(), labels.end(), inputCloud[i].label) != labels.end())) {
			//Label not in labels-vector yet.
			labels.push_back(static_cast<int>(inputCloud[i].label));
		}
	}

	return labels.size();
}

//Re-Labels a labeled PointCloud such that the smallest label starts at 1 and the next label is 2, the next 3, and so on.
void LCCPSegmenter::orderLabels(pcl::PointCloud<pcl::PointXYZL>& inOutCloud)
{
	if (inOutCloud.empty()) {
		std::cout << "orderLabels: Input Cloud has no points. Nothing changed." << std::endl;
		return;
	}
	std::vector<int> labels {};
	for (size_t i = 0; i < inOutCloud.size(); i++)
	{
		if (!(std::find(labels.begin(), labels.end(), inOutCloud[i].label) != labels.end())) {
			//Label not in labels-vector yet.
			labels.push_back(static_cast<int>(inOutCloud[i].label));
		}
	}

	std::sort(labels.begin(), labels.end());

	std::map<size_t, size_t> reLabel {};

	for (size_t i = 0; i < labels.size(); ++i)
	{
		reLabel.insert({ labels.at(i), i + 1 });
	}

	for (size_t i = 0; i < inOutCloud.size(); i++)
	{
		inOutCloud[i].label = reLabel.at(inOutCloud[i].label);
	}
}

void LCCPSegmenter::removeSmallSegments(pcl::PointCloud<pcl::PointXYZL>& inOutCloud, int sizeThreshold)
{
	if (inOutCloud.empty()) {
		std::cout << "orderLabels: Input Cloud has no points. Nothing changed." << std::endl;
		return;
	}
	
	std::map<int, pcl::PointCloud<pcl::PointXYZL>> labelsCountMap {};

	for (size_t i = 0; i < inOutCloud.size(); ++i)
	{
		if (labelsCountMap.count(inOutCloud[i].label) == 0) {
			pcl::PointCloud<pcl::PointXYZL> pCloud {};
			pCloud.push_back(inOutCloud[i]);
			labelsCountMap.insert({ inOutCloud[i].label , std::move(pCloud) });
		}
		else {
			labelsCountMap.at(inOutCloud[i].label).push_back(inOutCloud[i]);
		}
	}
	const int size = inOutCloud.size();
	inOutCloud.clear();
	inOutCloud.reserve(size);
	for (const auto& lEntry : labelsCountMap) {
		if (lEntry.second.size() > sizeThreshold) {
			inOutCloud += lEntry.second;
		}
	}
	inOutCloud.resize(inOutCloud.points.size());
}
