/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#include "Segment.h"

#include <iostream>
#include <pcl/common/transforms.h>
#include <pcl/common/common.h>

using std::cout;
//Construct a Segment with the default constructor. ID will be set to -1.
Segment::Segment()
{
	convexHull->setDimension(3);
}

//Construct a Segment with a given Input (Segment) Cloud. The ID will be set to the label of the segCloud.
// This assumes that all points in the input cloud have the same label.
Segment::Segment(const pcl::PointCloud<pcl::PointXYZL>& segCloud)
{
	if (!segCloud.empty()) {
		id = segCloud.at(0).label;
		auto cloudAsXYZ = std::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
		pcl::copyPointCloud(segCloud, *cloudAsXYZ);
		convexHull->setDimension(3);
		convexHull->setInputCloud(cloudAsXYZ);
		this->computeCHull();
	}
	else {
		cout << "WARNING: Segment Input Cloud in Segment Constructor is empty.\n";
		cout << "Segment Constructor: Setting ID from Segment Cloud unsuccessful. Please provide a non-empty segment cloud\n";
	}
	
}


//Construct a Segment with a given Input (Segment) Cloud. The ID will be set to the value of parameter "id".
Segment::Segment(const pcl::PointCloud<pcl::PointXYZ>& segCloud, int id)
{
	if (!segCloud.empty()) {
		this->id = id;
		auto cloudAsXYZ = std::make_shared<pcl::PointCloud<pcl::PointXYZ>>(segCloud);
		convexHull->setDimension(3);
		convexHull->setInputCloud(cloudAsXYZ);
		this->computeCHull();
	}
	else {
		cout << "WARNING: Segment Input Cloud in Segment Constructor is empty.\n";
	}
}

Segment::Segment(const Segment& otherSegment)
{
	*this = Segment(*otherSegment.getInputCloud(), otherSegment.getId());
}

//If a non-empty Input Cloud was set for this object, the convex hull will be reconstructed.
// Returns true on successful completion, false if input cloud is not set or empty.
// After this method, hullCloud is populated
bool Segment::computeCHull()
{
	if (convexHull->getInputCloud() == nullptr) {
		cout << "No Input Cloud for CHull set. Please set an input cloud before attempting to construct the hull.\n";
		return false;
	} else if(convexHull->getInputCloud()->empty()){
		cout << "No POINTS in Input Cloud for CHull. Please set a non-empty input cloud before attempting to construct the hull.\n";
		return false;
	}
	this->polygons.clear();
	this->polygons.reserve(convexHull->getInputCloud()->size() / 5);
	convexHull->setComputeAreaVolume(true);
	convexHull->reconstruct(*hullCloud, this->polygons);
	pcl::PointXYZ pointOut1{};
	pcl::computeCentroid(*this->hullCloud, pointOut1);
	hullcenter = Eigen::Vector3f(pointOut1.x, pointOut1.y, pointOut1.z);
	pcl::PointXYZ minV{}, maxV{};
	this->getMinMax3D(minV, maxV);
	abbMiddle = (maxV.getVector3fMap() + minV.getVector3fMap()) * 0.5f;
	return true;
}

//Translates a segment. Forces re-calculation of the convex hull, so this is quite expensive.
void Segment::translateSegment(const Eigen::Vector3f translateVec)
{
	pcl::PointCloud<pcl::PointXYZ> origCloudCpy{ *convexHull->getInputCloud() };
	Eigen::Affine3f transform = Eigen::Affine3f::Identity();
	transform.translation() << translateVec;
	pcl::transformPointCloud(origCloudCpy, origCloudCpy, transform);
	convexHull->setInputCloud(std::make_shared<pcl::PointCloud<pcl::PointXYZ>>(origCloudCpy));
	computeCHull();
}

//Rotates a segment. Forces re-calculation of the convex hull, so this is quite expensive.
void Segment::rotateSegment(const Eigen::Vector3f rotateAxis, float angleInRad)
{
	auto origCloudCpy = std::make_shared<pcl::PointCloud<pcl::PointXYZ>>(*convexHull->getInputCloud());
	Eigen::Affine3f transform = Eigen::Affine3f::Identity();
	transform.rotate(Eigen::AngleAxisf(angleInRad, rotateAxis));
	pcl::transformPointCloud(*origCloudCpy, *origCloudCpy, transform);
	convexHull->setInputCloud(origCloudCpy);
	computeCHull();
}

void Segment::rotateThenTranslateSegment(const Eigen::Vector3f rotateAxis, float angleInRad, const Eigen::Vector3f translateVec)
{
	auto origCloudCpy = std::make_shared<pcl::PointCloud<pcl::PointXYZ>>(*convexHull->getInputCloud());
	Eigen::Affine3f transform = Eigen::Affine3f::Identity();
	transform.rotate(Eigen::AngleAxisf(angleInRad, rotateAxis));
	transform.translation() << translateVec;
	pcl::transformPointCloud(*origCloudCpy, *origCloudCpy, transform);
	convexHull->setInputCloud(origCloudCpy);
	computeCHull();
}

void Segment::translateSegmentNoCHull(const Eigen::Vector3f translateVec)
{
	pcl::PointCloud<pcl::PointXYZ> origCloudCpy{ *convexHull->getInputCloud() };
	Eigen::Affine3f transform = Eigen::Affine3f::Identity();
	transform.translation() << translateVec;
	pcl::transformPointCloud(origCloudCpy, origCloudCpy, transform);
	convexHull->setInputCloud(std::make_shared<pcl::PointCloud<pcl::PointXYZ>>(origCloudCpy));
}

void Segment::rotateSegmentNoCHull(const Eigen::Vector3f rotateAxis, float angleInRad)
{
	auto origCloudCpy = std::make_shared<pcl::PointCloud<pcl::PointXYZ>>(*convexHull->getInputCloud());
	Eigen::Affine3f transform = Eigen::Affine3f::Identity();
	transform.rotate(Eigen::AngleAxisf(angleInRad, rotateAxis));
	pcl::transformPointCloud(*origCloudCpy, *origCloudCpy, transform);
	convexHull->setInputCloud(origCloudCpy);
}

void Segment::applyTransformMatrix(const Eigen::Matrix4f transformM)
{
	auto origCloudCpy = std::make_shared<pcl::PointCloud<pcl::PointXYZ>>(*convexHull->getInputCloud());
	Eigen::Affine3f transform = Eigen::Affine3f::Identity();
	transform.matrix() = transformM;
	pcl::transformPointCloud(*origCloudCpy, *origCloudCpy, transform);
	convexHull->setInputCloud(origCloudCpy);
	computeCHull();
}

//Sets a new InputCloud for ConvexHull, but does not compute the ConvexHull anew itself. Sets the ID to the new label of the first point.
void Segment::setInputCloud(const pcl::PointCloud<pcl::PointXYZL>& sCloud)
{
	if (!sCloud.empty()) {
		auto inCloud = std::make_shared< pcl::PointCloud<pcl::PointXYZ>>();
		pcl::copyPointCloud(sCloud, *inCloud);
		this->convexHull->setInputCloud(inCloud);
		this->id = static_cast<int>(sCloud[0].label);
	}
	else {
		cout << "Segment: Setting Input Cloud FAILED. Please provide a non-empty cloud\n";
	}
}

void Segment::setId(int id)
{
	this->id = id;
}

int Segment::getId() const
{
	return this->id;
}

Eigen::Vector3f Segment::getHullCenter() const
{
	return hullcenter;
}

Eigen::Vector3f Segment::getABBMiddle() const
{
	return abbMiddle;
}

//probably an antipattern but... it works here.
void Segment::getMinMax3D(pcl::PointXYZ& minPt, pcl::PointXYZ& maxPt) const
{
	pcl::getMinMax3D(*this->hullCloud, minPt, maxPt); 
}

const pcl::ConvexHull<pcl::PointXYZ>& Segment::getConvexHull() const
{
	return *this->convexHull;
}

const pcl::PointCloud<pcl::PointXYZ>& Segment::getHullCloud() const
{
	return *this->hullCloud;
}

const std::shared_ptr<const pcl::ConvexHull<pcl::PointXYZ>> Segment::getConvexHullPTR() const
{
	return this->convexHull;
}

const std::shared_ptr<const pcl::PointCloud<pcl::PointXYZ>> Segment::getHullCloudPTR() const
{
	return this->hullCloud;
}

const std::shared_ptr<const pcl::PointCloud<pcl::PointXYZ>> Segment::getInputCloud() const
{
	return this->convexHull->getInputCloud();
}

std::vector<pcl::Vertices> Segment::getPolygonsCopy() const
{
	return this->polygons;
}
