/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#pragma once
#ifndef LCCPSegmentationConfig_H
#define LCCPSegmentationConfig_H


/// <summary>
/// Holds the necessary parameters for using LCCPSegmentation. 
/// </summary>
class LCCPSegmentationConfig
{
public:
	LCCPSegmentationConfig();

	void printConfig() const;

	void setManual(float voxelSize, float seedFactor, float concavAngle, int minSegSize, float spaceImp, float normalImp, float colorImp);
	
	//Adjusts the voxel_resolution setting and the voxel seeding.
	void updateVoxelSizeAndSeeding(float newVoxelSize, float seedFactor);

	// Supervoxels

	// If too small and cloud density is low, supervoxels might not get classified as adjacent
	float voxel_resolution = 0.05f;
	// Too small = very man tiny fragments. Too big = too few supervoxels. ~ >1.5 times the voxel_resolution seems fine.
	float seed_resolution = 0.15f;
	float color_importance = 0.0f;
	float spatial_importance = 1.0f;
	float normal_importance = 4.0f;
	
	// LCCPSegmentation
	// Angle over/under (?) which a connection is classified as convex
	float concavity_tolerance_angle = 9.0f;
	// Smoothing between Supervoxels before Segmentation
	float smoothness_threshold = 1.0f; //0.4
	// Tries to merge segments smaller than this size
	unsigned int min_segment_size = 50;
	// Extended Convexity definition (true is recommended)
	bool use_extended_convexity = true;
	// Leads to more edges in the middle of larger structures to be classified as convex. Recommended: FALSE
	bool use_sanity_criterion = false;
};


#endif /* LCCPSegmentationConfig_H */