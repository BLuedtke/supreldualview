/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#include "LabelToSegmentConverter.h"
#include "Segment.h"

#include <map>
#include <tuple>

//Recommended value for sparsenessThreshold: average nearest neighbor distance (of the original cloud before segmentation) multiplied by five.
std::vector<Segment> LabelToSegmentConverter::produceSgmtsFromLabelCloud(const pcl::PointCloud<pcl::PointXYZL>& inputCloud, int sizeThreshold, bool removeSparseSegments, float sparsenessThreshold)
{
	if (inputCloud.empty()) {
		std::cout << "produceSgmtsFromLabelCloud: Input Cloud has no points. Returning empty Vector." << std::endl;
		return std::vector<Segment>();
	}
	if (removeSparseSegments && sparsenessThreshold <= 0.0f) {
		std::cout << "produceSgmtsFromLabelCloud: Removing Sparse Segments not possible if sparsenessThreshold is 0 (threshold too low, everything would be removed).";
		std::cout << " No sparse Segments will be removed.\n";
		removeSparseSegments = false;
	}
	std::map<std::uint32_t, pcl::PointCloud<pcl::PointXYZL>> labelCloudMap {};
	for (const auto& pt : inputCloud.points) {
		labelCloudMap[pt.label].push_back(pt);
	}
	int runningID = 1;
	std::vector<Segment> retVec {};
	retVec.reserve(labelCloudMap.size());
	for (const auto& labelPair : labelCloudMap) {
		if (labelPair.second.size() > sizeThreshold) {
			if (removeSparseSegments) {
				Segment aTemp{ labelPair.second };
				//If the segment has a large size (volume), but few points, this value will be larger than for segments with many points per volume.
				const float inversePtPerVolumex1k = aTemp.getConvexHull().getTotalVolume() * 1000.0f / labelPair.second.size();
				//Only segments with a low inversePtPerVolume are added (aka high enough density).
				if (inversePtPerVolumex1k <= sparsenessThreshold) {
					aTemp.setId(runningID);
					runningID++;
					retVec.push_back(std::move(aTemp));
				}
			}
			else {
				Segment bTemp{ labelPair.second };
				bTemp.setId(runningID);
				runningID++;
				retVec.push_back(std::move(bTemp));
			}
		}
	}
	retVec.shrink_to_fit();
	return retVec;
}