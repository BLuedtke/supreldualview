/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#pragma once
#ifndef Segment_H
#define Segment_H
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/surface/convex_hull.h>

/// <summary>
/// <para>Holds information about one segment (many points with the same label representing one 'object').</para>
/// <para>The ID is the label from the first PointXYZL in the input cloud if a labeled cloud is provided. Otherwise, 
/// ID can be set manually if a PointXYZ-based cloud is provided.</para>
/// <para>Provides the convex hull (getConvexHull) and the computed hullcloud (getHullcloud) -> direct access to the convex
/// hull member might be deprecated in the future.</para>
/// <para>Some of these methods have an implicit call order/sequence, making direct creation a bit tedious. 
/// Recommended: Use the static LabelToSegmentConverter methods to create Segments.</para>
/// </summary>
class Segment
{
public:
	//Usual
	Segment();
	Segment(const pcl::PointCloud<pcl::PointXYZL>& segCloud);
	Segment(const pcl::PointCloud<pcl::PointXYZ>& segCloud, int id);
	//Copy
	Segment(const Segment& otherSegment);
	Segment& operator=(Segment const&) = default;
	//Move
	Segment(Segment&&) = default;
	Segment& operator=(Segment&&) = default;

	bool computeCHull();

	void translateSegment(const Eigen::Vector3f translateVec);
	void rotateSegment(const Eigen::Vector3f rotateAxis, float angleInRad);
	void rotateThenTranslateSegment(const Eigen::Vector3f rotateAxis, float angleInRad, const Eigen::Vector3f translateVec);
	void applyTransformMatrix(const Eigen::Matrix4f transformM);

	//After calling this, computeCHull needs to be called manually!
	void translateSegmentNoCHull(const Eigen::Vector3f translateVec);
	//After calling this, computeCHull needs to be called manually!
	void rotateSegmentNoCHull(const Eigen::Vector3f rotateAxis, float angleInRad);
	
	//DEPRECATED
	//void trtSegment(const Eigen::Vector3f rotateAxis, float angleInRad, const Eigen::Vector3f translateVec1, const Eigen::Vector3f translateVec2);

	void setId(int id);
	int getId() const;
	Eigen::Vector3f getHullCenter() const;
	Eigen::Vector3f getABBMiddle() const;
	void getMinMax3D(pcl::PointXYZ& minPt, pcl::PointXYZ& maxPt) const;

	const pcl::ConvexHull<pcl::PointXYZ>& getConvexHull() const;
	const pcl::PointCloud<pcl::PointXYZ>& getHullCloud() const;
	const std::shared_ptr<const pcl::ConvexHull<pcl::PointXYZ>> getConvexHullPTR() const;
	const std::shared_ptr<const pcl::PointCloud<pcl::PointXYZ>> getHullCloudPTR() const;

	const std::shared_ptr<const pcl::PointCloud<pcl::PointXYZ>> getInputCloud() const;
	void setInputCloud(const pcl::PointCloud<pcl::PointXYZL>& sCloud);

	std::vector<pcl::Vertices> getPolygonsCopy() const;


private:
	int id{-1};
	std::shared_ptr<pcl::ConvexHull<pcl::PointXYZ>> convexHull = std::make_shared<pcl::ConvexHull<pcl::PointXYZ>>();
	std::shared_ptr<pcl::PointCloud<pcl::PointXYZ>> hullCloud = std::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
	//To capture the pcl::Vertices result from CHull-Reconstruction. 
	std::vector<pcl::Vertices> polygons{}; 
	Eigen::Vector3f hullcenter{ 0.0f,0.0f,0.0f };
	Eigen::Vector3f abbMiddle{ 0.0f,0.0f,0.0f };
};

#endif /* Segment_H */