/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#pragma once
#ifndef LabelToSegmentConverter_H
#define LabelToSegmentConverter_H
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

class Segment;

static class LabelToSegmentConverter
{
public:
	static std::vector<Segment> produceSgmtsFromLabelCloud(const pcl::PointCloud<pcl::PointXYZL>& inputCloud, int sizeThreshold = 0, bool removeSparseSegments = false, float sparsenessThreshold = 0.0f);
};
#endif /* LabelToSegmentConverter_H */
