/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#include "LCCPSegmentationConfig.h"
#include <iostream>

LCCPSegmentationConfig::LCCPSegmentationConfig()
{
	//Def
}

void LCCPSegmentationConfig::printConfig() const
{
	using std::cout;
	cout << "LCCPSegmentationConfig:" << "\n";
	cout << "    Spatial Importance: " << spatial_importance << "\n";
	cout << "    Normal Importance: " << normal_importance << "\n";
	cout << "    Color Importance: " << color_importance << "\n";
	cout << "    Voxel-Resolution: " << voxel_resolution << "\n";
	cout << "    Seed-Resolution: " << seed_resolution << "\n";
	cout << "    concavToleranceAngle: " << concavity_tolerance_angle << "\n";
	cout << "    min-Segment-Size: " << min_segment_size << "\n";
	cout << "    extended-convexity: " << voxel_resolution << "\n";
	cout << "    smoothness-Thresh: " << smoothness_threshold << "\n";
}

/// <summary>
/// Set the Parameters for LCCP Segmentation.
/// </summary>
/// <param name="voxelSize"> = voxel_resolution</param>
/// <param name="seedFactor">seed_resolution = voxelSize * seedFactor</param>
/// <param name="concavAngle">Angle at which a connection is classified as convex</param>
/// <param name="minSegSize">If there are segments with fewer points than minSegSize, a merging will be attempted (no guaranteed success)</param>
/// <param name="spaceImp">Importance factor of the spatial relation for determining convexity</param>
/// <param name="normalImp">Importance factor of the relation of normals for determining convexity</param>
/// <param name="colorImp">Importance factor of the color for determining convexity</param>
void LCCPSegmentationConfig::setManual(float voxelSize, float seedFactor, float concavAngle, int minSegSize, float spaceImp, float normalImp, float colorImp)
{
	voxel_resolution = voxelSize;
	seed_resolution = voxelSize * seedFactor;
	concavity_tolerance_angle = concavAngle;
	min_segment_size = minSegSize;
	spatial_importance = spaceImp;
	normal_importance = normalImp;
	color_importance = colorImp;
}

void LCCPSegmentationConfig::updateVoxelSizeAndSeeding(float newVoxelSize, float seedFactor)
{
	voxel_resolution = newVoxelSize;
	seed_resolution = voxel_resolution * seedFactor;
}