/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#include "SpatialAnalyzer.h"
#include "../Constants/Constants.h"
#include <pcl/kdtree/kdtree_flann.h>
#include <memory>

/// <summary>
/// Returns a PointCloud (XYZ point format) which contains all points from input cloud aCloud for which there is no point 
/// in bCloud at the same location (+- Epsilon) in aCloud.
/// </summary>
/// <param name="aCloud">Input Cloud 1. IMPORTANT: THIS CLOUD SHALL NOT CONTAIN ANY DUPLICATE POINTS! 
/// Exception: Offset voxels simulating the weight of something.</param>
/// <param name="bCloud">Input Cloud 2</param>
/// <returns>All points in aCloud which are not closer than Epsilon to a point in cloud bCloud.</returns>
pcl::PointCloud<pcl::PointXYZ> SpatialAnalyzer::getANotExactlyInB(const pcl::PointCloud<pcl::PointXYZ>& aCloud, const pcl::PointCloud<pcl::PointXYZ>& bCloud)
{
	if (aCloud.empty() || bCloud.empty()) {
		std::cout << "WARNING getANotExactlyInB: One of the Input Clouds is empty! Returning empty.\n";
		return pcl::PointCloud<pcl::PointXYZ>();
	}
	using PointT = pcl::PointXYZ;
	pcl::KdTreeFLANN<PointT> kdtree;
	kdtree.setInputCloud(std::make_shared<pcl::PointCloud<PointT>>(aCloud));
	//Setting the Epsilon any higher than 0.0f proved very very dangerous when running the support
	// relation with small voxel sizes. Furthermore, a very small epsilon might be fine, but I couldn't find
	// any speed benefit here.
	kdtree.setEpsilon(0.0f);
	int K = 1;
	std::vector<int> pIDs(K);
	std::vector<float> pSqDists(K);
	
	// Assume that only few points in A have points in B that are at the very same position
	// => 2-5x faster to delete the ones where that _is_ in fact the case, compared to adding point-by-point
	std::vector<int> idsToErase{};
	for (const auto& pt : bCloud.points) {
		//Tried this with radius search as well; knN a bit faster or equal.
		//Radius search would be able to handle duplicates however, to maybe that'd be preferable.
		if (kdtree.nearestKSearch(pt, K, pIDs, pSqDists) > 0) {
			if (std::sqrtf(pSqDists[0]) < Constants::EPSILON) {
				idsToErase.push_back(pIDs[0]);
			}
		}
	}
	std::sort(idsToErase.begin(), idsToErase.end());
	pcl::PointCloud<PointT> a_cut_b_cloud{ aCloud };
	for (auto it = idsToErase.crbegin(); it != idsToErase.crend(); ++it) {
		std::swap(a_cut_b_cloud[*it], a_cut_b_cloud[a_cut_b_cloud.size() - 1]);
		a_cut_b_cloud.points.pop_back();
	}
	a_cut_b_cloud.resize(a_cut_b_cloud.size());
	return a_cut_b_cloud;
}

//2-5x slower than the method without _OLD! (At least when used with VoxelSegments and Intersections).
// DEPRECATED
pcl::PointCloud<pcl::PointXYZ> SpatialAnalyzer::getANotExactlyInB_OLD(const pcl::PointCloud<pcl::PointXYZ>& aCloud, const pcl::PointCloud<pcl::PointXYZ>& bCloud)
{
	if (aCloud.empty() || bCloud.empty()) {
		std::cout << "WARNING getANotExactlyInB: One of the Input Clouds is empty! Returning empty.\n";
		return pcl::PointCloud<pcl::PointXYZ>();
	}
	using PointT = pcl::PointXYZ;
	pcl::KdTreeFLANN<PointT> kdtree;
	kdtree.setInputCloud(std::make_shared<pcl::PointCloud<PointT>>(bCloud));
	kdtree.setEpsilon(0.0f);
	int K = 1;
	std::vector<int> pIDs(K);
	std::vector<float> pSqDists(K);
	pcl::PointCloud<PointT> a_cut_b_cloud{};
	a_cut_b_cloud.reserve(aCloud.size());
	//Tried this with radius search as well; knN a bit faster or equal.
	for (const auto& pt : aCloud.points) {
		if (kdtree.nearestKSearch(pt, K, pIDs, pSqDists) > 0) {
			if (std::sqrtf(pSqDists[0]) > Constants::EPSILON) {
				a_cut_b_cloud.push_back(pt);
			}
		}
	}
	a_cut_b_cloud.resize(a_cut_b_cloud.size());
	return a_cut_b_cloud;
}

//Returns a cloud with all points from a which would be within EPSILON distance to a point in bCloud if both were projected onto the XZ plane.
// The returned cloud is not projected onto the XZ plane itself.
pcl::PointCloud<pcl::PointXYZ> SpatialAnalyzer::getAPointsNearBonXZ(const pcl::PointCloud<pcl::PointXYZ>& aCloud, const pcl::PointCloud<pcl::PointXYZ>& bCloud, float distThreshold)
{
	if (aCloud.empty() || bCloud.empty()) {
		std::cout << "WARNING getAPointsNearBonXZ: One of the Input Clouds is empty! Returning empty.\n";
		return pcl::PointCloud<pcl::PointXYZ>();
	}
	using PointT = pcl::PointXYZ;
	auto bCpyPtr = std::make_shared<pcl::PointCloud<PointT>>(bCloud);
	for (auto& pt : bCpyPtr->points) {
		pt.y = 0.0f;
	}
	pcl::KdTreeFLANN<PointT> kdtree;
	kdtree.setInputCloud(bCpyPtr);
	int K = 1;
	std::vector<int> pIDs(K);
	std::vector<float> pSqDists(K);
	pcl::PointCloud<PointT> aModCloud {};
	aModCloud.reserve(aCloud.size()); 

	for (const auto& pt : aCloud.points) {
		PointT projectedPT = PointT(pt.x, 0.0f, pt.z);
		if (kdtree.nearestKSearch(projectedPT, K, pIDs, pSqDists) > 0) {
			if (std::sqrtf(pSqDists[0]) < distThreshold) {
				aModCloud.push_back(pt);
			}
		}
	}
	aModCloud.resize(aModCloud.size());
	return aModCloud;
}

/// <summary>
/// Returns the euclidean distance from pPoint to the NEAREST point in inputCloud.
/// </summary>
/// <param name="c_cloud"></param>
/// <param name="p_point"></param>
/// <returns></returns>
float SpatialAnalyzer::getMinimumDistToCloud(const pcl::PointCloud<pcl::PointXYZ>& inputCloud, const pcl::PointXYZ& pPoint)
{
	return getMinimumDistToCloud(std::make_shared<pcl::PointCloud<pcl::PointXYZ>>(inputCloud), pPoint);
}

/// <summary>
/// Returns the euclidean distance from pPoint to the NEAREST point in inputCloud.
/// </summary>
/// <param name="c_cloud"></param>
/// <param name="p_point"></param>
/// <returns></returns>
float SpatialAnalyzer::getMinimumDistToCloud(const std::shared_ptr<const pcl::PointCloud<pcl::PointXYZ>>& inputCloud, const pcl::PointXYZ& pPoint)
{
	if (inputCloud->empty()) {
		std::cout << "Minimum Distance to an empty cloud is impossible. Returning -1.f" << std::endl;
		return -1.f;
	}
	pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;
	kdtree.setEpsilon(0.0f);
	kdtree.setInputCloud(inputCloud);
	int K = 1;
	std::vector<int> pIDs(K);
	std::vector<float> pSqDists(K);
	if (kdtree.nearestKSearch(pPoint, K, pIDs, pSqDists) > 0) {
		return std::sqrtf(pSqDists[0]);
	}
	else {
		std::cout << "No Distance to the pointcloud could be found! Returning -1.f" << std::endl;
		return -1.f;
	}
}

//Returns the number of Voxels (/points) from srcCloud which are closer to first element(/cloud) in the ptClouds vector than to any other clouds in the ptClouds vector.
int SpatialAnalyzer::getNumberOfPointsClosestToV0(std::vector<std::shared_ptr<const pcl::PointCloud<pcl::PointXYZ>>>& ptClouds, const pcl::PointCloud<pcl::PointXYZ>& srcCloud)
{
	if (srcCloud.empty()) {
		std::cout << "getNumberOfPointsClosestToV0: SRC Cloud is empty! Returning 0.\n";
		return 0;
	}
	if (ptClouds.size() < 2) {
		return srcCloud.size();
	}
	std::vector<std::unique_ptr<pcl::KdTreeFLANN<pcl::PointXYZ>>> vecKdTrees {};
	for (size_t i = 0; i < ptClouds.size(); ++i) {
		if (!ptClouds[i]->empty()) {
			auto kdTreePtr = std::make_unique<pcl::KdTreeFLANN<pcl::PointXYZ>>();
			kdTreePtr->setEpsilon(0.0f);
			kdTreePtr->setInputCloud(ptClouds.at(i));
			vecKdTrees.push_back(std::move(kdTreePtr));
		}
		else {
			std::cout << "getNumberOfPointsClosestToV0: Cloud at index " << i << " is empty. Not adding that cloud.\n";
		}
	}
	const int K = 1;
	std::vector<int> pIDs(K);
	std::vector<float> pSqDists(K);
	// First set count = all points, then decrement for each point that is actually closer to a cloud other than V0
	int retCounter = srcCloud.size();
	for (const auto& ptOrig : srcCloud) {
		float vec0Dist = 0.0f;
		if (vecKdTrees[0]->nearestKSearch(ptOrig, K, pIDs, pSqDists) > 0) {
			vec0Dist = std::sqrtf(pSqDists[0]);
		}
		for (size_t i = 1; i < vecKdTrees.size(); ++i) {
			pIDs.clear();
			pSqDists.clear();
			if (vecKdTrees[i]->nearestKSearch(ptOrig, K, pIDs, pSqDists) > 0) {
				if (std::sqrtf(pSqDists[0]) < vec0Dist) {
					retCounter--;
					//Breaks the inner (!) loop
					break; 
				}
			}
		}
	}
	//std::cout << "Returning Retcounter " << retCounter << " out of " << srcCloud.size() << " total possible\n";
	return retCounter;
}


//This method implements functionality present in pcl::CropHull, but sadly class-private and otherwise inaccessible.
//Adapted from PCL Source Code https://pointclouds.org/documentation/crop__hull_8hpp_source.html (pcl::CropHull<PointT>::isPointIn2DPolyWithVertIndices)
bool SpatialAnalyzer::isPointInPolygon(const pcl::PointXYZ& point, const pcl::Vertices& verts, const pcl::PointCloud<pcl::PointXYZ>& cloud)
{
	if (cloud.empty()) {
		std::cout << "isPointInPolygon: Input Cloud is empty! Returning false.\n";
		return false;
	}
	bool in_poly = false;
	float x1, x2, y1, y2;
	const int nr_poly_points = static_cast<int>(verts.vertices.size());
	unsigned int PlaneDim1 = 0;
	unsigned int PlaneDim2 = 2;
	float xold = cloud[verts.vertices[nr_poly_points - 1]].getVector3fMap()[PlaneDim1];
	float yold = cloud[verts.vertices[nr_poly_points - 1]].getVector3fMap()[PlaneDim2];
	//Test if the vectors are all co-planar; if yes, return false
	Eigen::Vector3f start{ xold, 0.0f, yold };
	start.normalize();
	int dotCoplanarCounter = 0;
	for (int i = 0; i < nr_poly_points; i++) {
		const float xnew = cloud[verts.vertices[i]].getVector3fMap()[PlaneDim1];
		const float ynew = cloud[verts.vertices[i]].getVector3fMap()[PlaneDim2];
		Eigen::Vector3f tmpV{ xnew, 0.0f, ynew };
		tmpV.normalize();
		if (start.dot(tmpV) > 0.99999f) {
			dotCoplanarCounter++;
		}
		if (dotCoplanarCounter == nr_poly_points) {
			return false; // Vertices are ALL pointing in the same direction on the XZ plane
		}
		if (xnew > xold) {
			x1 = xold;
			x2 = xnew;
			y1 = yold;
			y2 = ynew;
		}
		else {
			x1 = xnew;
			x2 = xold;
			y1 = ynew;
			y2 = yold;
		}

		if ((xnew < point.getVector3fMap()[PlaneDim1]) == (point.getVector3fMap()[PlaneDim1] <= xold) &&
			(point.getVector3fMap()[PlaneDim2] - y1) * (x2 - x1) < (y2 - y1) * (point.getVector3fMap()[PlaneDim1] - x1))
		{
			in_poly = !in_poly;
		}
		xold = xnew;
		yold = ynew;
	}
	return (in_poly);
}

template float SpatialAnalyzer::getAvgNNDist<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& inputCloud);
template float SpatialAnalyzer::getAvgNNDist<pcl::PointXYZL>(const pcl::PointCloud<pcl::PointXYZL>& inputCloud);
template float SpatialAnalyzer::getAvgNNDist<pcl::PointXYZRGB>(const pcl::PointCloud<pcl::PointXYZRGB>& inputCloud);
template float SpatialAnalyzer::getAvgNNDist<pcl::PointXYZRGBNormal>(const pcl::PointCloud<pcl::PointXYZRGBNormal>& inputCloud);
template float SpatialAnalyzer::getAvgNNDist<pcl::PointXYZRGBA>(const pcl::PointCloud<pcl::PointXYZRGBA>& inputCloud);

//Returns the average distance from a point to its nearest neighbor in the cloud
template<typename PointT>
inline float SpatialAnalyzer::getAvgNNDist(const pcl::PointCloud<PointT>& inputCloud)
{
	if (inputCloud.empty()) {
		std::cout << "getAvgNNDist Input Empty: Avg Nearest Neighbour Distance: 0" << std::endl;
		return 0.f;
	}
	auto cloudAsPtr = std::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
	pcl::copyPointCloud(inputCloud, *cloudAsPtr);
	pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;
	kdtree.setEpsilon(0.0f);
	kdtree.setInputCloud(cloudAsPtr);
	int K = 2;
	std::vector<int> pIDs(K);
	std::vector<float> pSqDists(K);
	float avg = 0.f;
	for (const auto& pPoint : cloudAsPtr->points) {
		if (kdtree.nearestKSearch(pPoint, K, pIDs, pSqDists) >= K) {
			avg += std::sqrtf(pSqDists[1]);	//0 would be the point itself.
		}
		pIDs.clear();
		pSqDists.clear();
	}
	return (avg / cloudAsPtr->size());
}
