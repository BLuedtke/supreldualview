/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#pragma once
#ifndef SpatialAnalyzer_H
#define SpatialAnalyzer_H
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/Vertices.h>

static class SpatialAnalyzer
{
public:
	//Returns a pointcloud containing all points from aCloud not also found in bCloud (spatially).
	static pcl::PointCloud<pcl::PointXYZ> getANotExactlyInB(const pcl::PointCloud<pcl::PointXYZ>& aCloud, const pcl::PointCloud<pcl::PointXYZ>& bCloud);
	//DEPRECATED
	static pcl::PointCloud<pcl::PointXYZ> getANotExactlyInB_OLD(const pcl::PointCloud<pcl::PointXYZ>& aCloud, const pcl::PointCloud<pcl::PointXYZ>& bCloud);

	//Returns a pointcloud containing all points from A which are within distThreshold of any point in bCloud WHEN projected on the XZ plane
	static pcl::PointCloud<pcl::PointXYZ> getAPointsNearBonXZ(const pcl::PointCloud<pcl::PointXYZ>& aCloud, const pcl::PointCloud<pcl::PointXYZ>& bCloud, float distThreshold);
	
	//Returns the distance from pPoint to the nearest point in inputCloud
	static float getMinimumDistToCloud(const pcl::PointCloud<pcl::PointXYZ>& inputCloud, const pcl::PointXYZ& pPoint);
	static float getMinimumDistToCloud(const std::shared_ptr<const pcl::PointCloud<pcl::PointXYZ>>& inputCloud, const pcl::PointXYZ& pPoint);
	
	//Returns true if a point is within a polygon as defined by verts and cloud.
	static bool isPointInPolygon(const pcl::PointXYZ& point, const pcl::Vertices& verts, const pcl::PointCloud<pcl::PointXYZ>& cloud);

	//Returns the number of points in srcCloud which are closest to the FIRST cloud in the ptClouds vector (compared to the other clouds in that vector)
	static int getNumberOfPointsClosestToV0(std::vector<std::shared_ptr<const pcl::PointCloud<pcl::PointXYZ>>>& ptClouds, const pcl::PointCloud<pcl::PointXYZ>& srcCloud);

	//Returns the average distance to the nearest neighbor from each point in the cloud.
	template<typename PointT>
	static float getAvgNNDist(const pcl::PointCloud<PointT>& inputCloud);
	
};
#endif /* SpatialAnalyzer_H */
