/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)

#include "Visualizer.h"
#include <iomanip>
#include <ctime>

#include <thread>
#include <vector>
#include <vtkPolyLine.h>

//Defining static var to be available to static methods
bool Visualizer::MOUSELEFTDOWN = false;
std::string Visualizer::FILENAMEINSERT = "-";

Visualizer::Visualizer()
{
	viewer = nullptr;
	//viewer->registerMouseCallback(mouseEvent, viewer);
	//viewer->registerKeyboardCallback(keyboardEventOccured, viewer);
}

void Visualizer::init()
{
	if (viewer == nullptr) {
		viewer = new pcl::visualization::PCLVisualizer();
		viewer->setSize(650, 900);
		viewer->setPosition(850, 10);
		viewer->registerMouseCallback(mouseEvent, viewer);
		//viewer->registerKeyboardCallback(keyboardEventOccured, viewer);
		std::cout << "Viewer created\n";
	}
	else {
		std::cout << "Viewer was already initialized.\n";
	}
}

//This aims to ensure that the UP-Vector of the camera stays (0,1,0), as that is the most intuitive behavior IMO
void Visualizer::mouseEvent(const pcl::visualization::MouseEvent& event_arg, void* cookie)
{
	if (event_arg.getButton() == event_arg.LeftButton && event_arg.getType() == event_arg.MouseButtonPress) {
		MOUSELEFTDOWN = true;
	}
	else if (event_arg.getButton() == event_arg.LeftButton && event_arg.getType() == event_arg.MouseButtonRelease) {
		MOUSELEFTDOWN = false;
	}
	if (MOUSELEFTDOWN) {
		try {
			pcl::visualization::PCLVisualizer* viewer = (pcl::visualization::PCLVisualizer*)cookie;
			if (viewer != nullptr) {
				std::vector<pcl::visualization::Camera> cams;
				viewer->getCameras(cams);
				const auto& c = cams.at(0);
				viewer->setCameraPosition(c.pos[0], c.pos[1], c.pos[2], 0., 1., 0.);
			}
		}
		catch (std::exception& e) {
			cout << "Could not cast back viewer cookie in MouseEvent: " << e.what() << endl;
		}
	}
}

void Visualizer::keyboardEventOccured(const pcl::visualization::KeyboardEvent& event_arg, void* cookie)
{
	try {
		pcl::visualization::PCLVisualizer* viewer = (pcl::visualization::PCLVisualizer*)cookie;
		if (viewer != nullptr) {
			//std::cout << "Pressed Keyname: " << event_arg.getKeySym() << std::endl;
		}
	}
	catch (std::exception& e) {
		cout << "Could not cast back viewer cookie in KeyboardEvent: " << e.what() << endl;
	}
}


void Visualizer::addCoordSysArrows(double scale)
{
	viewer->addCoordinateSystem(scale);
}

void Visualizer::addOBB(pcl::PointXYZ min_point_OBB, pcl::PointXYZ max_point_OBB, pcl::PointXYZ position_OBB, Eigen::Matrix3f rotational_matrix_OBB, const std::string& name)
{
	Eigen::Vector3f position(position_OBB.x, position_OBB.y, position_OBB.z);
	Eigen::Quaternionf quat(rotational_matrix_OBB);
	viewer->addCube(position, quat, max_point_OBB.x - min_point_OBB.x, max_point_OBB.y - min_point_OBB.y, max_point_OBB.z - min_point_OBB.z, name);
	viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_REPRESENTATION, pcl::visualization::PCL_VISUALIZER_REPRESENTATION_WIREFRAME, name);
}

void Visualizer::addCube(const pcl::PointXYZ& position, const pcl::PointXYZ& min_point, const pcl::PointXYZ& max_point, const Eigen::Matrix3f& rotational_matrix, const std::string& name)
{
	Eigen::Vector3f pos(position.x, position.y, position.z);
	Eigen::Quaternionf quat(rotational_matrix);
	viewer->addCube(pos, quat, max_point.x - min_point.x, max_point.y - min_point.y, max_point.z - min_point.z, name);
	viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_REPRESENTATION, pcl::visualization::PCL_VISUALIZER_REPRESENTATION_WIREFRAME, name);
}

void Visualizer::addLine(const pcl::PointXYZ& pt1, const pcl::PointXYZ& pt2, const std::string& name)
{
	viewer->addLine(pt1, pt2, 200, 200, 200, name);
}

void Visualizer::addText(const std::string& text, int wPos, int hPos, const std::string& name)
{
	viewer->addText(text, wPos, hPos, 40, 150, 255, 150, name);
}

void Visualizer::removeShape(const std::string& id)
{
	if (viewer->contains(id)) {
		viewer->removeShape(id);
	}
}

void Visualizer::removeAllShapes()
{
	viewer->removeAllShapes();
}

/// <summary>
/// <para>Starts up a Window which displays the objects previously added to this Visualizer.</para>
/// <para>Press 'q' while the windows is focused to stop the blocking/running.</para>
/// <para>In order to properly close the window, run closeWindow().</para>
/// </summary>
/// <param name="resetCamera">If set to true, the camera adjusts at the start of the visualization to better show all added objects.
/// If set to false, does not change the camera from it's default or previous position.</param>
void Visualizer::run(bool resetCamera)
{
	//std::cout << "Starting Pointcloud visualization. Press q (when viewer is the active window!) to end visualization." << std::endl;
	viewer->spinOnce(100, true);
	if (resetCamera) {
		viewer->resetCamera();
	}
	std::vector<pcl::visualization::Camera> cams{};
	using namespace std::chrono_literals;
	//std::this_thread::sleep_for(50ms);
	while (!viewer->wasStopped())
	{
		viewer->getCameras(cams);
		const auto& c = cams.at(0);
		viewer->setCameraPosition(c.pos[0], c.pos[1], c.pos[2], 0.0, 1.0, 0.0);
		viewer->spinOnce(150);
		std::this_thread::sleep_for(100ms);
	}
	viewer->resetStoppedFlag();
}

void Visualizer::runFor(int seconds, bool resetCamera)
{
	std::vector<pcl::visualization::Camera> cams;
	if (resetCamera) {
		viewer->resetCamera();
	}
	viewer->setSize(600, 900);
	viewer->setPosition(700, 10);
	
	const int millisConv = seconds * 1000;
	int millisRun = 0;
	while (!viewer->wasStopped() && millisRun < millisConv)
	{
		viewer->getCameras(cams);
		const auto& c = cams.at(0);
		viewer->setCameraPosition(c.pos[0], c.pos[1], c.pos[2], 0., 1., 0.);
		viewer->spinOnce(50);
		using namespace std::chrono_literals;
		std::this_thread::sleep_for(50ms);
		millisRun += 50;
	}
	viewer->resetStoppedFlag();
}

void Visualizer::spinOnce()
{
	std::vector<pcl::visualization::Camera> cams;
	viewer->getCameras(cams);
	const auto& c = cams.at(0);
	viewer->setCameraPosition(c.pos[0], c.pos[1], c.pos[2], 0., 1., 0.);
	viewer->spinOnce(50);
}


void Visualizer::removeAllPClouds()
{
	viewer->removeAllPointClouds();
}

void Visualizer::updatePCloudRGBManual(const std::shared_ptr<const pcl::PointCloud<pcl::PointXYZ>> &pCloud, double red, double green, double blue, const std::string& name)
{
	if (viewer->contains(name)) {
		pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> rgbData(pCloud, red, green, blue);

		viewer->updatePointCloud(pCloud, rgbData, name);
	}
	else {
		std::cout << "The viewer does not contain a Pointcloud to update with name/id " << name << "." << std::endl;
	}
}


void Visualizer::removePointCloudByName(const std::string& name)
{
	if (viewer->contains(name)) {
		viewer->removePointCloud(name);
	}
}

void Visualizer::configureCamera(const Eigen::Vector3f &position, const Eigen::Vector3f &lookAt, double fovInRad)
{
	viewer->initCameraParameters();
	viewer->setCameraPosition(position.x(), position.y(), position.z(), lookAt.x(), lookAt.y(), lookAt.z(), 0., 1., 0.);
	viewer->setCameraFieldOfView(fovInRad);
}

void Visualizer::closeWindow()
{
	viewer->close();
}


//#####################################################################################
//############################# TEMPLATED METHODS #####################################
//#####################################################################################




//#################### ADDING POINTCLOUDS DIRECTLY ####################
template void Visualizer::addPointCloud<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& pCloud, const std::string& name);
template void Visualizer::addPointCloud<pcl::PointXYZL>(const pcl::PointCloud<pcl::PointXYZL>& pCloud, const std::string& name);
template void Visualizer::addPointCloud<pcl::PointXYZRGB>(const pcl::PointCloud<pcl::PointXYZRGB>& pCloud, const std::string& name);
template void Visualizer::addPointCloud<pcl::PointXYZRGBA>(const pcl::PointCloud<pcl::PointXYZRGBA>& pCloud, const std::string& name);
//template void Visualizer::addPointCloud<pcl::PointNormal>(const pcl::PointCloud<pcl::PointNormal>& pCloud, const std::string& name);

template<typename PointT>
inline void Visualizer::addPointCloud(const pcl::PointCloud<PointT>& pCloud, const std::string& name)
{
	viewer->addPointCloud(std::make_shared<pcl::PointCloud<PointT>>(pCloud), name);
	viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, POINTSIZE, name);
}

template void Visualizer::addPointCloud<pcl::PointXYZ>(const std::shared_ptr<pcl::PointCloud<pcl::PointXYZ>>& pCloud, const std::string& name);
template void Visualizer::addPointCloud<pcl::PointXYZL>(const std::shared_ptr<pcl::PointCloud<pcl::PointXYZL>>& pCloud, const std::string& name);
template void Visualizer::addPointCloud<pcl::PointXYZRGB>(const std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>>& pCloud, const std::string& name);
template void Visualizer::addPointCloud<pcl::PointXYZRGBA>(const std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGBA>>& pCloud, const std::string& name);
//template void Visualizer::addPointCloud<pcl::PointNormal>(const std::shared_ptr<pcl::PointCloud<pcl::PointNormal>>& pCloud, const std::string& name);

template<typename PointT>
inline void Visualizer::addPointCloud(const std::shared_ptr<pcl::PointCloud<PointT>>& pCloud, const std::string& name)
{
	viewer->addPointCloud(pCloud, name);
	viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, POINTSIZE, name);
}


template void Visualizer::addPointCloud<pcl::PointXYZ>(const std::shared_ptr<const pcl::PointCloud<pcl::PointXYZ>>& pCloud, const std::string& name);
template void Visualizer::addPointCloud<pcl::PointXYZL>(const std::shared_ptr<const pcl::PointCloud<pcl::PointXYZL>>& pCloud, const std::string& name);
template void Visualizer::addPointCloud<pcl::PointXYZRGB>(const std::shared_ptr<const pcl::PointCloud<pcl::PointXYZRGB>>& pCloud, const std::string& name);
template void Visualizer::addPointCloud<pcl::PointXYZRGBA>(const std::shared_ptr<const pcl::PointCloud<pcl::PointXYZRGBA>>& pCloud, const std::string& name);
//template void Visualizer::addPointCloud<pcl::PointNormal>(const std::shared_ptr<const pcl::PointCloud<pcl::PointNormal>>& pCloud, const std::string& name);

template<typename PointT>
inline void Visualizer::addPointCloud(const std::shared_ptr<const pcl::PointCloud<PointT>>& pCloud, const std::string& name)
{
	viewer->addPointCloud(pCloud, name);
	viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, POINTSIZE, name);
}
// ###################################################################




//#################### ADDING POINTCLOUDS WITH RANDOM COLOR ####################

//Note: Calling this with something like a point type of "XYZRGBA" (which already contains color information) is untested.

template void Visualizer::addPointCloudRDMColor<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& pCloud, const std::string& name);
template void Visualizer::addPointCloudRDMColor<pcl::PointXYZL>(const pcl::PointCloud<pcl::PointXYZL>& pCloud, const std::string& name);
template void Visualizer::addPointCloudRDMColor<pcl::PointXYZRGB>(const pcl::PointCloud<pcl::PointXYZRGB>& pCloud, const std::string& name);
template void Visualizer::addPointCloudRDMColor<pcl::PointXYZRGBA>(const pcl::PointCloud<pcl::PointXYZRGBA>& pCloud, const std::string& name);

template<typename PointT>
inline void Visualizer::addPointCloudRDMColor(const pcl::PointCloud<PointT>& pCloud, const std::string& name)
{
	auto asPTR = std::make_shared<pcl::PointCloud<PointT>>(pCloud);
	pcl::visualization::PointCloudColorHandlerRandom<PointT> rdmColor(asPTR);
	viewer->addPointCloud(asPTR, rdmColor, name);
	viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, POINTSIZE, name);
}

template void Visualizer::addPointCloudRDMColor<pcl::PointXYZ>(const std::shared_ptr<pcl::PointCloud<pcl::PointXYZ>>& pCloud, const std::string& name);
template void Visualizer::addPointCloudRDMColor<pcl::PointXYZL>(const std::shared_ptr<pcl::PointCloud<pcl::PointXYZL>>& pCloud, const std::string& name);
template void Visualizer::addPointCloudRDMColor<pcl::PointXYZRGB>(const std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>>& pCloud, const std::string& name);
template void Visualizer::addPointCloudRDMColor<pcl::PointXYZRGBA>(const std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGBA>>& pCloud, const std::string& name);

template<typename PointT>
void Visualizer::addPointCloudRDMColor(const std::shared_ptr<pcl::PointCloud<PointT>>& pCloud, const std::string& name)
{
	pcl::visualization::PointCloudColorHandlerRandom<PointT> rdmColor(pCloud);
	viewer->addPointCloud(pCloud, rdmColor, name);
	viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, POINTSIZE, name);
}


// ############################################################################




//################## ADDING POINTCLOUDS WITH A USER-SPECIFIED COLOR ################

template void Visualizer::addPointCloudRGBColor<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& pCloud, double red, double green, double blue, const std::string& name);

template<typename PointT>
void Visualizer::addPointCloudRGBColor(const pcl::PointCloud<PointT>& pCloud, double red, double green, double blue, const std::string& name)
{
	auto as_ptr = std::make_shared<pcl::PointCloud<PointT>>(pCloud);
	pcl::visualization::PointCloudColorHandlerCustom<PointT> rgbData(as_ptr, red, green, blue);
	viewer->addPointCloud(as_ptr, rgbData, name);
	viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, POINTSIZE, name);
}

template void Visualizer::addPointCloudRGBColor<pcl::PointXYZ>(const std::shared_ptr<pcl::PointCloud<pcl::PointXYZ>>& pCloud, double red, double green, double blue, const std::string& name);

template<typename PointT>
void Visualizer::addPointCloudRGBColor(const std::shared_ptr<pcl::PointCloud<PointT>>& pCloud, double red, double green, double blue, const std::string& name)
{
	pcl::visualization::PointCloudColorHandlerCustom<PointT> rgbData(pCloud, red, green, blue);
	viewer->addPointCloud(pCloud, rgbData, name);
	viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, POINTSIZE, name);
}
/*
template<typename PointT>
void Visualizer::addPointCloudRGBColor(const std::shared_ptr<const pcl::PointCloud<PointT>>& pCloud, double red, double green, double blue, const std::string& name)
{
	pcl::visualization::PointCloudColorHandlerCustom<PointT> rgbData(pCloud, red, green, blue);
	viewer->addPointCloud(pCloud, rgbData, name);
}
/**/
// ################################################################################





//#################### ADDING NORMALS FOR POINTCLOUDS PREVIOUSLY ADDED ####################
template void Visualizer::addPointCloudNormals<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& pCloud, const std::shared_ptr<const pcl::PointCloud<pcl::Normal>>& pNormals, const std::string& name, unsigned int stride, float normLength);
//template void Visualizer::addPointCloudNormals<pcl::PointXYZI>(const pcl::PointCloud<pcl::PointXYZI>& pCloud, const std::shared_ptr<const pcl::PointCloud<pcl::Normal>>& pNormals, const std::string& name, unsigned int stride, float normLength);
template void Visualizer::addPointCloudNormals<pcl::PointXYZL>(const pcl::PointCloud<pcl::PointXYZL>& pCloud, const std::shared_ptr<const pcl::PointCloud<pcl::Normal>>& pNormals, const std::string& name, unsigned int stride, float normLength);
template void Visualizer::addPointCloudNormals<pcl::PointXYZRGB>(const pcl::PointCloud<pcl::PointXYZRGB>& pCloud, const std::shared_ptr<const pcl::PointCloud<pcl::Normal>>& pNormals, const std::string& name, unsigned int stride, float normLength);
template void Visualizer::addPointCloudNormals<pcl::PointXYZRGBA>(const pcl::PointCloud<pcl::PointXYZRGBA>& pCloud, const std::shared_ptr<const pcl::PointCloud<pcl::Normal>>& pNormals, const std::string& name, unsigned int stride, float normLength);

template<typename PointT>
void Visualizer::addPointCloudNormals(const pcl::PointCloud<PointT>& pCloud, const std::shared_ptr<const pcl::PointCloud<pcl::Normal>>& pNormals, const std::string& name, unsigned int stride, float normLength)
{
	viewer->addPointCloudNormals<PointT, pcl::Normal>(std::make_shared<pcl::PointCloud<PointT>>(pCloud), pNormals, stride, normLength, name);
}

template void Visualizer::addPointCloudNormals<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& pCloud, const pcl::PointCloud<pcl::Normal>& pNormals, const std::string& name, unsigned int stride, float normLength);
//template void Visualizer::addPointCloudNormals<pcl::PointXYZI>(const pcl::PointCloud<pcl::PointXYZI>& pCloud, const pcl::PointCloud<pcl::Normal>& pNormals, const std::string& name, unsigned int stride, float normLength);
template void Visualizer::addPointCloudNormals<pcl::PointXYZL>(const pcl::PointCloud<pcl::PointXYZL>& pCloud, const pcl::PointCloud<pcl::Normal>& pNormals, const std::string& name, unsigned int stride, float normLength);
template void Visualizer::addPointCloudNormals<pcl::PointXYZRGB>(const pcl::PointCloud<pcl::PointXYZRGB>& pCloud, const pcl::PointCloud<pcl::Normal>& pNormals, const std::string& name, unsigned int stride, float normLength);
template void Visualizer::addPointCloudNormals<pcl::PointXYZRGBA>(const pcl::PointCloud<pcl::PointXYZRGBA>& pCloud, const pcl::PointCloud<pcl::Normal>& pNormals, const std::string& name, unsigned int stride, float normLength);

template<typename PointT>
void Visualizer::addPointCloudNormals(const pcl::PointCloud<PointT>& pCloud, const pcl::PointCloud<pcl::Normal>& pNormals, const std::string& name, unsigned int stride, float normLength)
{
	viewer->addPointCloudNormals<PointT, pcl::Normal>(std::make_shared<pcl::PointCloud<PointT>>(pCloud), std::make_shared<pcl::PointCloud<pcl::Normal>>(pNormals), stride, normLength, name);
}

template void Visualizer::addPointCloudNormals<pcl::PointXYZ>(const std::shared_ptr<pcl::PointCloud<pcl::PointXYZ>>& pCloud, const std::shared_ptr<const pcl::PointCloud<pcl::Normal>>& pNormals, const std::string& name, unsigned int stride, float normLength);
//template void Visualizer::addPointCloudNormals<pcl::PointXYZI>(const std::shared_ptr<pcl::PointCloud<pcl::PointXYZI>>& pCloud, const std::shared_ptr<const pcl::PointCloud<pcl::Normal>>& pNormals, const std::string& name, unsigned int stride, float normLength);
template void Visualizer::addPointCloudNormals<pcl::PointXYZL>(const std::shared_ptr<pcl::PointCloud<pcl::PointXYZL>>& pCloud, const std::shared_ptr<const pcl::PointCloud<pcl::Normal>>& pNormals, const std::string& name, unsigned int stride, float normLength);
template void Visualizer::addPointCloudNormals<pcl::PointXYZRGB>(const std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>>& pCloud, const std::shared_ptr<const pcl::PointCloud<pcl::Normal>>& pNormals, const std::string& name, unsigned int stride, float normLength);
template void Visualizer::addPointCloudNormals<pcl::PointXYZRGBA>(const std::shared_ptr<pcl::PointCloud<pcl::PointXYZRGBA>>& pCloud, const std::shared_ptr<const pcl::PointCloud<pcl::Normal>>& pNormals, const std::string& name, unsigned int stride, float normLength);

template<typename PointT>
void Visualizer::addPointCloudNormals(const std::shared_ptr<pcl::PointCloud<PointT>>& pCloud, const std::shared_ptr<const pcl::PointCloud<pcl::Normal>>& pNormals, const std::string& name, unsigned int stride, float normLength)
{
	viewer->addPointCloudNormals<PointT, pcl::Normal>(pCloud, pNormals, stride, normLength, name);
}
// #################################################################################




//#################### UPDATING POINTCLOUDS ####################

template void Visualizer::updatePointcloud<pcl::PointXYZ>(const pcl::PointCloud<pcl::PointXYZ>& pCloud, const std::string& name);
//template void Visualizer::updatePointcloud<pcl::PointXYZI>(const pcl::PointCloud<pcl::PointXYZI>& pCloud, const std::string& name);
template void Visualizer::updatePointcloud<pcl::PointXYZL>(const pcl::PointCloud<pcl::PointXYZL>& pCloud, const std::string& name);
template void Visualizer::updatePointcloud<pcl::PointXYZRGB>(const pcl::PointCloud<pcl::PointXYZRGB>& pCloud, const std::string& name);
template void Visualizer::updatePointcloud<pcl::PointXYZRGBA>(const pcl::PointCloud<pcl::PointXYZRGBA>& pCloud, const std::string& name);

template<typename PointT>
void Visualizer::updatePointcloud(const pcl::PointCloud<PointT>& pCloud, const std::string& name)
{
	if (viewer->contains(name)) {
		auto asPtr = std::make_shared<pcl::PointCloud<PointT>>(pCloud);
		viewer->updatePointCloud(asPtr, name);
	}
	else {
		std::cout << "The viewer does not contain a Pointcloud to update with name/id " << name << "." << std::endl;
	}
}

template void Visualizer::updatePointcloud<pcl::PointXYZ>(const std::shared_ptr<const pcl::PointCloud<pcl::PointXYZ>>& pCloud, const std::string& name);
//template void Visualizer::updatePointcloud<pcl::PointXYZI>(const std::shared_ptr<const pcl::PointCloud<pcl::PointXYZI>>& pCloud, const std::string& name);
template void Visualizer::updatePointcloud<pcl::PointXYZL>(const std::shared_ptr<const pcl::PointCloud<pcl::PointXYZL>>& pCloud, const std::string& name);
template void Visualizer::updatePointcloud<pcl::PointXYZRGB>(const std::shared_ptr<const pcl::PointCloud<pcl::PointXYZRGB>>& pCloud, const std::string& name);
template void Visualizer::updatePointcloud<pcl::PointXYZRGBA>(const std::shared_ptr<const pcl::PointCloud<pcl::PointXYZRGBA>>& pCloud, const std::string& name);

template<typename PointT>
void Visualizer::updatePointcloud(const std::shared_ptr<const pcl::PointCloud<PointT>>& pCloud, const std::string& name)
{
	if (viewer->contains(name)) {
		viewer->updatePointCloud(pCloud, name);
	}
	else {
		std::cout << "The viewer does not contain a Pointcloud to update with name/id " << name << "." << std::endl;
	}
}
// ##########################################################
