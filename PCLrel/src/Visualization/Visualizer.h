/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#pragma once
#ifndef Visualizer_HPP
#define Visualizer_HPP
#include <iostream>
#include <memory>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <string>

/// <summary>
/// <para>Class for handling the use of PCLVisualizer, especially to capsulte it's callbacks and configurations for mouse and keyboard events. </para>
/// <para> Adjust the static public member "FILENAMEINSERT" to adjust the filename of a screenshot that is saved when pressing the button 's'.
/// The Screenshot will be saved under /data/screenshots/PCLVis"+ FILENAMEINSERT + "T" + TIME + ".png". </para>
/// </summary>
class Visualizer
{
public:
    Visualizer();
    
    //This MUST be called before calling anything else.
    void init();

    //Adds a Pointcloud to visualize once the visualizer is run. 
    // The "Name" field convention: Use a different name every time (essentially acts like an ID)

    template<typename PointT>
    void addPointCloud(const pcl::PointCloud<PointT>& pCloud, const std::string &name);
    template<typename PointT>
    void addPointCloud(const std::shared_ptr<pcl::PointCloud<PointT>>& pCloud, const std::string& name);
    template<typename PointT>
    void addPointCloud(const std::shared_ptr<const pcl::PointCloud<PointT>>& pCloud, const std::string& name);
    
    // Adding PointClouds with random color (one color for the whole cloud, not per point)

    template<typename PointT>
    void addPointCloudRDMColor(const pcl::PointCloud<PointT>& pCloud, const std::string& name);
    template<typename PointT>
    void addPointCloudRDMColor(const std::shared_ptr<pcl::PointCloud<PointT>>& pCloud, const std::string& name);
    template<typename PointT>
    void addPointCloudRDMColor(const std::shared_ptr<const pcl::PointCloud<PointT>>& pCloud, const std::string& name);
    

    // Adding PointClouds with User-defined RGB Color.

    template<typename PointT>
    void addPointCloudRGBColor(const std::shared_ptr<pcl::PointCloud<PointT>>& pCloud, double red, double green, double blue, const std::string& name);
    template<typename PointT>
    void addPointCloudRGBColor(const std::shared_ptr<const pcl::PointCloud<PointT>>& pCloud, double red, double green, double blue, const std::string& name);
    template<typename PointT>
    void addPointCloudRGBColor(const pcl::PointCloud<PointT>& pCloud, double red, double green, double blue, const std::string& name);


    // Adding Normals for existing (i.e., already added) PointClouds
    
    template<typename PointT>
    void addPointCloudNormals(const pcl::PointCloud<PointT>& pCloud, const std::shared_ptr<const pcl::PointCloud<pcl::Normal>>& pNormals, const std::string& name, unsigned int stride = 1, float normLength = 0.015f);
    template<typename PointT>
    void addPointCloudNormals(const pcl::PointCloud<PointT>& pCloud, const pcl::PointCloud<pcl::Normal>& pNormals, const std::string& name, unsigned int stride = 1, float normLength = 0.015f);
    template<typename PointT>
    void addPointCloudNormals(const std::shared_ptr<pcl::PointCloud<PointT>>& pCloud, const std::shared_ptr<const pcl::PointCloud<pcl::Normal>>& pNormals, const std::string& name, unsigned int stride = 1, float normLength = 0.015f);



    // Update a PointCloud found based on the 'name' field. Recommended: Use the same PointT types when updating as the ones used for actually adding it.
    
    template<typename PointT>
    void updatePointcloud(const pcl::PointCloud<PointT>& pCloud, const std::string& name);
    template<typename PointT>
    void updatePointcloud(const std::shared_ptr<const pcl::PointCloud<PointT>>& pCloud, const std::string& name);


    // Specific Update case for Pointclouds with manual color setting
    void updatePCloudRGBManual(const std::shared_ptr<const pcl::PointCloud<pcl::PointXYZ>> &pCloud, double red, double green, double blue, const std::string &name);
    

    // Adding other types of objects
    
    void addCoordSysArrows(double scale);
    void addOBB(pcl::PointXYZ min_point_OBB, pcl::PointXYZ max_point_OBB, pcl::PointXYZ position_OBB, Eigen::Matrix3f rotational_matrix_OBB, const std::string& name);
    void addCube(const pcl::PointXYZ& position, const pcl::PointXYZ& min_point, const pcl::PointXYZ& max_point, const Eigen::Matrix3f& rotational_matrix, const std::string& name);
    void addLine(const pcl::PointXYZ& pt1, const pcl::PointXYZ& pt2, const std::string& name);
    void addText(const std::string& text, int wPos, int hPos, const std::string& name);

    // Removing a shape (e.g. a cube, line, ...)
    
    //void removeText(const std::string& name);
    void removeShape(const std::string& id);
    void removeAllShapes();

    // Remove all previously added Point Clouds
    void removeAllPClouds();

    // Remove a specific Object by its name
    void removePointCloudByName(const std::string &name);
    
    // Adjusts camera parameters
    void configureCamera(const Eigen::Vector3f &position, const Eigen::Vector3f &lookAt, double fovInRad);

    // Execute/Show Window and visualize
    void run(bool resetCamera = false);
    void runFor(int seconds, bool resetCamera = false);

    // Close the Window
    void closeWindow();

    // Experimental Method for visualizing LCCP-Segmentation
    //void addLCCPExperimental(pcl::LCCPSegmentation<pcl::PointXYZ>::SupervoxelAdjacencyList& adjList, std::map<std::uint32_t, std::shared_ptr<pcl::Supervoxel<pcl::PointXYZ>>>& supervoxel_clusters);

    //Experimental
    void spinOnce();

    static std::string FILENAMEINSERT;
    double POINTSIZE = 1.0;

private:
    pcl::visualization::PCLVisualizer* viewer;
    static void mouseEvent(const pcl::visualization::MouseEvent& event_arg, void* cookie);
    static void keyboardEventOccured(const pcl::visualization::KeyboardEvent& event_arg, void* cookie);
    static bool MOUSELEFTDOWN;
};






#endif /* Visualizer_HPP */
