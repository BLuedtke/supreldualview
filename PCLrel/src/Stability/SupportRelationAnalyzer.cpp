/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#include "SupportRelationAnalyzer.h"

// LOCAL includes
#include "../Segmentation/Segment.h"
#include "../Spatial/SpatialAnalyzer.h"

// STD includes
#include <iomanip>
#include <sstream>

// PCL/BOOST includes
#include <pcl/filters/crop_hull.h>
#include <pcl/filters/crop_box.h>
#include <pcl/common/common.h>
#include <pcl/common/centroid.h>
#include <pcl/features/moment_of_inertia_estimation.h>
#include <boost/log/trivial.hpp>

using std::vector;
using std::shared_ptr;
using _PointT = pcl::PointXYZ;
using PointCloudXYZ = pcl::PointCloud<pcl::PointXYZ>;
using std::cout;

//####################################### CONSTRUCTORS ###################################

SupportRelationAnalyzer::SupportRelationAnalyzer()
	: config {true, 0.0f}
{
	;
}
SupportRelationAnalyzer::SupportRelationAnalyzer(const std::vector<Segment>& sceneSegments, bool useGenerousHulls, float voxelSize)
	: config {useGenerousHulls, voxelSize}
{
	this->segments = sceneSegments;
}
SupportRelationAnalyzer::SupportRelationAnalyzer(std::vector<Segment>&& sceneSegments, bool useGenerousHulls, float voxelSize)
	: config { useGenerousHulls, voxelSize }
{
	this->segments = std::move(sceneSegments);
}

//####################################### SETTERS ###################################

void SupportRelationAnalyzer::setInputSceneSegments(const std::vector<Segment>& sceneSegments)
{
	this->segments.clear();
	this->voxelSegmentMap.clear();
	this->segments = sceneSegments;
}

void SupportRelationAnalyzer::setInputSceneSegments(std::vector<Segment>&& sceneSegments)
{
	this->segments.clear();
	this->voxelSegmentMap.clear();
	this->segments = std::move(sceneSegments);
}



//####################################### GETTERS ###################################


bool SupportRelationAnalyzer::inputIsEmpty() const
{
	return this->segments.empty();
}

//####################################### HELPER(S) ###################################


inline float getAvgNNDistOverVector(const std::vector<Segment>& segments) {
	float nnDists = 0.0f;
	for (const auto& seg : segments) {
		nnDists += SpatialAnalyzer::getAvgNNDist(*seg.getInputCloud());
	}
	nnDists = (segments.size() > 0) ? (nnDists / segments.size()) : nnDists;
	return nnDists;
}

inline pcl::PointCloud<pcl::PointXYZ> combineAllHullClouds(const std::vector<Segment>& segments) {
	pcl::PointCloud<pcl::PointXYZ> combiAll{};
	for (const auto& seg : segments) {
		combiAll += seg.getHullCloud();
	}
	return combiAll;
}


//Clear scene, but not configuration
void SupportRelationAnalyzer::resetScene()
{
	this->segments.clear();
	this->voxelSegmentMap.clear();
}

float SupportRelationAnalyzer::guessVoxelSize(const std::vector<Segment>& segments)
{
	if (segments.empty()) {
		BOOST_LOG_TRIVIAL(debug) << "Guessing Voxel Size - Segment input vector is empty";
		BOOST_LOG_TRIVIAL(error) << "Internal Error: Could not make a guess for voxel size to use";
	}
	const auto nnDists = getAvgNNDistOverVector(segments);
	auto voxSize = nnDists * 3.0f;

	const auto combiAll = combineAllHullClouds(segments);
	Eigen::Vector4f minAll, maxAll;
	pcl::getMinMax3D(combiAll, minAll, maxAll);

	std::uint64_t approxMaxVoxCount = 0;
	const auto aBBspan = Eigen::Vector3f(maxAll.x() - minAll.x(), maxAll.y() - minAll.y(), maxAll.z() - minAll.z());
	do
	{
		if (approxMaxVoxCount > 1000000000) {
			BOOST_LOG_TRIVIAL(debug) << "Increasing Voxel Size due to possible too high Voxel Count, apprx Max count: " << approxMaxVoxCount;
			voxSize *= 1.5f;
		}
		//Very very roughly approximate how many voxels this is going to take.
		// Change VoxelSize if it would be too many (>1 000 000 000)
		const auto xVoxelCount = static_cast<std::uint64_t>(std::ceilf(aBBspan.x() / voxSize)) + 1;
		const auto yVoxelCount = static_cast<std::uint64_t>(std::ceilf(aBBspan.y() / voxSize)) + 1;
		const auto zVoxelCount = static_cast<std::uint64_t>(std::ceilf(aBBspan.z() / voxSize)) + 1;

		approxMaxVoxCount = xVoxelCount * yVoxelCount * zVoxelCount;
	} while (approxMaxVoxCount > 1000000000);
	BOOST_LOG_TRIVIAL(debug) << "Chosen voxel size: " << voxSize;
	return voxSize;
}

void SupportRelationAnalyzer::prepareSegments(std::vector<Segment>& segments)
{
	const auto combiAll = combineAllHullClouds(segments);
	Eigen::Vector4f minAll, maxAll;
	pcl::getMinMax3D(combiAll, minAll, maxAll);
	const auto min3 = Eigen::Vector3f(minAll.x(), minAll.y(), minAll.z());

	const float baseOffset = 0.05f;
	const auto offsetFromOrigin = Eigen::Vector3f(baseOffset, baseOffset, baseOffset);
	const auto combinedOffset = -min3 + offsetFromOrigin;
	for (auto& seg : segments) {
		seg.translateSegment(combinedOffset);
	}
}


std::vector<pcl::PointCloud<pcl::PointXYZ>> SupportRelationAnalyzer::getVoxelRepresentationAfterEvaluate() const
{
	if (voxelSegmentMap.empty()) {
		BOOST_LOG_TRIVIAL(warning) << "  getVoxelRepresentationAfterEvaluate: No Voxel Representation available.";
		return std::vector<pcl::PointCloud<pcl::PointXYZ>>();
	}
	std::vector<pcl::PointCloud<pcl::PointXYZ>> retVec {};
	for (const auto& entry : voxelSegmentMap) {
		retVec.push_back(entry.second.getVoxels());
	}
	return retVec;
}

std::map<int, pcl::PointCloud<pcl::PointXYZ>> SupportRelationAnalyzer::getVoxelIntersectionRepresentationAfterEvaluate() const
{
	std::map<int, pcl::PointCloud<pcl::PointXYZ>> retMap {};
	for (const auto& vxS : voxelSegmentMap) {
		const auto& intersections = vxS.second.getIntersections();
		for (const auto& isec : intersections) {
			retMap[vxS.first] += *isec.second.first;
		}
	}
	return retMap;
}

pcl::PointCloud<pcl::PointXYZ> SupportRelationAnalyzer::getVoxelRepresentationForSegmentID(int id) const
{
	if (voxelSegmentMap.empty()) {
		BOOST_LOG_TRIVIAL(warning) << "getVoxelRepresentationForSegmentID: No Voxel Segments available.";
		return pcl::PointCloud<pcl::PointXYZ>();
	}
	if (voxelSegmentMap.count(id) != 0) {
		return voxelSegmentMap.at(id).getVoxels();
	}
	else {
		BOOST_LOG_TRIVIAL(warning) << "getVoxelRepresentationForSegmentID: No Voxel Segment with this id available.";
		return pcl::PointCloud<pcl::PointXYZ>();
	}
}


//####################################### EVALUATION ###################################

/// <summary>
/// <para>Based on the Segments and VoxelSize set for this SupportRelationController, 
/// uses Voxel-Representations of the Segments to check the physical support relations.</para>
/// 
/// </summary>
/// <returns><para>-1 if voxelSize is not set greater 0 or if there are no segments.</para>
/// <para>-2 if any intermediate Voxel-Generation fails.</para><para>-3 if there are floating VoxelSegments.</para> 
/// <para>-4 if scene is unstable.</para><para> 0 if scene is physically plausible.</para></returns>
int SupportRelationAnalyzer::evaluateSupportRelations()
{
	if (!(config.voxelSize > 0.0f)) {
		BOOST_LOG_TRIVIAL(error) << "evaluateSupportRelations: VoxelSize is <= 0.0, set VoxelSize before evaluating!";
		return -1;
	}
	if (segments.empty()) {
		BOOST_LOG_TRIVIAL(error) << "evaluateSupportRelations: Can't evaluate Support Relations with no segments in the scene.";
		return -1;
	}
	// Create VoxelSegments
	if (setVoxelSegmentMapFromScene(true) != true) {
		BOOST_LOG_TRIVIAL(error) << "evaluateSupportRelations: Determining Voxel representation of Segments unsuccessful.";
		return -2;
	}
	//cout << "Voxel Segments: \n";
	//for (const auto& vxSegment : voxelSegmentMap) {
		//vxSegment.second.printOutRels();
	//}

	//VoxelSegments are now available via private member "voxelSegmentMap"
	BOOST_LOG_TRIVIAL(info) << "Starting floating check";
	
	// Check simple condition: No Segment (except for the floor) shall be allowed to float.
	bool ret = checkFloatViolation(voxelSegmentMap);
	if (!ret) {
		BOOST_LOG_TRIVIAL(trace) << "evaluateSupportRelations End: Floating Segment";
		return -3; //At least one Segment is floating
	}
	// Check Stability/Support relationships between Segments
	BOOST_LOG_TRIVIAL(info) << "Float check passed, now check stability";

	ret = checkStability(voxelSegmentMap);
	if (!ret) {
		BOOST_LOG_TRIVIAL(debug) << "evaluateSupportRelations End: Unstable";
		return -4;
	}
	else {
		BOOST_LOG_TRIVIAL(debug) << "evaluateSupportRelations End: Stable";
		return 0;
	}
}

std::vector<int> SupportRelationAnalyzer::findCriticalSegments()
{
	//TODO add check if this worked (via returned bool)
	setVoxelSegmentMapFromScene(false);
	
	vector<int> idsToCheckWith {};
	for (const auto& element : voxelSegmentMap) {
		// The floor shouldn't be removed in this process
		if (!element.second.getIsFloor()) {
			idsToCheckWith.push_back(element.first);
		}
	}
	//for (int i = 0; i < idsToCheckWith.size(); ++i) {
		//pcl::PointXYZ ctr {};
		//pcl::computeCentroid(voxelSegmentMap.at(idsToCheckWith.at(i)).getVoxels(), ctr);
		//BOOST_LOG_TRIVIAL(trace) << "#" << idsToCheckWith.at(i) << " ctr: " << ctr << "\n";
		//cout << "#"<< idsToCheckWith.at(i)<< " ctr: " << ctr << "\n";
		//voxelSegmentMap.at(idsToCheckWith.at(i)).printOutRels();
	//}
	
	// Create a copy of the voxelSegmentMap and remove one segment in idsToCheckWith,
	// then check if the scene is still stable. Repeat with next ID etc 
	// (after 1 removal we copy anew, so no 2 or more segments removed at same time)

	std::vector<int> ret {};
	for (size_t i = 0; i < idsToCheckWith.size(); ++i)
	{
		std::map<int, VoxelSegment> cpyVoxMap{ this->voxelSegmentMap };
		
		removeVoxelSegmentMentionsFromMap(cpyVoxMap, idsToCheckWith[i]);
		if (checkFloatViolation(cpyVoxMap) && checkStability(cpyVoxMap,3)) {
			//cout << "Scene STABLE without VXsegmentID " << idsToCheckWith[i] << "\n";
		}
		else {
			//cout << "Scene UNSTABLE without VXsegmentID " << idsToCheckWith[i] << "\n";
			//Unstable -> Critical Segment
			ret.push_back(idsToCheckWith[i]);
		}
	}
	BOOST_LOG_TRIVIAL(trace) << "Criticial Segment Search Finished";
	return ret;
}

//Checks if there are segments which float (apart from the floor), i.e., don't have an intersection
// which is supported by a different segment.
// Returns TRUE if there are NO floating segments.
bool SupportRelationAnalyzer::checkFloatViolation(const std::map<int, VoxelSegment>& voxSegments)
{
	BOOST_LOG_TRIVIAL(trace) << "checkFloatViolation Start";
	for (auto const& voxSeg1 : voxSegments) {
		//The Floor is expected to not have any supports itself, so skip it.
		if (!voxSeg1.second.getIsFloor()) {
			if (voxSeg1.second.getNumberOfSupportingIntersections() == 0) {
				BOOST_LOG_TRIVIAL(info) << "FLOATING_SEGMENT_VIOLATION:   VoxelSegment with ID " << voxSeg1.first << " has no Intersections with any other VoxelSegments -> Floating";
				BOOST_LOG_TRIVIAL(trace) << "checkFloatViolation End: Floating";
				return false;
			}
		}
	}
	BOOST_LOG_TRIVIAL(trace) << "checkFloatViolation End: Nothing Floats";
	return true;
}

inline int getFloorIDOverVoxSegmentMap(const std::map<int, VoxelSegment>& voxSegments) {
	for (const auto& vSeg : voxSegments) {
		if (vSeg.second.getIsFloor()) {
			return vSeg.first;
		}
	}
	return -10;
}

//Checks the stability of the scene by initiating the bottom-up traverse and integration
// for every segment standing on the floor.
// Returns TRUE if the scene is deemed STABLE.
bool SupportRelationAnalyzer::checkStability(const std::map<int, VoxelSegment>& voxSegments, int indentLevel)
{
	BOOST_LOG_TRIVIAL(trace) << "checkStability Start";
	const auto floorID = getFloorIDOverVoxSegmentMap(voxSegments);
	if (floorID == -10) {
		BOOST_LOG_TRIVIAL(debug) << "checkStability End: Floor VoxSegment COULD NOT BE FOUND";
		return false;
	}
	const auto& floorVoxSeg = voxSegments.at(floorID);
	if (floorVoxSeg.getIntersections().empty() && voxSegments.size() > 1) {
		BOOST_LOG_TRIVIAL(debug) << "checkStability End: Floor VoxSegment has no intersections, but there are other segments in the scene! Shouldn't be possible";
		return false;
	}

	//For every Intersection of floor with a VoxelSegment above, perform the integrateUpwards stability check.
	for (const auto& isec : floorVoxSeg.getIntersections()) {
		if (isec.second.second) {
			BOOST_LOG_TRIVIAL(debug) << " checkStability: Floor VoxSegment has supportive intersections, but the floor should not be supported by anything!";
		}
		else {
			BOOST_LOG_TRIVIAL(trace) << std::string(indentLevel, ' ') << "CHECK: VX#" << isec.first << " standing on Floor(#" << floorID << ")";
			integrationTrace.clear();
			const auto retCloud = this->integrateUpwards(voxSegments, floorID, isec.first, floorID, indentLevel);
			if (retCloud.empty()) {
				BOOST_LOG_TRIVIAL(trace) << std::string(indentLevel, ' ') << " ===> UNSTABLE: VX#"<< isec.first << " standing on " << floorID << " integration returned empty.";
				BOOST_LOG_TRIVIAL(trace) << "checkStability End: Unstable";
				return false;
			}
			else {
				BOOST_LOG_TRIVIAL(trace) << std::string(indentLevel, ' ') << " ===> STABLE: VX#" << isec.first << " integration is stable on the floor (VX#" << floorID << ").";
			}
		}
	}
	//All stability tests have passed.
	BOOST_LOG_TRIVIAL(trace) << "checkStability End: Stable";
	return true;
}


//####################################### TRAVERSAL AND INTEGRATION ###################################

//Heart of the stability analysis. Better Doc to follow.
// voxSegments: Map of <id,VoxelSegment>
// segIDSupport: ID of lower/supporting Segment 
// segToIntegrateID: ID of upper/resting Segment to be integrated into segIDSupport
// floorID: ID of the floor to make sure it is not considered for integration
pcl::PointCloud<pcl::PointXYZ> SupportRelationAnalyzer::integrateUpwards(const std::map<int, VoxelSegment>& voxSegments, int segSupportID, int segToIntegrateID, int floorID, int indntLvl)
{
	using std::string;
	indntLvl = indntLvl + 3;
	if (segToIntegrateID == floorID) {
		integrateOutputErrorOut(-1, segSupportID, segToIntegrateID, indntLvl, "Trying to integrate the floor - forbidden.");
		BOOST_LOG_TRIVIAL(warning) << "Internal Error: Stability analysis internal inconsistencies. Results may be inaccurate/unreliable";
		return PointCloudXYZ();
	}
	if (voxSegments.count(segToIntegrateID) == 0) {
		BOOST_LOG_TRIVIAL(debug) << "INTEGRATE ALARM: segToIntegrateID ID NOT FOUND IN MAP";
		BOOST_LOG_TRIVIAL(warning) << "Internal Error: Stability analysis internal inconsistencies. Results may be inaccurate/unreliable";
		return PointCloudXYZ();
	}
	if (voxSegments.count(segSupportID) == 0) {
		BOOST_LOG_TRIVIAL(debug) << "INTEGRATE ALARM: segSupportID ID NOT FOUND IN MAP";
		BOOST_LOG_TRIVIAL(warning) << "Internal Error: Stability analysis internal inconsistencies. Results may be inaccurate/unreliable";
		return PointCloudXYZ();
	}
	const auto& segToIntgrt = voxSegments.at(segToIntegrateID);
	const auto& segSupport = voxSegments.at(segSupportID);

	//Check that the two segments to be integrated intersect each other, and the intersection is classified as supportive for segToIntgrt
	if (!(segSupport.hasIntersectionWith(segToIntgrt.getId()) && segToIntgrt.hasSupportiveIntersectionWith(segSupport.getId()))) {
		integrateOutputErrorOut(-1, segSupportID, segToIntegrateID, indntLvl, "Integration Support and Integration Segment do not intersect.");
		return PointCloudXYZ();
	}

	if (segToIntgrt.getIntersections().size() == 1) {
		//CASE 1
		return this->integrateCase1(voxSegments, segSupportID, segToIntegrateID, floorID, indntLvl);
	}
	else {
		//CASE 2 OR 3
		return this->integrateCase2or3(voxSegments, segSupportID, segToIntegrateID, floorID, indntLvl);
	}
}

pcl::PointCloud<pcl::PointXYZ> SupportRelationAnalyzer::integrateCase1(const std::map<int, VoxelSegment>& voxSegments, int segSupportID, int segToIntegrateID, int floorID, int indntLvl)
{
	using std::string;
	lastIntegration = { segSupportID, segToIntegrateID };
	if (voxSegments.count(segToIntegrateID) == 0 || voxSegments.count(segSupportID) == 0) {
		BOOST_LOG_TRIVIAL(debug) << "INTEGRATE CASE 1 ALARM: One of the two VXs not in Map";
		BOOST_LOG_TRIVIAL(error) << "Internal Error: Stability Check Scene IDs inconsistent";
		return PointCloudXYZ();
	}
	else {
		const auto& segToIntgrt = voxSegments.at(segToIntegrateID);
		//Case 1: 
		// The Segment segToIntgrt has no other intersections, so segSupport is the only supporter -> Easiest case.
		// 'Only' have to check the stability. If stable, return the points of segToIntgrt that are not already in segSupport/the intersection.
		BOOST_LOG_TRIVIAL(trace) << string(indntLvl, ' ') << " CASE 1: VX#" << segToIntegrateID << " standing on VX#" << segSupportID << " with no other contact.";
		if (segToIntgrt.checkIntersectionStabilityAllSupports(config.voxelSize)) {
			//Stable on support
			//Only return the Voxels of segToIntgrt that are not already in segSupport
			if (segToIntgrt.getVoxels().empty() || segToIntgrt.getIntersections().count(segSupportID) == 0) {
				BOOST_LOG_TRIVIAL(debug) << "INTEGRATE CASE 1 ALARM: Segment to integrate has no voxels or does not intersect with segSupportID -> Return Empty";
				BOOST_LOG_TRIVIAL(error) << "Internal Error: Stability Check Scene contact/intersections inconsistent";
				return PointCloudXYZ();
			}
			else if (segToIntgrt.getIntersections().at(segSupportID).first->empty()) {
				BOOST_LOG_TRIVIAL(debug) << "INTEGRATE CASE 1 ALARM: Intersection in segToIntgrt has no voxels -> Return Empty";
				BOOST_LOG_TRIVIAL(error) << "Internal Error: Stability Check Scene segment inconsistent";
				return PointCloudXYZ();
			}
			auto integrCloud = SpatialAnalyzer::getANotExactlyInB(segToIntgrt.getVoxels(), *(segToIntgrt.getIntersections().at(segSupportID).first));
			if (integrCloud.empty()) {
				BOOST_LOG_TRIVIAL(trace) << string(indntLvl, ' ') << "        => integrCloud is empty -> Contact but no weight transfer";
			} else {
				BOOST_LOG_TRIVIAL(trace) << string(indntLvl, ' ') << "        => Stable.";
				integrationTrace.push_back(segToIntegrateID);
			}
			return integrCloud;
		}
		else {
			//Unstable on support
			BOOST_LOG_TRIVIAL(trace) << string(indntLvl, ' ') << " CASE 1: VX#" << segToIntegrateID << " is not stable on VX#" << segSupportID;
			return PointCloudXYZ();
		}
	}
}

pcl::PointCloud<pcl::PointXYZ> SupportRelationAnalyzer::integrateCase2or3(const std::map<int, VoxelSegment>& voxSegments, int segSupportID, int segToIntegrateID, int floorID, int indntLvl)
{
	using std::string;
	//Combine commonalities between cases 2 and 3
	if (voxSegments.count(segToIntegrateID) == 0) {
		BOOST_LOG_TRIVIAL(debug) << "INTEGRATE CASE 2 OR 3 ALARM: segToIntegrateID ID not in Map";
		//TODO check if this return here is correct
		return PointCloudXYZ();
	}
	VoxelSegment workingVSeg = voxSegments.at(segToIntegrateID);
	int caseSep = 0;
	if (workingVSeg.hasUnsupportiveIntersections()) {
		//CASE 3: segToIntgrt/workingVSeg has VoxelSegments lying on it, and maybe other supporting segments
		caseSep = 3;
		BOOST_LOG_TRIVIAL(trace) << string(indntLvl, ' ') << " CASE: 3 | Supporter: " << segSupportID << "; Upper: " << segToIntegrateID;
		BOOST_LOG_TRIVIAL(trace) << string(indntLvl, ' ') << " -> First Integrtng TmpVX#" << segToIntegrateID << " with VXs above, then check stability on VX#" << segSupportID;
		PointCloudXYZ relyIntegrate {};
		const auto traceSize = integrationTrace.size();
		for (const auto& isec : workingVSeg.getIntersections()) {
			if (isec.first != segSupportID && isec.second.second == false) {
				const auto upwardsCloud = integrateUpwards(voxSegments, workingVSeg.getId(), isec.first, floorID, indntLvl);				//RECURSIVE CALL
				if (!upwardsCloud.empty()) {
					relyIntegrate += upwardsCloud;
					BOOST_LOG_TRIVIAL(trace) << string(indntLvl, ' ') << " ==> Integrated TempVX#" << segToIntegrateID << " with VC#" << lastIntegration.second;
				}
				else {
					//integrateOutputErrorOut(caseSep, segToIntegrateID, isec.first, indntLvl, "recursive call returned empty.");
					BOOST_LOG_TRIVIAL(trace) << string(indntLvl, ' ') << " ==> Integration with VX#" << lastIntegration.second << " returned empty - unstable.";
					return PointCloudXYZ();
				}
			}
		}
		if (integrationTrace.size() > traceSize) {
			std::stringstream traceStream{};
			traceStream << string(indntLvl, ' ') << "Segments Integrated into " << segToIntegrateID << ": ";
			for (size_t i = traceSize; i < integrationTrace.size(); ++i) {
				traceStream << integrationTrace[i];
				if (1 + i == integrationTrace.size()) {
					traceStream << ".\n";
				}
				else {
					traceStream << ", ";
				}
			}
			BOOST_LOG_TRIVIAL(trace) << traceStream.str();
		}
		//Now the relyIntegrate PCloud represents the relying (upper) segments of SegToIntgrt, but without segToIntgrt itself.
		// WorkingVSeg now represents the combination of those relying voxels with the SegToIntgrt itself.
		// It only has the intersections that SegToIntgrt has, as the intersections of the upper VSegs have already been checked.
		workingVSeg.addVoxels(relyIntegrate);
	}
	else { //CASE 2 Debug/Output setting, does not actually need anything else done.
		caseSep = 2;
		BOOST_LOG_TRIVIAL(trace) << string(indntLvl, '-') << " CASE: 2 | Supporter: " << segSupportID << "; Upper: " << segToIntegrateID;
	}
	// CASE 2-3 COMMON, Check the Stability of workingVSeg.
	if (workingVSeg.checkIntersectionStabilityAllSupports(config.voxelSize)) {
		// If workingVSeg is resting on another support, we shall only add the voxels to the output which actually rest on the supporting intersection of segSupport.
		// If workingVSeg is solely resting on us, calling the getVoxelsSupportedBy[..] is useful, as it removes the duplicate points of lower intersections (desirable).
		//This is not perfectly physically accurate, but rather a rough approximation
		PointCloudXYZ integrCloud{ workingVSeg.getVoxelsSupportedByVoxIDContactRelaxedWithWeight(segSupportID, config.voxelSize) };
		
		if (integrCloud.empty()) {
			//Only the intersection itself relies on segSupport. Very weird case, but not an error-case per se.
			BOOST_LOG_TRIVIAL(trace) << string(indntLvl, '-') << " CASE: " << caseSep << " integrCloud is empty -> Contact but no weight transfer. Might still be stable.";
			//TODO: It's nice that I wrote "might still be stable", but the caller of this recursive call won't know that. have to fix that somehow.
		}
		else {
			BOOST_LOG_TRIVIAL(trace) << string(indntLvl, '-') << " CASE: " << caseSep << " SUCCESSFULL";
		}
		//We do not have to add workingVSeg to this, as that concatenation is performed by the caller (see Case 3).
		lastIntegration = { segSupportID, segToIntegrateID };
		integrationTrace.push_back(segToIntegrateID);
		return integrCloud;
	}
	else {
		integrateOutputErrorOut(caseSep, segSupportID, segToIntegrateID, indntLvl, "(+ integrated) are not stable.");
		BOOST_LOG_TRIVIAL(trace) << string(indntLvl, '-') << " CASE: " << caseSep << " Number of Supporting Intersections for WorkingVSeg: " << workingVSeg.getNumberOfSupportingIntersections();
		lastIntegration = { segSupportID, segToIntegrateID };
		return PointCloudXYZ();
	}
}

void SupportRelationAnalyzer::integrateOutputErrorOut(int caseNum, int segSupportID, int segToIntgrtID, int indtLvl, const std::string& message)
{
	std::string sCase;
	if (caseNum < 1) {
		sCase = "PRE_Case";
	}
	else {
		sCase = "CASE " + std::to_string(caseNum);
	}
	BOOST_LOG_TRIVIAL(debug) << std::string(indtLvl, '-') << " ALARM " << sCase << ": SegSupID " << segSupportID << ", segIntgrtID " << segToIntgrtID << "=> " << message;
}


//####################################### VOXEL SEGMENT/SCENE DETERMINATION ###################################

//returns true on success, false on failure
bool SupportRelationAnalyzer::setVoxelSegmentMapFromScene(bool reset)
{
	if (!voxelSegmentMap.empty()) {
		if (reset) {
			voxelSegmentMap.clear();
		}
		else { //Already set and no need to calculate it again
			return true;
		}
	}
	// Create VoxelSegments
	BOOST_LOG_TRIVIAL(trace) << "Creating Voxel Segments. Segments Size: " << segments.size();

	int maxId = -1;
	for (const auto& seg : segments) {
		_PointT minPt{}, maxPt{};
		pcl::getMinMax3D(seg.getHullCloud(), minPt, maxPt);
		if (minPt.x == maxPt.x || minPt.y == maxPt.y || minPt.z == maxPt.z) {
			//"Flat" in at least one dimension. This will cause issues with CropHull!
			BOOST_LOG_TRIVIAL(debug) << "Segment to be added as VoxelSegment has one dimension with minpt=maxpt in that plane."
				<< " This will likely cause issues with CropHull next. SupportRelationAnalyzer CPP setVoxelSegmentMapFromScene.";
			BOOST_LOG_TRIVIAL(warning) << "Internal error when building voxel scene! Results unreliable";
		}
		
		const auto voxelBaseSegment = getVoxelBaseGridForSegment(seg);
		
		const auto iVox = getInlyingVoxels(seg, voxelBaseSegment);
		
		//The value also holds the key -> weird, but necessary workaround
		voxelSegmentMap.insert({ seg.getId(), VoxelSegment(seg.getId(), iVox) });
		
		maxId = (seg.getId() > maxId) ? seg.getId() : maxId; //Update maxId
	}
	if (voxelSegmentMap.empty()) {
		BOOST_LOG_TRIVIAL(debug) << "setVoxelSegmentMapFromScene: No Voxel Segments could be created";
		return false;
	}
	// Add VoxelSegment to represent the floor
	const auto floorVox = getFloorVoxels(voxelSegmentMap);
	if (floorVox.empty()) {
		BOOST_LOG_TRIVIAL(debug) << "setVoxelSegmentMapFromScene: Floor Voxels are empty";
		return false;
	}
	voxelSegmentMap.insert({ maxId + 1, VoxelSegment(maxId + 1, floorVox, true) });
	//cout << "VoxelSegmentMap Finished Size: " << voxelSegmentMap.size() << ".\n";
	//cout << "Map IDs: ";
	//for (const auto& voxSeg : voxelSegmentMap) {
		//cout << voxSeg.first << ", ";
	//}
	//cout << "\n";
	// Determine intersections between segments with each other (incl. the floor).
	for (auto& voxSeg : voxelSegmentMap) {
		for (auto& voxSeg2 : voxelSegmentMap) {
			if (voxSeg.first != voxSeg2.first) {
				VoxelSegment::COMPUTEANDSETINTERSECTION(voxSeg.second, voxSeg2.second, config.voxelSize);
			}
		}
	}
	
	BOOST_LOG_TRIVIAL(trace) << "setVoxelSegmentMapFromScene End";
	return true;
}

//Helper for removing a VoxelSegment from the current scene.
void SupportRelationAnalyzer::removeVoxelSegmentMentionsFromMap(std::map<int, VoxelSegment>& voxSegments, int id)
{
	for (auto& voxSegEntry : voxSegments) {
		voxSegEntry.second.removeIntersection(id);
	}
	auto elIT = voxSegments.find(id);
	if (elIT != voxSegments.end()) {
		voxSegments.erase(elIT);
	}
}

pcl::PointCloud<pcl::PointXYZ> SupportRelationAnalyzer::getVoxelBaseGridForSegment(const Segment& segment)
{
	if (config.voxelSize <= 0.0f) {
		BOOST_LOG_TRIVIAL(error) << "Internal Error: VoxelSize invalid (<= 0.0) @ getVoxelBaseGridForSegment";
		return pcl::PointCloud<pcl::PointXYZ>();
	}
	if (segment.getHullCloud().empty()) {
		BOOST_LOG_TRIVIAL(warning) << "Internal Error: VoxelBaseGrid Generator: Segment Hullcloud is empty.";
		return pcl::PointCloud<pcl::PointXYZ>();
	}
	_PointT minPt {};
	_PointT maxPt {};
	pcl::getMinMax3D(segment.getHullCloud(), minPt, maxPt);
	//Ensure enough voxels for edge cases
	minPt.x -= config.voxelSize;
	minPt.y -= config.voxelSize;
	minPt.z -= config.voxelSize;
	const auto aBBspan = _PointT(maxPt.x - minPt.x, maxPt.y - minPt.y, maxPt.z - minPt.z);
	const auto xVoxelCount = static_cast<std::uint64_t>(std::ceilf(aBBspan.x / config.voxelSize)) + 2;
	const auto yVoxelCount = static_cast<std::uint64_t>(std::ceilf(aBBspan.y / config.voxelSize)) + 2;
	const auto zVoxelCount = static_cast<std::uint64_t>(std::ceilf(aBBspan.z / config.voxelSize)) + 2;
	if (xVoxelCount * yVoxelCount * zVoxelCount > 10000000) {
		BOOST_LOG_TRIVIAL(warning) << "ATTEMPTING VERY LARGE VOXEL BASE GRID RESERVATION FOR SEGMENT ID " << segment.getId() << "!";
	}
	//The rough Voxel Grid is generated for every _isec separately, but we want to compare voxels for equality.
	//This offsetAlign aligns the Voxel Grids to the global voxel "pattern".
	const auto offsetAlign = getOffset(minPt);
	
	auto voxelBaseGrid = fillVoxelGrid(xVoxelCount, yVoxelCount, zVoxelCount, offsetAlign);
	
	BOOST_LOG_TRIVIAL(trace) << "getVoxelBaseGridForSegment End";
	return voxelBaseGrid;
}


pcl::PointCloud<pcl::PointXYZ> SupportRelationAnalyzer::getFloorVoxels(const std::map<int, VoxelSegment>& voxelSegments)
{
	if (config.voxelSize <= 0.0f) {
		BOOST_LOG_TRIVIAL(error) << "Internal Error: getFloorVoxels: VoxelSize invalid (<= 0.0) @ getFloorVoxels";
		return PointCloudXYZ();
	}
	if (voxelSegments.empty()) {
		BOOST_LOG_TRIVIAL(warning) << "Internal Error: getFloorVoxels: VoxelSegment Map is empty";
		return PointCloudXYZ();
	}
	PointCloudXYZ allClouds {};
	for (const auto& _isec : voxelSegments) {
		allClouds += _isec.second.getVoxels();
	}
	
	if (allClouds.empty()) {
		BOOST_LOG_TRIVIAL(warning) << "Internal Error: getFloorVoxels: VoxelSegments have no points -> Floor is empty";
		return PointCloudXYZ();
	}

	_PointT minAll{}, maxAll{};
	pcl::getMinMax3D(allClouds, minAll, maxAll);
	//Shift one voxel to origin for x and z
	minAll.x -= config.voxelSize;
	minAll.z -= config.voxelSize;
	//Shift 3 voxels down for y
	minAll.y -= config.voxelSize * 3.0f;
	const auto xVoxelCount = static_cast<std::uint64_t>(std::ceilf((maxAll.x - minAll.x) / config.voxelSize)) + 2;
	const auto zVoxelCount = static_cast<std::uint64_t>(std::ceilf((maxAll.z - minAll.z) / config.voxelSize)) + 2;
	const auto offset = getOffset(minAll);
	//the shift of 3 voxels down (~4 lines before) + the "5" here means that the floor is 5 voxels high,
	// and has 1 voxel layer which is one voxelsize higher than the "actual" floor, 
	// the other 4 layers are below/at the ground height.
	auto voxelFloor = fillVoxelGrid(xVoxelCount, 5, zVoxelCount, offset); 
	return voxelFloor;
}


pcl::PointCloud<pcl::PointXYZ> SupportRelationAnalyzer::getInlyingVoxels(const Segment& segment, const pcl::PointCloud<pcl::PointXYZ>& voxelBaseGrid)
{
	using std::fabsf;
	if (segment.getHullCloud().empty() || voxelBaseGrid.empty()) {
		//std::cerr << "SupportRelationController::getInlyingVoxels: Segment HullCloud or Voxel Base Grid EMPTY! Returning no inlying voxels.\n";
		BOOST_LOG_TRIVIAL(warning) << "getInlyingVoxels: Segment HullCloud or Voxel Base Grid EMPTY! Returning no inlying voxels.";
		return PointCloudXYZ();
	}
	
	auto deepCopyHullCloudPtr = std::make_shared<PointCloudXYZ>(segment.getHullCloud());
	if (config.useGenerousHulls) {
		//Enlargen the Hullcloud such that ~1 voxel more fits in in both directions (this impl. is a very rough approximation)
		_PointT minPt{}, maxPt{};
		pcl::getMinMax3D(*deepCopyHullCloudPtr, minPt, maxPt);
		const Eigen::Vector3f halfABBvec = 0.5f * (maxPt.getVector3fMap() - minPt.getVector3fMap());
		const Eigen::Vector3f abbMiddle = minPt.getVector3fMap() + halfABBvec;
		const float vxAddScale = config.voxelSize * 0.5f;
		const Eigen::Vector3f halfABBvecMOD = Eigen::Vector3f(fabsf(halfABBvec.x()) + vxAddScale, fabsf(halfABBvec.y()) + vxAddScale, fabsf(halfABBvec.z()) + vxAddScale);
		const float floatDiffFctr = halfABBvecMOD.norm() / halfABBvec.norm();

		Eigen::Affine3f transformT = Eigen::Affine3f::Identity();
		transformT.translation() << -abbMiddle;
		pcl::transformPointCloud(*deepCopyHullCloudPtr, *deepCopyHullCloudPtr, transformT);

		transformT = Eigen::Affine3f::Identity();
		transformT.scale(floatDiffFctr);
		pcl::transformPointCloud(*deepCopyHullCloudPtr, *deepCopyHullCloudPtr, transformT);

		transformT = Eigen::Affine3f::Identity();
		transformT.translation() << abbMiddle;
		pcl::transformPointCloud(*deepCopyHullCloudPtr, *deepCopyHullCloudPtr, transformT);
	}

	// Get a OBB for the hull and remove Voxels outside that OBB
	// This improves performance of finding the inliers for the hull later in this method,
	// as the number of voxels to check with is reduced.
	
	//use MomentOfInertiaEstimation to get the OBB (not necessarily the Min OBB)
	pcl::MomentOfInertiaEstimation<pcl::PointXYZ> feature_extractor;
	feature_extractor.setInputCloud(deepCopyHullCloudPtr);
	feature_extractor.compute();
	pcl::PointXYZ min_point_OBB{}, max_point_OBB{}, position_OBB{};
	Eigen::Matrix3f rotationMatrix = Eigen::Matrix3f::Identity();
	//Extract the OBB
	feature_extractor.getOBB(min_point_OBB, max_point_OBB, position_OBB, rotationMatrix);
	
	//Create the CropBox
	pcl::CropBox<_PointT> cropBoxOBB{};
	cropBoxOBB.setMin(min_point_OBB.getVector4fMap());
	cropBoxOBB.setMax(max_point_OBB.getVector4fMap());

	//The Rotation Matrix and position from the OBB is combined, and its inverse is applied to the 
	// input pointcloud (voxelBaseGrid). This is easier than using the setRotation and setTranslation
	// methods of CropBox.
	
	//Need to convert the Matrix3f into a Matrix4f to use Affine3f.
	Eigen::MatrixXf mat;
	mat.resize(3, 3);
	mat << rotationMatrix;
	mat.conservativeResize(4, 4);
	mat.col(3).setZero();
	mat.row(3).setZero();
	mat(3, 3) = 1;
	Eigen::Affine3f tf3 = Eigen::Affine3f::Identity();
	tf3.matrix() = mat;
	tf3.translation() << position_OBB.getVector3fMap();
	//As mentioned before, set the inverse OBB transform as the transform to use internally
	//for the cropping. The output will be at its "correct" position nonetheless.
	cropBoxOBB.setTransform(tf3.inverse());
	cropBoxOBB.setInputCloud(std::make_shared<PointCloudXYZ>(voxelBaseGrid));
	//Perform the filtering. Reserving space for the filter cloud improves performance on Win64bit
	auto vxBoxed = std::make_shared<PointCloudXYZ>();
	vxBoxed->reserve(voxelBaseGrid.size());
	cropBoxOBB.filter(*vxBoxed);
	vxBoxed->resize(vxBoxed->size());

	if (!vxBoxed->empty()) {
		//Remove any NaN artifacts
		std::vector<int> e;
		pcl::removeNaNFromPointCloud(*vxBoxed, *vxBoxed, e);
	} else {
		//CropBox went wrong one way or the other; vxBoxed shouldn't be empty here.
		// Reset vxBoxed to the orig. input.
		vxBoxed = std::make_shared<PointCloudXYZ>(voxelBaseGrid);
	}
	
	//Could also apply Axis-oriented Bounding Box Cropping here, as it might remove voxels that the OBB missed.
	Eigen::Vector4f minPt, maxPt;
	pcl::getMinMax3D(*deepCopyHullCloudPtr, minPt, maxPt);
	pcl::CropBox<_PointT> cropBoxABB {};
	cropBoxABB.setMin(minPt);
	cropBoxABB.setMax(maxPt);
	cropBoxABB.setInputCloud(vxBoxed);
	cropBoxOBB.filter(*vxBoxed);
	vxBoxed->resize(vxBoxed->size());
	//Same check as before.
	if (!vxBoxed->empty()) {
		std::vector<int> e{};
		pcl::removeNaNFromPointCloud(*vxBoxed, *vxBoxed, e);//Remove any NaN artifacts
	} else {
		//CropBox went wrong one way or the other; vxBoxed shouldn't be empty here. Reset vxBoxed to the orig. input.
		vxBoxed = std::make_shared<PointCloudXYZ>(voxelBaseGrid);
	}
	// Now start the Crop Hull Inlier determination.

	pcl::CropHull<_PointT> cropHullT{};
	cropHullT.setInputCloud(vxBoxed);
	cropHullT.setCropOutside(true); //Want to remove the voxels outside the segment, not inside.
	cropHullT.setDim(3);
	cropHullT.setHullCloud(deepCopyHullCloudPtr);
	cropHullT.setHullIndices(segment.getPolygonsCopy());
	
	PointCloudXYZ outPoints{};
	outPoints.reserve(vxBoxed->size());
	cropHullT.filter(outPoints);
	outPoints.resize(outPoints.points.size());
	return outPoints;
}

//For a given starting position (offsetAlign), creates a point cloud with the voxels
// specified by the combination of the starting position with the three count parameters.
// The voxelSize needs to be set before calling this method.
pcl::PointCloud<pcl::PointXYZ> SupportRelationAnalyzer::fillVoxelGrid(std::uint64_t xVoxCount, std::uint64_t yVoxCount, std::uint64_t zVoxCount, const pcl::PointXYZ& offsetAlign)
{
	if (config.voxelSize <= 0.0f) {
		BOOST_LOG_TRIVIAL(error) << "Internal Error: VoxelSize invalid (<= 0.0) @ fillVoxelGrid";
		return pcl::PointCloud<pcl::PointXYZ>();
	}
	//cout << "Reserving: #" << xVoxCount * yVoxCount * zVoxCount << "\n";
	pcl::PointCloud<pcl::PointXYZ> voxelFillCloud {};
	voxelFillCloud.reserve(xVoxCount * yVoxCount * zVoxCount);
	for (std::uint64_t i = 1; i <= xVoxCount; ++i)
	{
		for (std::uint64_t k = 1; k <= yVoxCount; ++k)
		{
			for (std::uint64_t n = 1; n <= zVoxCount; ++n)
			{
				voxelFillCloud.push_back(_PointT(i * config.voxelSize + offsetAlign.x, k * config.voxelSize + offsetAlign.y, n * config.voxelSize + offsetAlign.z));
			}
		}
	}
	voxelFillCloud.height = 1;
	voxelFillCloud.width = voxelFillCloud.size();
	return voxelFillCloud;
}

// Returns the offset needed to align a segment voxel grid to the global voxel grid, based on the minPt from the
// Segment Voxel Grid.
pcl::PointXYZ SupportRelationAnalyzer::getOffset(const pcl::PointXYZ& minPt)
{
	if (config.voxelSize <= 0.0f) {
		BOOST_LOG_TRIVIAL(error) << "Internal Error: VoxelSize invalid (<= 0.0) @ getOffset";
		return _PointT();
	}
	_PointT offset = _PointT();
	offset.x = std::floorf(minPt.x / config.voxelSize) * config.voxelSize - (config.voxelSize * 0.25f);
	offset.y = std::floorf(minPt.y / config.voxelSize) * config.voxelSize - (config.voxelSize * 0.25f);
	offset.z = std::floorf(minPt.z / config.voxelSize) * config.voxelSize - (config.voxelSize * 0.25f);
	return offset;
}
