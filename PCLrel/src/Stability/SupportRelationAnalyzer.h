/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#pragma once
#ifndef SupportRelationAnalyzer_H
#define SupportRelationAnalyzer_H
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)

#include <memory>
#include <vector>
#include <map>
#include <string>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include "VoxelSegment.h"

class Segment;

//Class to perform support relation testing.
// Make sure that the Scene only lies in positive coordinates! (Positive x,y,z for all points)

/// <summary>
/// <para>Class to be used for analyzing the support relations/physical relations of a 'scene', consisting of multiple Segments.
/// To be used with registered and combined Segments from multiple views. More coverage of the objects = better chance of accurate analysis. </para>
/// <para>IMPORTANT: The input dataset should only contain points with positive X,Y and Z coordinates. Recommended: Make sure this is the
/// case BEFORE converting the pointclouds to segments for input here.</para>
/// <para>VoxelSize is set to 0.05(cm) by default, heavily dependent on the size of the objects in the scene.
/// If the objects are roughly similar in size, set voxelsize such that the smallest sidelength of the smallest object
/// is 5-20 times larger than one voxel. If the object sizes are very homogenous, this might lead to very small voxels 
/// relative to most objects, resulting in reduced performance/physical relation detection.</para>
/// DEVNOTES: Want to get rid of always having shared_ptr on Segments instead of direct resources, so that might change (2021-02-09).
/// </summary>
class SupportRelationAnalyzer
{
	//TODO make better setting control setup
	struct SupportRelationAnalyzerConfig {
		//if set to true, the hulls of the segments will be made slightly bigger before determining the voxels, such that 
		// the voxels generated for each segment are close to the actual dimensions
		bool useGenerousHulls;
		float voxelSize;
	};

public:
	SupportRelationAnalyzer();
	SupportRelationAnalyzer(const std::vector<Segment>& sceneSegments, bool useGenerousHulls, float voxelSize);
	SupportRelationAnalyzer(std::vector<Segment>&& sceneSegments, bool useGenerousHulls, float voxelSize);

	//Setters
	void setInputSceneSegments(const std::vector<Segment>& sceneSegments);
	void setInputSceneSegments(std::vector<Segment>&& sceneSegments);

	//Stability/Support analysis methods
	int evaluateSupportRelations();
	std::vector<int> findCriticalSegments();

	//Config
	SupportRelationAnalyzerConfig config;

	//Getters
	bool inputIsEmpty() const;
	std::vector<pcl::PointCloud<pcl::PointXYZ>> getVoxelRepresentationAfterEvaluate() const;
	std::map<int,pcl::PointCloud<pcl::PointXYZ>> getVoxelIntersectionRepresentationAfterEvaluate() const;
	pcl::PointCloud<pcl::PointXYZ> getVoxelRepresentationForSegmentID(int id) const;

	//Helpers
	void resetScene();

	//STATIC Helper to get a somewhat reasonable voxelsize guess
	static float guessVoxelSize(const std::vector<Segment>& segments);
	static void prepareSegments(std::vector<Segment>& segments);

private:

	//Input segments
	std::vector<Segment> segments {};

	//implicit sequence usage, be careful when modyfying this.
	std::map<int, VoxelSegment> voxelSegmentMap {};

	//Experimental for logging and understanding order
	std::pair<int, int> lastIntegration = { -1,-1 };
	std::vector<int> integrationTrace {};

	//VoxelSegment Map Creation and Modification
	bool setVoxelSegmentMapFromScene(bool reset);
	void removeVoxelSegmentMentionsFromMap(std::map<int, VoxelSegment>& voxSegments, int id);

	//Check for Stability
	bool checkFloatViolation(const std::map<int, VoxelSegment>& voxSegments);
	bool checkStability(const std::map<int, VoxelSegment>& voxSegments, int indentLevel = 0);

	//Recursive Methods for traversing the physical relations between VoxelSegments
	pcl::PointCloud<pcl::PointXYZ> integrateUpwards(const std::map<int, VoxelSegment>& voxSegments, int segSupportID, int segToIntegrateID, int floorID, int indntLvl = 0);
	pcl::PointCloud<pcl::PointXYZ> integrateCase1(const std::map<int, VoxelSegment>& voxSegments, int segSupportID, int segToIntegrateID, int floorID, int indntLvl = 0);
	pcl::PointCloud<pcl::PointXYZ> integrateCase2or3(const std::map<int, VoxelSegment>& voxSegments, int segSupportID, int segToIntegrateID, int floorID, int indntLvl = 0);
	
	//Output Helper
	void integrateOutputErrorOut(int caseNum, int segSupportID, int segToIntgrtID, int indtLvl, const std::string& message);

	//Voxel Creation
	pcl::PointCloud<pcl::PointXYZ> getVoxelBaseGridForSegment(const Segment& segment);
	pcl::PointCloud<pcl::PointXYZ> getFloorVoxels(const std::map<int, VoxelSegment>& voxelSegments);
	pcl::PointCloud<pcl::PointXYZ> getInlyingVoxels(const Segment& segment, const pcl::PointCloud<pcl::PointXYZ>& voxelBaseGrid);
	pcl::PointCloud<pcl::PointXYZ> fillVoxelGrid(std::uint64_t xVoxCount, std::uint64_t yVoxCount, std::uint64_t zVoxCount, const pcl::PointXYZ& offsetAlign);

	//retrieve the Offset used for a point
	pcl::PointXYZ getOffset(const pcl::PointXYZ& minPt);
};
#endif /* SupportRelationAnalyzer_H */
