/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#pragma once
#ifndef VoxelSegment_HPP
#define VoxelSegment_HPP

#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/Vertices.h>
#include <map>
//#include "VoxelSegPhysicRelation.h"

class VoxelSegment
{
public:
    //Standard
    VoxelSegment() noexcept = default;
    VoxelSegment(int id, const pcl::PointCloud<pcl::PointXYZ>& voxels, bool isFloor = false) noexcept;
    //Copy
    VoxelSegment(const VoxelSegment& vSegment);
    VoxelSegment& operator=(VoxelSegment const&) = default;
    //Move
    VoxelSegment(VoxelSegment&&) noexcept = default;
    VoxelSegment& operator=(VoxelSegment&&) noexcept = default;




    const pcl::PointCloud<pcl::PointXYZ>& getVoxels() const;
    const std::map<int, std::pair<std::shared_ptr<pcl::PointCloud<pcl::PointXYZ>>, bool>>& getIntersections() const;

    int getId() const;
    bool getIsFloor() const;
    bool hasIntersectionWith(int id) const;
    bool hasSupportiveIntersectionWith(int id) const;
    bool hasUnsupportiveIntersections() const;
    unsigned int getNumberOfSupportingIntersections() const;
    
    //Supportive: 'Can support Weight'
    bool addIntersection(int id, const pcl::PointCloud<pcl::PointXYZ>& intersectedVoxels, bool supportive);
    
    void removeIntersection(int id);
    void setIsFloor(bool isFloor);
    void addVoxels(const pcl::PointCloud<pcl::PointXYZ>& vxsToAdd);

    bool checkIntersectionStabilityAllSupports(const float voxelSize = 0.0f) const;
    
    pcl::PointCloud<pcl::PointXYZ> getVoxelsSupportedByVoxIDContact(int id) const;
    pcl::PointCloud<pcl::PointXYZ> getVoxelsSupportedByVoxIDContactRelaxed(int id, const float voxelSize) const;
    pcl::PointCloud<pcl::PointXYZ> getVoxelsSupportedByVoxIDContactRelaxedWithWeight(int id, const float voxelSize) const;

    static bool INTERSECTION_CAN_SUPPORT(const VoxelSegment& voxSeg1, const pcl::PointCloud<pcl::PointXYZ>& intersectedVoxels, float voxelSize);
    static bool INTERSECTION_IS_SUPPORTED(const VoxelSegment& voxSeg1, const pcl::PointCloud<pcl::PointXYZ>& intersectedVoxels, float voxelSize);
    static void COMPUTEANDSETINTERSECTION(VoxelSegment& voxSeg1, VoxelSegment& voxSeg2, float voxelSize);

    void printOutRels() const;
    //VoxelSegPhysicRelation physicDependencies;
    //void addSupportedByEntry(int id);

private:
    
    pcl::PointCloud<pcl::PointXYZ> getSupportingIntersectionsCombinedXZ(bool subdivide, float voxelSize = 0.f) const;
    
    int id{ -1 };
    bool isFloor{ false };
    pcl::PointCloud<pcl::PointXYZ> voxels{};

    //bool: 'Can support Weight' -> true, else false
    std::map<int, std::pair<std::shared_ptr<pcl::PointCloud<pcl::PointXYZ>>, bool>> intersections{}; 
};

#endif /* VoxelSegment_HPP */