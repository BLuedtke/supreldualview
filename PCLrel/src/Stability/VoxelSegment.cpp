/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#include "VoxelSegment.h"

#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)
#include "../Constants/Constants.h"
#include "../Spatial/SpatialAnalyzer.h"


#include <pcl/common/centroid.h>
#include <pcl/common/common.h>
#include <pcl/surface/convex_hull.h>
//TESTING
#include <chrono>
#include <iomanip>


using PointT = pcl::PointXYZ;
using PointCloudXYZ = pcl::PointCloud<pcl::PointXYZ>;
using std::cout;
using std::endl;

VoxelSegment::VoxelSegment(int _id, const pcl::PointCloud<pcl::PointXYZ>& _voxels, bool _isFloor) noexcept : id{ _id }, isFloor{_isFloor}, voxels{_voxels}
{
	if (voxels.empty()) {
		std::cout << "VoxelSegment Constructor WARNING: INPUT VOXELS ARE EMPTY!\n";
	}
}

//Copy constructor
VoxelSegment::VoxelSegment(const VoxelSegment& vSegment) 
{
	this->id = vSegment.getId();
	this->voxels = vSegment.getVoxels();
	this->intersections = vSegment.getIntersections();
	this->isFloor = vSegment.getIsFloor();
}


/// <summary>
/// Returns true if an Intersection with the VoxelSegment-ID is currently registered
/// </summary>
/// <param name="id">ID of the VoxelSegment for which to check if an intersection is registered</param>
/// <returns></returns>
bool VoxelSegment::hasIntersectionWith(int id) const
{
	if (intersections.count(id) != 0) {
		if (intersections.at(id).first->empty()) {
			std::cout << "hasIntersectionWith WARNING: ID is in Map, but the size of the intersection is 0!\n";
			return false;
		}
		else {
			return true;
		}
	}
	else {
		return false;
	}
}
/// <summary>
/// See VoxelSegment::hasIntersectionWith, but only returns true if that intersection can support weight from this segment.
/// </summary>
/// <param name="id">ID of the VoxelSegment for which to check if an intersection is registered and can take load</param>
/// <returns>True if load-supporting intersection exists, false otherwise</returns>
bool VoxelSegment::hasSupportiveIntersectionWith(int id) const
{
	auto search = intersections.find(id);
	if (search != intersections.end()) {
		return std::get<1>(*search).second;
	}
	return false;
}

/// <summary>
/// Returns true if there is at least one intersection registered for the voxelsegment which is classified as not supporting this segment.
/// </summary>
/// <returns></returns>
bool VoxelSegment::hasUnsupportiveIntersections() const
{
	for (const auto& iSec : intersections) {
		if (iSec.second.second == false) {
			return true;
		}
	}
	return false;
}

/// <summary>
/// Returns the number of intersections registered for the voxelsegment which are classified as supportive.
/// </summary>
/// <returns></returns>
unsigned int VoxelSegment::getNumberOfSupportingIntersections() const
{
	unsigned int counter = 0;
	for (const auto& isec : intersections) {
		if (isec.second.second) {
			counter++;
		}
	}
	return counter;
}

/// <summary>
/// Adds an Intersection for the VoxelSegment.
/// </summary>
/// <param name="id">The ID of the other VoxelSegment which is being intersected</param>
/// <param name="intersectedVoxels">The intersecting voxels.</param>
/// <param name="supportive">Can the calling voxelSegment rest weight over this intersection? Yes (true) or No (false).</param>
/// <returns>True, if the intersection was registered successfully. False, if an intersection with a VoxelSegment with this ID already exists.</returns>
bool VoxelSegment::addIntersection(int id, const pcl::PointCloud<pcl::PointXYZ>& intersectedVoxels, bool supportive)
{
	if (intersections.count(id) != 0) {
		//Found existing intersection entry for this id
		return false;
	}
	else {
		//cout << "VoxelSegment id " << this->id << " intersection with ID " << id << " was added." << endl;
		intersections.insert({ id, {std::make_shared<PointCloudXYZ>(intersectedVoxels),supportive} });
		return true;
	}
}

void VoxelSegment::removeIntersection(int id)
{
	auto it = intersections.find(id);
	if (it != intersections.end()) {
		intersections.erase(it);
	}
	if (intersections.count(id) != 0) {
		cout << "WARNING: Called removeIntersection, but there is an intersection with this id remaining after removal!\n";
	}
}


/// <summary>
/// Computes the Center of Gravity and checks whether the VoxelSegment is stable based on the supporting intersections saved for/in this VoxelSegment.
/// Assumes that the density of the VoxelSegment is the same at every point.
/// It is heavily recommended to set the correct/used voxelSize, this also helps to prevent cases
/// of coplanarity where QHull would fail otherwise.
/// </summary>
/// <param name="voxelSize">Size that each voxel represents. Set this to improve check accuracy.</param>
/// <returns>True, if this VoxelSegment can rest upon its supporting intersections. False, if it would fall over.</returns>
bool VoxelSegment::checkIntersectionStabilityAllSupports(const float voxelSize) const
{
	//cout << "checkIntersectionStabilityAllSupports" << endl;
	if (voxels.empty()) {
		cout << "Can't perform checkIntersectionStabilityAllSupports with no voxels being set!\n";
		return false;
	}
	if (intersections.empty()) {
		cout << "Can't perform checkIntersectionStabilityAllSupports with no intersections being set!\n";
		return false;
	}

	//1. Build XZ-projected area of support

	//PointCloudXYZ allSupInsecPts;
	auto allSupInsec = std::make_shared<PointCloudXYZ>();
	if (voxelSize > 0.f) { //If possible, subdivides the Intersection zones to better represent the 'size' of one voxel.
		*allSupInsec = getSupportingIntersectionsCombinedXZ(true, voxelSize);
	}
	else {
		*allSupInsec = getSupportingIntersectionsCombinedXZ(false);
	}
	
	if (allSupInsec->empty()) {
		cout << "No Supporting Intersections found. Segment w/ ID " << id << " classified as unstable!" << endl;
		return false;
	}
	
	//Check if they are Co-planar and shift the first point slightly to avoid that.
	Eigen::Vector4f minPt, maxPt;
	pcl::getMinMax3D(*allSupInsec, minPt, maxPt);
	if (minPt.x() == maxPt.x()) {
		allSupInsec->points[0].x += 0.01; // Naive prevention of coplanarity
	}
	if (minPt.z() == maxPt.z()) {
		allSupInsec->points[0].z += 0.01;
	}

	pcl::ConvexHull<pcl::PointXYZ> convexHull{};
	std::vector<pcl::Vertices> polygons{};
	PointCloudXYZ areaOfSupport{};
	
	convexHull.setDimension(2);
	convexHull.setInputCloud(allSupInsec);
	convexHull.reconstruct(areaOfSupport, polygons);
	
	//2. Determine CoG and project on XZ plane
	//	CoG = Center of Gravity, assuming a uniform weight distribution within the object.
	PointT centr{};	
	pcl::computeCentroid(this->voxels, centr);
	//Projected on XZ like the Convex Hull input / hullcloud
	centr.y = 0.f;					
	
	//3. Determine whether CoG is whithin the area of support or not
	if (polygons.size() > 0) {
		// If the CoG lies within the polygon spanned by the supports (both projected onto the XZ plane), then it is stable.
		return (SpatialAnalyzer::isPointInPolygon(centr, polygons[0], areaOfSupport));
	}
	else {
		// This is a fallback necessary because the convexHull reconstruction from before can fail in some cases (coplanarity).
		// It is a very crude method though (check if the distance from support center to CoG is lower than the distance from
		// support center to the edge of the ABB for the combined support intersections).
		// There's probably a better way to do this... TODO for future projects.
		//cout << "checkIntersectionStabilityAllSupports EMERGENCY ALTERNATIVE \n";
		PointT centrSup{};
		pcl::computeCentroid(*allSupInsec, centrSup);
		PointT min{}, max{};
		pcl::getMinMax3D(*allSupInsec, min, max);
		return (((centrSup.getVector3fMap() - centr.getVector3fMap()).norm() < (centrSup.getVector3fMap() - max.getVector3fMap()).norm()) ? true : false);
	}
}


/// <summary>
/// <para>For a given ID of a different VoxelSegment, determines which voxels of the calling VoxelSegment rest upon the 
/// supporting intersection of the other VoxelSegment, given that such a supporting intersection exists. 
/// This process is based on simplified static analysis.</para>
/// <para>Note: Even if there's only one supporting contect (by vID), the returned cloud is likely smaller than 
/// this segment itself. This is because the points in the intersection between this and the vID VoxelSegment are not included.</para>
/// </summary>
/// <param name="vID">ID of the VoxelSegment whose supporting intersection is to be looked at.</param>
/// <returns>Returns all Voxels that rest their weight upon the VoxelSegment with ID vID. Does not include voxels present in the intersection itself.</returns>
pcl::PointCloud<pcl::PointXYZ> VoxelSegment::getVoxelsSupportedByVoxIDContact(int vID) const
{
	if (!hasSupportiveIntersectionWith(vID)) {
		cout << "getVoxelsSupportedByVoxIDContact: VoxelSegment with ID " << this->id << " does not rely on VoxelSegment with ID " << vID << "! Returning EMPTY" << endl;
		return PointCloudXYZ();
	} else if (getNumberOfSupportingIntersections() == 1) {
		if (intersections.at(vID).first->empty()) {
			cout << "getVoxelsSupportedByVoxIDContact Case 1: Has intersection, but its size is 0! Returning empty.\n";
			return PointCloudXYZ();
		}
		else {
			return (SpatialAnalyzer::getANotExactlyInB(this->voxels, *intersections.at(vID).first));
		}
	}
	else {
		if (intersections.at(vID).first->empty()) {
			cout << "getVoxelsSupportedByVoxIDContact Case 2: Has intersection, but its size is 0! Returning empty.\n";
			return PointCloudXYZ();
		}
		//For every Voxel, the (euclidean) nearest intersection is classified as the one which takes the weight of that voxel.
		PointCloudXYZ ptsSupported{};
		const PointCloudXYZ voxelsCut{ SpatialAnalyzer::getANotExactlyInB(this->voxels, *intersections.at(vID).first) };
		for (const auto& pt : voxelsCut) {
			int closest = -1;
			float minDist = 99999999.f;
			//Theoretically possible to migrate this to SpatialAnalyzer to make use of kdTreeFLANN
			for (const auto& isec : intersections) {
				if (isec.second.second) {
					const auto dist = SpatialAnalyzer::getMinimumDistToCloud(isec.second.first, pt);
					if (dist < minDist && dist > -1.f && !isnan(dist)) { //>-1.f check because that's what getMinimumDistToCloud returns if there's a failure.
						minDist = dist;
						closest = isec.first;
					}
				}
			}
			if (closest == vID) {
				ptsSupported.push_back(pt);
			}
		}
		ptsSupported.resize(ptsSupported.size());
		return ptsSupported;
	}
}

//Like the other variant, but only voxels *above* the intersection are said to be supported
//Idea note: possibly 3rd variant, where fake weight voxels are added
pcl::PointCloud<pcl::PointXYZ> VoxelSegment::getVoxelsSupportedByVoxIDContactRelaxed(int vID, const float voxelSize) const
{
	if (!hasSupportiveIntersectionWith(vID)) {
		cout << "VoxelSegment with ID " << this->id << " does not rely on VoxelSegment with ID " << vID << "! Returning EMPTY" << endl;
		return PointCloudXYZ();
	}
	else if (getNumberOfSupportingIntersections() == 1) {
		//cout << "GetSupportVoxIDContact Relaxed Case 1\n";
		return (SpatialAnalyzer::getANotExactlyInB(this->voxels, *intersections.at(vID).first));
	}
	else {
		//cout << "GetSupportVoxIDContact Relaxed Case 2\n";
		const auto& targetIsc = intersections.at(vID).first;
		PointCloudXYZ voxelsCut{ SpatialAnalyzer::getANotExactlyInB(this->voxels, *intersections.at(vID).first) };
		PointCloudXYZ pointsSTest{ SpatialAnalyzer::getAPointsNearBonXZ(std::move(voxelsCut), *targetIsc, voxelSize * 0.9f) };
		//cout << "PointsSTest Size " << pointsSTest.size() << "\n";
		return pointsSTest;
	}
}

//Like getVoxelsSupportedByVoxIDContactRelaxed, but adds offset voxels simulating the weight that is actually carried
//Never use avgNNDist or similar methods on the returned cloud.
pcl::PointCloud<pcl::PointXYZ> VoxelSegment::getVoxelsSupportedByVoxIDContactRelaxedWithWeight(int vID, const float voxelSize) const
{
	if (!hasSupportiveIntersectionWith(vID)) {
		cout << "VoxelSegment with ID " << this->id << " does not rely on VoxelSegment with ID " << vID << "! Returning EMPTY" << endl;
		return PointCloudXYZ();
	}
	else if (getNumberOfSupportingIntersections() == 1) {
		if (intersections.at(vID).first->empty()) {
			cout << "getVoxelsSupportedByVoxIDContactRelaxedWithWeight Case 1: Has intersection, but its size is 0! Returning empty.\n";
			return PointCloudXYZ();
		}
		else {
			return (SpatialAnalyzer::getANotExactlyInB(this->voxels, *intersections.at(vID).first));
		}
	}
	else {
		//Get the points above
		const auto& targetIsc = intersections.at(vID).first;
		if (targetIsc->empty()) {
			cout << "getVoxelsSupportedByVoxIDContactRelaxedWithWeight Case 2: Has intersection, but its size is 0! Returning empty.\n";
			return PointCloudXYZ();
		}
		const PointCloudXYZ voxelsCut{ SpatialAnalyzer::getANotExactlyInB(this->voxels, *targetIsc) };
		PointCloudXYZ pointsSupAbove{ SpatialAnalyzer::getAPointsNearBonXZ(voxelsCut, *targetIsc, voxelSize * 0.9f) };
		
		//Add Voxels for the weight
		const PointCloudXYZ ptsCheck{ SpatialAnalyzer::getANotExactlyInB(voxelsCut, pointsSupAbove) };
		std::vector<std::shared_ptr<const pcl::PointCloud<pcl::PointXYZ>>> isecVec {};
		isecVec.reserve(intersections.size());
		isecVec.push_back(targetIsc);
		for (const auto& isec : intersections) {
			if (isec.second.second && isec.first != vID) {
				isecVec.push_back(isec.second.first);
			}
		}
		
		int weightVCounter = SpatialAnalyzer::getNumberOfPointsClosestToV0(isecVec, ptsCheck);
		
		Eigen::Vector4f ctr;
		pcl::compute3DCentroid(*targetIsc, ctr); //Weight added at middle of intersection
		const pcl::PointXYZ ptWeightSimulator = pcl::PointXYZ(ctr.x(), ctr.y(), ctr.z());
		pointsSupAbove.reserve(pointsSupAbove.size() + weightVCounter);
		for (int i = 0; i < weightVCounter; ++i) {
			pointsSupAbove.push_back(ptWeightSimulator);
		}
		return pointsSupAbove;
	}
}


/// <summary>
/// <para>Combines all Voxels of known supporting intersections into one Pointcloud, and sets their y component to zero.</para>
/// <para>If subdivide is set to true, you MUST also pass a voxelSize > 0.</para>
/// <para>With subdivide=true and voxelSize >0, for every voxel in an intersection, 4 voxels are
/// added to the combined pointcloud instead of the intersection voxel. These 4 voxels are offset by 
/// a third of the voxelsize in the XZ plane (++,+-,--,-+) to better represent the 'volume' of the voxel.</para>
/// </summary>
/// <param name="subdivide">Whether to subdivide the intersection voxels or not</param>
/// <param name="voxelSize">Used for the subdivision process, has to be set to a value >0 if subdivide is true.</param>
/// <returns>A Pointcloud combining all the supportive intersection voxels for this VoxelSegment. 
/// Empty Pointcloud if no supportive intersections present.</returns>
pcl::PointCloud<pcl::PointXYZ> VoxelSegment::getSupportingIntersectionsCombinedXZ(bool subdivide, float voxelSize) const
{
	PointCloudXYZ allSupInsec{};
	if (subdivide) {
		const float vThrd = (voxelSize > 0.0f) ? (voxelSize / 3.f) : 0.0001f;
		allSupInsec.reserve(allSupInsec.size() + intersections.size() * 4);
		for (const auto& _isec : intersections)
		{
			if (_isec.second.second && _isec.second.first != nullptr && !_isec.second.first->empty()) {
				//-> Intersection is supportive
				//Subdivide into a sub-voxel representation to better respect actual voxel width
				for (const auto& pt : *(_isec.second.first)) {
					allSupInsec.push_back((PointT(pt.x + vThrd, 0.f, pt.z + vThrd)));
					allSupInsec.push_back((PointT(pt.x - vThrd, 0.f, pt.z + vThrd)));
					allSupInsec.push_back((PointT(pt.x + vThrd, 0.f, pt.z - vThrd)));
					allSupInsec.push_back((PointT(pt.x - vThrd, 0.f, pt.z - vThrd)));
				}
			}
		}
		allSupInsec.points.shrink_to_fit();
	}
	else {
		for (const auto& _isec : intersections) {
			//Is supportive?
			if (_isec.second.second && _isec.second.first != nullptr && !_isec.second.first->empty()) {
				allSupInsec += *_isec.second.first;
			}
		}
		//Project the intersection onto the horizontal plane
		for (auto& pt : allSupInsec) {
			pt.y = 0.f;
		}
	}
	allSupInsec.resize(allSupInsec.size());
	return allSupInsec;
}




/// <summary>
/// <para>Naively computes whether a given VoxelSegment (voxSeg1) can rest weight on an intersection.</para>
/// <para>This is done by checking if there are any more voxels in voxSeg1 ABOVE and OVER the intersection which can transfer weight.</para>
/// <para>This is a very naive way of determining this physical relation.</para>
/// </summary>
/// <param name="voxSeg1">VoxelSegment</param>
/// <param name="intersectedVoxels">Voxels defining the intersection</param>
/// <returns>True, if the VoxelSegment can rest/transfer weight via the intersection.</returns>
bool VoxelSegment::INTERSECTION_CAN_SUPPORT(const VoxelSegment& voxSeg1, const pcl::PointCloud<pcl::PointXYZ>& intersectedVoxels, float voxelSize)
{
	if (voxSeg1.getVoxels().empty() || intersectedVoxels.empty()) {
		cout << "INTERSECTION_CAN_SUPPORT WARNING: Voxel Segment or IntersectionVoxels are empty!\n";
		return false;
	}
	//Would not scale well if we had extremely large point clouds, but entirely fine for now
	PointT minPt{};
	PointT maxPt{};
	pcl::getMinMax3D(intersectedVoxels, minPt, maxPt);
	PointCloudXYZ noInsecOnly{ SpatialAnalyzer::getANotExactlyInB(voxSeg1.getVoxels(), intersectedVoxels) };
	for (const auto& iSecPoint: intersectedVoxels)
	{
		//Search for a point in noInsecOnly which is higher (y) and and directly above the intersection point.
		for (size_t n = 0; n < noInsecOnly.size(); ++n)
		{
			if (std::fabsf(noInsecOnly.at(n).x - iSecPoint.x) < Constants::EPSILON &&
						(noInsecOnly.at(n).y > iSecPoint.y + Constants::EPSILON) &&  //Above
						(noInsecOnly.at(n).y <= iSecPoint.y + voxelSize + Constants::EPSILON) &&  //directly above!
				std::fabsf(noInsecOnly.at(n).z - iSecPoint.z) < Constants::EPSILON) { 
				return true;
			} 
		}
	}
	return false;
}
//Hard requirement: The intersectedVoxels parameter needs to be sorted by the points' height field (asc)
bool VoxelSegment::INTERSECTION_IS_SUPPORTED(const VoxelSegment& voxSeg1, const pcl::PointCloud<pcl::PointXYZ>& intersectedVoxels, float voxelSize)
{
	if (voxSeg1.getVoxels().empty() || intersectedVoxels.empty()) {
		cout << "INTERSECTION_IS_SUPPORTED WARNING: Voxel Segment or IntersectionVoxels are empty!\n";
		return false;
	}
	
	PointCloudXYZ noPointBeneathOnly{};
	for (int i = 0; i < intersectedVoxels.size(); ++i) {
		const PointT& pt = intersectedVoxels[i];
		bool pointbelow = false;
		for (int k = 0; k < i; ++k) {
			const PointT& ptBelowC = intersectedVoxels[k];
			if (std::fabsf(ptBelowC.x - pt.x) < Constants::EPSILON &&
						  (ptBelowC.y < pt.y - Constants::EPSILON) &&  //below
				std::fabsf(ptBelowC.z - pt.z) < Constants::EPSILON) {
				pointbelow = true;
				break; //breaks the inner loop
			}
		}
		if (!pointbelow) {
			noPointBeneathOnly.push_back(pt);
		}
	}
	//correct to use intersectedVoxels here
	pcl::PointCloud<pcl::PointXYZ> noInsecOnly{ SpatialAnalyzer::getANotExactlyInB(voxSeg1.getVoxels(), intersectedVoxels) };
	
	for (const auto& iSecPoint : noPointBeneathOnly)
	{
		//Search for a point in noInsecOnly which is lower (y) and directly below the intersection point.
		for (size_t n = 0; n < noInsecOnly.size(); ++n)
		{
			if (std::fabsf(noInsecOnly.at(n).x - iSecPoint.x) < Constants::EPSILON &&
				(noInsecOnly.at(n).y < iSecPoint.y - Constants::EPSILON) &&  //below
				(noInsecOnly.at(n).y >= iSecPoint.y - voxelSize - Constants::EPSILON) &&  //directly below!
				std::fabsf(noInsecOnly.at(n).z - iSecPoint.z) < Constants::EPSILON) {
				//cout << "Supported because: Point " << noInsecOnly.at(n) << " below " << iSecPoint << "\n";
				return true;
			}
		}
	}
	return false;
}

/// <summary>
/// <para>Computes the intersection between two VoxelSegments by checking which Voxels are contained in both VoxelSegments. </para>
/// <para>If an Intersection is found, classifies the intersections as supportive or non-supportive and
/// adds the intersection to both VoxelSegments.</para>
/// <para>Does nothing if both VoxelSegments already have saved data for intersections with each other.</para>
/// <para>Uses the set_intersection from the STL library with a custom comparator</para>
/// </summary>
/// <param name="voxSeg1">VoxelSegment (will be modified!)</param>
/// <param name="voxSeg2">VoxelSegment (will be modified!)</param>
void VoxelSegment::COMPUTEANDSETINTERSECTION(VoxelSegment& voxSeg1, VoxelSegment& voxSeg2, float voxelSize)
{
	//std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
	if (voxSeg1.hasIntersectionWith(voxSeg2.getId()) && voxSeg2.hasIntersectionWith(voxSeg1.getId())) {
		//Do nothing if intersection is already set
		return;
	}
	if (voxSeg1.getVoxels().empty() || voxSeg2.getVoxels().empty()) {
		cout << "COMPUTEANDSETINTERSECTION WARNING: At least one of the VoxelSegments has no voxels!\n";
	}
	using std::fabsf;
	//Custom-Comparator with Epsilon and x/y/z ordering
	struct {
		bool operator()(PointT a, PointT b) const {
			if (fabsf(a.x - b.x) < Constants::EPSILON) {
				if (fabsf(a.y - b.y) < Constants::EPSILON) {
					if (fabsf(a.z - b.z) < Constants::EPSILON) {
						return false;
					} else {
						return a.z < b.z;
					}
				} else {
					return a.y < b.y;
				}
			} else {
				return a.x < b.x;
			}
		}
	} customLessOP;
	//Deep copy because set_intersect wants an ordered set (and original shouldn't be modified)
	PointCloudXYZ vAcopy{ voxSeg1.getVoxels() };
	PointCloudXYZ vBcopy{ voxSeg2.getVoxels() };
	std::sort(vAcopy.points.begin(), vAcopy.points.end(), customLessOP);
	std::sort(vBcopy.points.begin(), vBcopy.points.end(), customLessOP);
	PointCloudXYZ voxIntersecting{};
	std::set_intersection(vAcopy.points.begin(), vAcopy.points.end(), vBcopy.points.begin(), vBcopy.points.end(), std::back_inserter(voxIntersecting), customLessOP);
	voxIntersecting.resize(voxIntersecting.size());
	//Sort the intersected Voxels based on height
	struct {
		bool operator()(const pcl::PointXYZ& a, const pcl::PointXYZ& b) const {
			return(a.y < b.y);
		}
	} customLessPtHeight;
	std::sort(voxIntersecting.begin(), voxIntersecting.end(), customLessPtHeight);
	if (!voxIntersecting.empty()) {
		//cout << "Testing Intersections for " << voxSeg1.getId() << " and " << voxSeg2.getId() << "\n";
		//cout << "VoxIntersecting Size: " << voxIntersecting.size() << "\n";
		
		bool supportFromVX1 = VoxelSegment::INTERSECTION_IS_SUPPORTED(voxSeg1, voxIntersecting, voxelSize);
		bool supportFromVX2 = VoxelSegment::INTERSECTION_IS_SUPPORTED(voxSeg2, voxIntersecting, voxelSize);
		
		if (supportFromVX1 && supportFromVX2) {
			cout << "Primary Method: Both Vxs Support this itsec (" << voxSeg1.getId() << "," << voxSeg2.getId() << "). Fallback to Old Version.\n";
			//Intersection supported by both? Weird. Use the older method instead.
			//IMPORTANT: THE ORDER OF THE BOOLS FOR THE addIntersection LATER IS SWITCHED WHEN USING THE NEWER METHOD!
			supportFromVX1 = VoxelSegment::INTERSECTION_CAN_SUPPORT(voxSeg1, voxIntersecting, voxelSize);
			supportFromVX2 = VoxelSegment::INTERSECTION_CAN_SUPPORT(voxSeg2, voxIntersecting, voxelSize);
			if (!supportFromVX1 && !supportFromVX2) {
				cout << "WARNING: Intersection added which can't be supported by either object.\n";
			} else if (supportFromVX1 && supportFromVX2) {
				cout << "   -> Older method also says that both can transfer weight.\n";
			}
			voxSeg1.addIntersection(voxSeg2.getId(), voxIntersecting, supportFromVX1);
			voxSeg2.addIntersection(voxSeg1.getId(), voxIntersecting, supportFromVX2);
		}
		else {
			if (!supportFromVX1 && !supportFromVX2) {
				cout << "WARNING: Intersection added which can't be supported by either object.\n";
			}
			//"Normal" New case, pay close attention to the Bool order.
			voxSeg1.addIntersection(voxSeg2.getId(), voxIntersecting, supportFromVX2);
			voxSeg2.addIntersection(voxSeg1.getId(), voxIntersecting, supportFromVX1);
		}
	}
	//std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
	//auto timeDiff = (std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count());
	//std::cout << std::setw(50) << "Time COMPUTEANDSETINTERSECTION: (sec) = " << timeDiff / 1000000.0 << std::endl;
}

void VoxelSegment::printOutRels() const
{
	std::vector<int> beingSupportedByIDs{};
	std::vector<int> notSupportiveContactIDs{};
	for (auto it = intersections.cbegin(); it != intersections.cend(); ++it) {
		if (it->second.second) {
			beingSupportedByIDs.push_back(it->first);
		}
		else {
			notSupportiveContactIDs.push_back(it->first);
		}
	}
	cout << "VX " << id;
	if (!beingSupportedByIDs.empty()) {
		cout << " is being supported by (";
		for (int i = 0; i < beingSupportedByIDs.size(); ++i) {
			cout << beingSupportedByIDs[i];
			if (i + 1 < beingSupportedByIDs.size()) {
				cout << ", ";
			}
		}
		cout << ").";
	}
	else {
		cout << " is not supported by anything.";
	}
	if (!notSupportiveContactIDs.empty()) {
		cout << " It touches but is not supported by (";
		for (int i = 0; i < notSupportiveContactIDs.size(); ++i) {
			cout << notSupportiveContactIDs[i];
			if (i + 1 < notSupportiveContactIDs.size()) {
				cout << ", ";
			}
		}
		cout << ").\n";
	}
	else {
		cout << " It has no other contact. \n";
	}

}

bool VoxelSegment::getIsFloor() const
{
	return this->isFloor;
}


void VoxelSegment::setIsFloor(bool isFloor)
{
	this->isFloor = isFloor;
}
//Does not prevent adding duplicate voxels!
void VoxelSegment::addVoxels(const pcl::PointCloud<pcl::PointXYZ>& vxsToAdd)
{
	this->voxels += vxsToAdd;
}


const pcl::PointCloud<pcl::PointXYZ>& VoxelSegment::getVoxels() const
{
	return this->voxels;
}

const std::map<int, std::pair<std::shared_ptr<pcl::PointCloud<pcl::PointXYZ>>, bool>>& VoxelSegment::getIntersections() const
{
	return this->intersections;
}

int VoxelSegment::getId() const
{
	return this->id;
}
