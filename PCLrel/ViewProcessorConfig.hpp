/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#pragma once
#ifndef ViewProcessorConfig_HPP
#define ViewProcessorConfig_HPP

class ViewProcessorConfig {
public:
	ViewProcessorConfig() = default;
	ViewProcessorConfig(bool _removeGround, bool _preferLowFloor = true, bool _applySmoothing = true, bool _clampXZRotationToZero = true, bool _useReciprocalCorrespondence = true, bool _useSymmetricObjective = true, bool _useABBMatching = false, unsigned int _maxSegmentationAttempts = 10, unsigned int _bruteForceLimit = 8, float _searchAngleStepSize = 10.0f)
		: preferLowFloor{ _preferLowFloor },
		removeFloor{ _removeGround },
		applySmoothing{ _applySmoothing },
		clampXZRotationToZero{ _clampXZRotationToZero },
		useReciprocalCorrespondence{ _useReciprocalCorrespondence },
		useSymmetricObjective{ _useSymmetricObjective },
		useABBMatching{ _useABBMatching },
		maximumSegmentationAttempts{ _maxSegmentationAttempts },
		bruteForceLimit{ _bruteForceLimit },
		searchAngleStepSize{ _searchAngleStepSize }
	{};
	~ViewProcessorConfig() = default;

	//Prioritize search for floor at lower heights (recommended: true; default: true).
	bool preferLowFloor = true;

	//If a ground should be tried to be found and removed or not (recommended: true; default: true). Set False if input data does not have a ground/floor in the views.
	bool removeFloor = true;

	//Apply Smoothing to the input pointclouds (recommended: true; default: true).
	bool applySmoothing = true;

	//Prevent the rotation around XZ (recommended: true; default true). -> true is good if your floor is nicely horizontal (i.e. parallel to the XZ plane)
	bool clampXZRotationToZero = true;

	//Use Reciprocal Correspondence for ICPN (recommended: true; default: true).
	bool useReciprocalCorrespondence = true;

	//Use the symmetric objective for ICPN (recommended: true; default: true).
	bool useSymmetricObjective = true;

	//Use the center of axis-oriented bounding boxes instead of geometric centroids (TODO SET RECOMMENDATION; default: false)
	bool useABBMatching = false;

	//Set the number of attempts for segmentation (recommended: value in range 5-10 normally, 1-3 if speed matters; default 10)
	unsigned int maximumSegmentationAttempts = 10;

	//Set the maximum number of segments at which brute force search is still applied. (Recommended: 7-9 depending on how good your CPU's single core performance is; default 8)
	unsigned int bruteForceLimit = 8;

	//Step Size of Rotation/Alignment Search (recommended 5-10, larger angles will mean faster computation but possible worse registration results). Default 10
	float searchAngleStepSize = 10.0f;

};

#endif /* ViewProcessorConfig_HPP */