/**
Original work Copyright (c) 2021 [Bernhard Luedtke]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 */

#include "ViewProcessor.h"

// LOCAL includes
#include "src/Registration/ICPRegistration.h"
#include "src/Segmentation/Segment.h"
#include "src/Segmentation/LCCPSegmenter.h"
#include "src/Segmentation/LCCPSegmentationConfig.h"
#include "src/Segmentation/LabelToSegmentConverter.h"
#include "src/Spatial/SpatialAnalyzer.h"
#include "src/Surface/SurfaceProcessor.h"
#include "src/Modifiers/PlaneRemoval.h"
#include "src/Modifiers/PointRemoval.h"
#include "src/Modifiers/Transforms.h"
#include "src/Registration/InitialGuesstimator.h"

// STD includes
#include <iostream>
#include <iomanip>

// PCL includes
#include <pcl/common/centroid.h>


// USING defines
using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::shared_ptr;
using std::setw;
using std::get;
using Eigen::Vector3f;

using normalColorCloud = pcl::PointCloud<pcl::PointXYZRGBNormal>;
using colorCloud = pcl::PointCloud<pcl::PointXYZRGB>;
using simpleCloud = pcl::PointCloud<pcl::PointXYZ>;
using std::fminf; using std::fmaxf;

namespace ProcessHelper {
	//Makes a rough guess what the "maximum correspondence" value for ICP should be.
	double getExperimentalMaxCorrespondGuess(const std::map<int, Segment>& v1segments, const std::map<int, Segment>& v2segments);
	
	unsigned int factorial(unsigned int n);
	
	//Calculate a rough approximation of the Final Angle resulting from ICP, only w.r.t. against the y-axis.
	float getICPYRotationAngle(Eigen::Matrix4f icpTransform);

	//Calculate a rough approximation of the Final Angle resulting from ICP, w.r.t. against the XZ-axis (/plane).
	float getICPXZRotationAngle(Eigen::Matrix4f icpTransform);

	std::map<int, Segment> getSegmentVecAsMap(const std::vector<Segment>& segments);
}


ViewProcessor::ViewProcessor()
{
	config = ViewProcessorConfig{};
}

ViewProcessor::ViewProcessor(const pcl::PointCloud<pcl::PointXYZRGB>& view1, const pcl::PointCloud<pcl::PointXYZRGB>& view2, const ViewProcessorConfig& vConfig)
	: config {vConfig},
	viewCloud1{ view1 },
	viewCloud2{ view2 }
{
	;
}

ViewProcessor::ViewProcessor(const ViewProcessorConfig& vConfig)
	: config {vConfig}
{
	;
}


std::vector<Segment> ViewProcessor::registerViewsToSegments()
{
	//Returns a cloud for each view, with the ground plane removed, (optionally smoothed), height aligned, normals estimated, and filtered.
	auto [view1Mod, view2Mod] = preProcessViewsForSegmentation(viewCloud1, viewCloud2);

	if (view1Mod.empty() || view2Mod.empty()) {
		std::cerr << "Unable to process input for Registration. No registration possible.\n";
		return std::vector<Segment>();
	}

	//Perform the LCCP Segmentation on both views.
	auto [seg1Map, seg2Map] = performSegmentation(view1Mod, view2Mod);

	if (seg1Map.empty() || seg2Map.empty()) {
		std::cerr << "At least one of two views has no segments. No Registration is possible.\n";
		return std::vector<Segment>();
	}

	//Calculates the Initial guess and applies it to seg2Map and view2Mod.
	auto [success, initialGuessInverseAngle, matches] = processSegmentsAndApplyGuess(seg1Map, seg2Map, view2Mod);

	if (!success) {
		std::cerr << "FAILURE to process Segments and apply the initial guess. Unable to register the views.\n";
		return std::vector<Segment>();
	}

	// Registration (ICP / ICPN)

	const double maxCorrespondDistance = fmaxf(ProcessHelper::getExperimentalMaxCorrespondGuess(seg1Map, seg2Map), 0.01f);

	Eigen::Matrix4f icpTransformOut = Eigen::Matrix4f::Identity();
	//TODO we could also just ignore this returned cloud, as we only use the extracted transform matrix anyway
	const auto view2ICPAligned = ICPRegistration::registerWithNormalsICPN(view2Mod, view1Mod, maxCorrespondDistance, config.useReciprocalCorrespondence, config.useSymmetricObjective, icpTransformOut);

	// Determine the Rotation introduced along X and Z by ICP (it is usually not wanted, as the floor is assumed to be parallel to the XZ plane already).
	const float xzAngleError = ProcessHelper::getICPXZRotationAngle(icpTransformOut);
	const float maxAllowedXZDeviation = 3.0f;
	// If user specified that XZ Rotation should be clamped to 0, then do so if there is any rotation worthwhile clamping
	// -> the clamping process is rather crude and not very nice... so only do it if absolutely necessary.
	if (xzAngleError > maxAllowedXZDeviation && config.clampXZRotationToZero) {
		const float yAngleRad = DEG2RAD(ProcessHelper::getICPYRotationAngle(icpTransformOut));
		pcl::PointCloud<pcl::PointXYZ> allSegments{};
		for (const auto& s : seg2Map) {
			allSegments += *s.second.getInputCloud();
		}
		pcl::PointXYZ mCenter{};
		pcl::computeCentroid(allSegments, mCenter);
		for (auto& mapEntry : seg2Map) {
			mapEntry.second.translateSegmentNoCHull(mCenter.getVector3fMap() * -1.0f);
			mapEntry.second.rotateSegmentNoCHull(Vector3f::UnitY(), -yAngleRad);
			mapEntry.second.translateSegmentNoCHull(mCenter.getVector3fMap());
			bool ret = mapEntry.second.computeCHull();
			if (!ret) {
				//TODO better Error message
				std::cerr << "ERROR REGISTRATION: COULD NOT APPLY Y ANGLE TO SEGMENT AND COMPUTE CHULL. Could cause issues later.\n";
			}
		}
	}
	else {
		//Apply ICPN transform to the segments from view2
		for (auto& mapEntry : seg2Map) {
			mapEntry.second.applyTransformMatrix(icpTransformOut);
		}
	}

	// The average nearest neighbor distance is necessary for applying the right duplicate prevention in mergeSegments.
	const float midNND = (SpatialAnalyzer::getAvgNNDist(view1Mod) + SpatialAnalyzer::getAvgNNDist(view2Mod)) * 0.5f;
	std::vector<Segment> mergedSegments = mergeSegments(seg1Map, seg2Map, matches, midNND);

	return mergedSegments;
}

void ViewProcessor::setInputViewClouds(const pcl::PointCloud<pcl::PointXYZRGB>& view1, const pcl::PointCloud<pcl::PointXYZRGB>& view2)
{
	this->viewCloud1 = pcl::PointCloud<pcl::PointXYZRGB>(view1);
	this->viewCloud2 = pcl::PointCloud<pcl::PointXYZRGB>(view2);
}

std::pair<pcl::PointCloud<pcl::PointXYZRGB>, pcl::PointCloud<pcl::PointXYZRGB>> ViewProcessor::getInputViewClouds() const
{
	return { this->viewCloud1, this->viewCloud2 };
}

void ViewProcessor::applySmartSmoothingOnScene(pcl::PointCloud<pcl::PointXYZRGB>& inputView1, pcl::PointCloud<pcl::PointXYZRGB>& inputView2)
{
	const float avgNNDist1 = SpatialAnalyzer::getAvgNNDist(inputView1);
	const float avgNNDist2 = SpatialAnalyzer::getAvgNNDist(inputView2);
	float fFactor1 = 4.0f;
	float fFactor2 = 4.0f;
	if (avgNNDist1 > avgNNDist2) {
		const float div = avgNNDist1 / avgNNDist2;
		fFactor1 = 3.5f * fmaxf(1.0f, div * 0.75f);
	}
	else if (avgNNDist1 < avgNNDist2) {
		const float div = avgNNDist2 / avgNNDist1;
		fFactor2 = 3.5f * fmaxf(1.0f, div * 0.75f);
	}
	inputView1 = SurfaceProcessor::smoothCloud(inputView1, avgNNDist1 * fFactor1);
	inputView2 = SurfaceProcessor::smoothCloud(inputView2, avgNNDist2 * fFactor2);
}

std::pair<pcl::PointCloud<pcl::PointXYZRGBNormal>, pcl::PointCloud<pcl::PointXYZRGBNormal>> ViewProcessor::preProcessViewsForSegmentation(const pcl::PointCloud<pcl::PointXYZRGB>& inputView1, const pcl::PointCloud<pcl::PointXYZRGB>& inputView2)
{
	if (inputView1.empty() || inputView2.empty()) {
		cout << "One or both Input Clouds for Preprocessing empty. Unable to process.\n";
		return std::pair<pcl::PointCloud<pcl::PointXYZRGBNormal>, pcl::PointCloud<pcl::PointXYZRGBNormal>>();
	}
	pcl::PointCloud<pcl::PointXYZRGB> tmpCloud1{ inputView1 };
	pcl::PointCloud<pcl::PointXYZRGB> tmpCloud2{ inputView2 };

	if (config.applySmoothing) {
		applySmartSmoothingOnScene(tmpCloud1, tmpCloud2);
	}

	//Get the average distance to the nearest other point averaged over the two views. Used to determine voxel grid size.
	const float midNND = (SpatialAnalyzer::getAvgNNDist(tmpCloud1) + SpatialAnalyzer::getAvgNNDist(tmpCloud2)) * 0.5f;

	//Apply the Voxel Grid Filter to both clouds (testing a bit with the factors still)
	const auto sampledView1 = SurfaceProcessor::applyVoxelGridFilter(std::move(tmpCloud1), midNND);
	const auto sampledView2 = SurfaceProcessor::applyVoxelGridFilter(std::move(tmpCloud2), midNND);

	//TODO: There should be a better way to generalize this.
	int kNormalsN = 80;
	//Reduce the Normal Neighbor Search count if the clouds are very small on average.
	while ((sampledView1.size() + sampledView2.size() / 2) / 80 < 4 && kNormalsN >= 20)
	{
		kNormalsN -= 10;
	}
	//Estimate the normals of both clouds.
	const auto view1Normals = SurfaceProcessor::estimateNormals(sampledView1, 0, kNormalsN);
	const auto view2Normals = SurfaceProcessor::estimateNormals(sampledView2, 0, kNormalsN);

	//Concatenate the normals to the XYZRGB Data so that we have one cloud for each view with normals.
	// Any transformation applied to point clouds with type PointXYZRGBNormal needs to be done with the "[...]WithNormals" transforms.

	pcl::PointCloud<pcl::PointXYZRGBNormal> view1Mod{};
	pcl::PointCloud<pcl::PointXYZRGBNormal> view2Mod{};
	pcl::concatenateFields(sampledView1, view1Normals, view1Mod);
	pcl::concatenateFields(sampledView2, view2Normals, view2Mod);

	//Determine the ground plane height, removing the ground plane in the process.
	float groundHeightView1{ 0.0f }, groundHeightView2{ 0.0f };
	if (config.removeFloor) {
		view1Mod = PlaneRemoval::removeGroundPlaneByRateOfChangeUpNorm(view1Mod, groundHeightView1, config.preferLowFloor);
		view2Mod = PlaneRemoval::removeGroundPlaneByRateOfChangeUpNorm(view2Mod, groundHeightView2, config.preferLowFloor);
		//TODO MAKE CONFIGURABLE
		//view1Mod = PlaneRemoval::removeDominantXZParallelPlane(view1Mod, 0.07, groundHeightView1); //RANSAC Version.
		//view2Mod = PlaneRemoval::removeDominantXZParallelPlane(view2Mod, 0.07, groundHeightView2);
	}

	//Apply statistical outlier removal - Removing the ground plane might leave a few outliers -> remove those.
	// TODO adjust settings dynamically!
	view1Mod = PointRemoval::statistOutliersRemoval(view1Mod, 10, 1.5f);
	view2Mod = PointRemoval::statistOutliersRemoval(view2Mod, 10, 1.5f);

	//Align the height of the two views by using the ground plane height.
	view2Mod = Transforms::translatePCloudWithNormals(view2Mod, 0.0f, (groundHeightView1 - groundHeightView2), 0.0f);

	return { view1Mod, view2Mod };
}

std::tuple<bool, float, std::map<int, int>> ViewProcessor::processSegmentsAndApplyGuess(const std::map<int, Segment>& seg1Map, std::map<int, Segment>& seg2Map, pcl::PointCloud<pcl::PointXYZRGBNormal>& view2Mod)
{
	// Settings
	//note: This is fine to be hardcoded, guesstimator will adjust this based on segment count and configured brute force limit anyway.
	const bool greedy = false;
	//TODO make configurable
	const bool useVariant2ForInitialGuess = true;
	//TODO make configurable
	const bool verboseGuess = false;
	//TODO make configurable
	const bool allowChangeY = false;
	
	InitialGuesstimator guessMachine{ greedy, useVariant2ForInitialGuess, config.useABBMatching, verboseGuess, config.bruteForceLimit, config.searchAngleStepSize, allowChangeY };

	guessMachine.setInputSegments(seg1Map, seg2Map);
	
	guessMachine.computeInitialGuess();

	const float initialGuessInverseAngle = guessMachine.getInitialGuessAngle();
	const float initialGuessAngle = -initialGuessInverseAngle;

	const Vector3f bestTotalTranslation{ guessMachine.getInitialGuessTranslation() };
	std::map<int, int> matches{ guessMachine.getInitialGuessMatches() };
	//cout << "Guessed Angle: " << initialGuessInverseAngle << "\n";

	if (matches.empty()) {
		cout << "No matches could be determined. No Registration is possible.\n";
		return { false, 0.0f, std::map<int,int>() };
	}

	//Apply the initial guess for rotation against the vertical (y) axis and the translation.
	view2Mod = Transforms::rotatePCloudAroundAxisWithNormals(view2Mod, Vector3f::UnitY(), DEG2RAD(initialGuessAngle));
	view2Mod = Transforms::translatePCloudWithNormals(view2Mod, bestTotalTranslation);

	//Apply Initial guess to the segments from Map2
	for (auto& mapEntry : seg2Map) {
		mapEntry.second.rotateThenTranslateSegment(Eigen::Vector3f::UnitY(), DEG2RAD(initialGuessAngle), bestTotalTranslation);
	}
	return { true, initialGuessInverseAngle, matches };
}

std::pair<std::map<int, Segment>, std::map<int, Segment>> ViewProcessor::performSegmentation(const pcl::PointCloud<pcl::PointXYZRGBNormal>& view1Mod, const pcl::PointCloud<pcl::PointXYZRGBNormal>& view2Mod)
{
	if (view1Mod.empty() || view2Mod.empty()) {
		std::cerr << "\nFAILURE: At least 1 Input Cloud for Segmentation is empty!\n";
		return std::pair<std::map<int, Segment>, std::map<int, Segment>>();
	}
	const float avgNND1 = SpatialAnalyzer::getAvgNNDist(view1Mod);
	const float avgNND2 = SpatialAnalyzer::getAvgNNDist(view2Mod);
	float midNND = (avgNND1 + avgNND2) * 0.5f;

	// Adjusted based on avgNNDist -> Would probably cause issues with clouds whose
	// density/resolution varies strongly, but then LCCP is a lost case anyway.
	const float vox = midNND * 1.4f; // 1.45 old

	//This is just a mult factor, so it adjusts the seed size dynamically based on voxelSize.
	const float seedF = 1.3f;		//old setting 1.35f;

	const bool removeVerySparseSegments = true;
	const float sparsenessThreshold = midNND * 5.0f;

	//Should be set by use of the size of the clouds, not hardcoded. Thats not trivial however, as we
	// do not know how many objects will be in the cloud. Also, Objects might not have the same point count.
	const int segMinSize = 80;
	//Setting for Angle Convexity for LCCP Segmentation
	const float angleC = 4.0f;

	//Settings for importance factors for LCCP Segmentation. Determined based on testing
	const float spatialImportance = 1.0f;
	const float normalImportance = 4.0f;
	const float colorImportance = 0.5f;

	float voxTrack1 = vox;
	float voxTrack2 = vox;

	//Increase Voxel Size if one of the clouds is noticeably coarser, but don't increase it toooooo much. (see *0.8f factor there).
	const float voxView1 = (avgNND1 * 1.25f > avgNND2) ? vox * fminf(fmaxf(1.0f, (avgNND1 / avgNND2) * 0.8f), 3.0f) : vox;
	const float voxView2 = (avgNND2 * 1.25f > avgNND1) ? vox * fminf(fmaxf(1.0f, (avgNND2 / avgNND1) * 0.8f), 3.0f) : vox;

	std::vector<Segment> segmentsView1{};
	std::vector<Segment> segmentsView2{};
	unsigned int iterations = 0;
	LCCPSegmentationConfig conf = LCCPSegmentationConfig();
	conf.setManual(vox, seedF, angleC, segMinSize, spatialImportance, normalImportance, colorImportance);
	do
	{
		if (iterations > 0) {
			cout << "Unequal amount of segments, trying again (" << segmentsView1.size() << "|" << segmentsView2.size() << ")\n";
		}
		voxTrack1 = voxView1 + (voxView1 * 0.02f * iterations); // increase the size if we are in second, third... iteration.
		conf.updateVoxelSizeAndSeeding(voxTrack1, seedF);
		auto view1L = LCCPSegmenter::segmentLCCP(view1Mod, conf);
		voxTrack2 = voxView2 + (voxView2 * 0.02f * iterations);
		conf.updateVoxelSizeAndSeeding(voxTrack2, seedF);
		auto view2L = LCCPSegmenter::segmentLCCP(view2Mod, conf);

		segmentsView1 = LabelToSegmentConverter::produceSgmtsFromLabelCloud(view1L, segMinSize, removeVerySparseSegments, sparsenessThreshold);
		segmentsView2 = LabelToSegmentConverter::produceSgmtsFromLabelCloud(view2L, segMinSize, removeVerySparseSegments, sparsenessThreshold);

		iterations++;
	} while (segmentsView1.size() != segmentsView2.size() && iterations < config.maximumSegmentationAttempts);

	return { ProcessHelper::getSegmentVecAsMap(segmentsView1), ProcessHelper::getSegmentVecAsMap(segmentsView2) };
}

std::vector<Segment> ViewProcessor::mergeSegments(const std::map<int, Segment>& seg1map, const std::map<int, Segment>& seg2map, const std::map<int, int>& matches, float midNNdist)
{
	//cout << "Merging Segments \n";
	if (seg1map.empty() && seg2map.empty()) {
		cout << "mergeSegments: Both Segment Maps empty!\n";
		return std::vector<Segment>();
	}
	//Merge Segments
	std::vector<Segment> mergedSegments = {};
	mergedSegments.reserve(matches.size());
	std::vector<int> seg2IdsMatched = {};
	std::vector<int> seg1IdsMatched = {};
	seg2IdsMatched.reserve(matches.size());
	seg1IdsMatched.reserve(matches.size());
	int idCounter = 1;
	for (const auto& match : matches) {
		//Only match if both IDs exist in the maps and are not -1.
		if (match.first != -1 && match.second != -1 && seg1map.count(match.first) != 0 && seg2map.count(match.second) != 0) {
			pcl::PointCloud<pcl::PointXYZ> combined;
			pcl::PointCloud<pcl::PointXYZ>::concatenate(*seg1map.at(match.first).getInputCloud(), *seg2map.at(match.second).getInputCloud(), combined);

			//When merging two segments, some points might be at exactly the same position -> we do not want these 'duplicate' points.
			//So: Combine Points that are very close together. -> VoxelGridFilter should do that nicely, slightly coarser than the one at the start
			combined = SurfaceProcessor::applyVoxelGridFilter(combined, midNNdist * 1.25f);
			mergedSegments.push_back(Segment(combined, idCounter));
			seg2IdsMatched.push_back(match.second);
			seg1IdsMatched.push_back(match.first);
			idCounter++;
		}
	}

	if (seg1map.size() != seg2map.size()) {
		//-> Segments were left over. Add them here
		for (const auto& segE : seg1map) {
			if (std::find(seg1IdsMatched.begin(), seg1IdsMatched.end(), segE.first) == seg1IdsMatched.end()) {
				//Not using the copy constructor as we want to adjust the ID 
				mergedSegments.push_back(Segment(*segE.second.getInputCloud(), idCounter));
				idCounter++;
				seg1IdsMatched.push_back(segE.first);
			}
		}
		for (const auto& segE : seg2map) {
			if (std::find(seg2IdsMatched.begin(), seg2IdsMatched.end(), segE.first) == seg2IdsMatched.end()) {
				mergedSegments.push_back(Segment(*segE.second.getInputCloud(), idCounter));
				idCounter++;
				seg2IdsMatched.push_back(segE.first);
			}
		}
	}
	return mergedSegments;
}















//######################################################## HELPERs Implementation ########################################################

namespace ProcessHelper {
	inline double getExperimentalMaxCorrespondGuess(const std::map<int, Segment>& v1segments, const std::map<int, Segment>& v2segments)
	{
		float maxLocalVal = 0.0f;
		const float reductionFactor = 0.2f;
		for (const auto& segE : v1segments) {
			pcl::PointXYZ minPt, maxPt;
			segE.second.getMinMax3D(minPt, maxPt);
			const float mLength = (maxPt.getVector3fMap() - minPt.getVector3fMap()).norm() * reductionFactor;
			if (mLength > maxLocalVal) {
				maxLocalVal = mLength;
			}
		}
		float maxLocalVal2 = 0.0f;
		for (const auto& segE : v2segments) {
			pcl::PointXYZ minPt, maxPt;
			segE.second.getMinMax3D(minPt, maxPt);
			const float mLength = (maxPt.getVector3fMap() - minPt.getVector3fMap()).norm() * reductionFactor;
			if (mLength > maxLocalVal2) {
				maxLocalVal2 = mLength;
			}
		}
		return fminf(maxLocalVal, maxLocalVal2);
	}

	inline unsigned int factorial(unsigned int n)
	{
		if (n == 0) {
			return 1;
		}
		if (n == 1) {
			return 1;
		}
		return n * factorial(n - 1);
	}


	//Calculate a rough approximation of the Final Angle resulting from ICP, only w.r.t. against the y-axis.
	inline float getICPYRotationAngle(Eigen::Matrix4f icpTransform)
	{
		Eigen::Vector4f rotationFromICPNAroundY = Eigen::Vector4f(1, 0, 0, 0);
		//cout << "ICP Transform: \n" << icpTransformOut << "\n\n";
		rotationFromICPNAroundY = icpTransform * rotationFromICPNAroundY;
		const Eigen::Vector2f shortTestRot = Eigen::Vector2f(rotationFromICPNAroundY.x(), rotationFromICPNAroundY.z()).normalized();
		float angleT = RAD2DEG(std::atan2f(shortTestRot.y(), shortTestRot.x()));
		return angleT;
	}

	//Calculate a rough approximation of the Final Angle resulting from ICP, w.r.t. against the XZ-axis (/plane).
	inline float getICPXZRotationAngle(Eigen::Matrix4f icpTransform)
	{
		//Vector3f transformX = Vector3f(icpTransform(0, 0), icpTransform(1, 0), icpTransform(2, 0));
		const Vector3f transformY = Vector3f(icpTransform(0, 1), icpTransform(1, 1), icpTransform(2, 1));
		//Rotation around X and Z has to be measured together.
		const Vector3f trueYVec = Vector3f::UnitY();
		//Formula: angle = atan2(norm(cross(a, b)), dot(a, b))
		float testAngle = std::atan2f(transformY.cross(trueYVec).norm(), transformY.dot(trueYVec));
		if (testAngle > M_PI) {
			testAngle -= 2.0f * M_PI;
		}
		else if (testAngle <= -M_PI) {
			testAngle += 2.0f * M_PI;
		}
		//cout << "3D XZ Angle: " << setw(10) << RAD2DEG(testAngle) << "\n";
		return RAD2DEG(testAngle);
	}

	inline std::map<int, Segment> getSegmentVecAsMap(const std::vector<Segment>& segments)
	{
		std::map<int, Segment> retMap{};
		for (const auto& seg : segments) {
			retMap[seg.getId()] = Segment(*seg.getInputCloud(), seg.getId());
		}
		return retMap;
	}
}